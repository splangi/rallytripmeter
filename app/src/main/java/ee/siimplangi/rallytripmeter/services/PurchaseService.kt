package ee.siimplangi.rallytripmeter.services

import android.app.Activity
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.util.Log
import com.android.billingclient.api.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import ee.siimplangi.rallytripmeter.AppLog
import ee.siimplangi.rallytripmeter.BuildConfig
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants
import ee.siimplangi.rallytripmeter.constants.Sku
import ee.siimplangi.rallytripmeter.enums.ProductStatus
import ee.siimplangi.rallytripmeter.helpers.TrialChecker
import ee.siimplangi.rallytripmeter.helpers.UniqueDeviceIDGenerator
import ee.siimplangi.rallytripmeter.listeners.OnTrialStatus
import ee.siimplangi.rallytripmeter.models.TrialModel
import org.greenrobot.eventbus.EventBus


/**
 * Created by Siim on 08.09.2017.
 */

class PurchaseService : Service(), PurchasesUpdatedListener, BillingClientStateListener {


    private var purchasedFeatures: List<Purchase>? = null
        set(value) {
            field = value
            updateProductStatus()
        }
    private var trialModel: TrialModel? = null
        set(value) {
            field = value
            updateProductStatus()
        }

    private var deviceTrialModel: TrialModel? = null
        set(value) {
            field = value
            updateProductStatus()
        }

    var trialLengthMillis: Long = FirebaseRemoteConfig.getInstance().getLong(FirebaseConstants.CONFIG_TRIAL_LENGTH) * 24 * 60 * 60 * 1000


    private var currentProductStatus: ProductStatus? = null
        set(value) {
            if (value == null && field != null) {
                field = value;
                Log.d(PurchaseService::class.java.simpleName, "Removing sticky event!")
                EventBus.getDefault().removeStickyEvent(ProductStatus::class.java)
            } else if (value != null && field != value) {
                field = value
                Log.d(PurchaseService::class.java.simpleName, "Sending sticky event")
                EventBus.getDefault().postSticky(value)
            }
        }


    private var testPurchaseToken: String? = null

    private val handler = Handler()


    private val shutdownTime = if (BuildConfig.DEBUG) 3000L else 10000L
    private var connectedCount = 0;

    private lateinit var billingClient: BillingClient

    private lateinit var deviceTrialChecker: TrialChecker

    private var trialChecker: TrialChecker? = null
        set(value) {
            trialModel = null
            field?.onStop()
            field = value
            value?.onStart()
        }


    init {

    }

    private val authStateListener = FirebaseAuth.AuthStateListener {
        updateProductStatus()
        val user = it.currentUser
        trialChecker = if (user != null) {
            TrialChecker(user.uid, onTrialStatusListener)
        } else {
            null
        }
    }

    private val onTrialStatusListener = object : OnTrialStatus {
        override fun onTrialStatus(productStatus: ProductStatus, trialModel: TrialModel?) {
            this@PurchaseService.trialModel = trialModel
        }
    }

    private val onDeviceTrialStatusListener = object : OnTrialStatus {
        override fun onTrialStatus(productStatus: ProductStatus, trialModel: TrialModel?) {
            this@PurchaseService.deviceTrialModel = trialModel
        }
    }


    override fun onBind(intent: Intent): IBinder? {
        onBound()
        return BillingServiceBinder()
    }

    override fun onUnbind(intent: Intent?): Boolean {
        onUnBound()
        return super.onUnbind(intent)
    }

    override fun onRebind(intent: Intent?) {
        onBound()
        super.onRebind(intent)
    }

    private fun onBound() {
        connectedCount++
        handler.removeCallbacksAndMessages(null)
    }

    private fun onUnBound() {
        connectedCount--
        handler.removeCallbacksAndMessages(null)
        if (connectedCount == 0) {
            handler.postDelayed({ this.stopSelf() }, shutdownTime)
        }

    }

    override fun onCreate() {
        super.onCreate()
        /*if (BuildConfig.DEBUG) {
            trialLengthMillis = resources.getInteger(R.integer.test_trial_length_millis).toLong()
        }*/
        deviceTrialChecker = TrialChecker(UniqueDeviceIDGenerator.getUniqueId(this), onDeviceTrialStatusListener)
        deviceTrialChecker.onStart()
        FirebaseAuth.getInstance().addAuthStateListener(authStateListener)
        //startService(PurchaseService.getIntent(this))
        billingClient = BillingClient.newBuilder(this).setListener(this).build()
        billingClient.startConnection(this)
        //billingClient.startConnection(this)
    }

    override fun onDestroy() {
        FirebaseAuth.getInstance().removeAuthStateListener { authStateListener }
        deviceTrialChecker.onStop()
        handler.removeCallbacksAndMessages(null)
        try {
            if (BuildConfig.DEBUG && testPurchaseToken != null) {
                billingClient.consumeAsync(testPurchaseToken) { _, _ -> }
            }
            trialChecker = null
            billingClient.endConnection()
        } catch (e: IllegalArgumentException) {
            AppLog.e(PurchaseService::class.java, "Could not end service with billingClient at onDestroy", e);
        }
        super.onDestroy()

    }

    private fun isRunningOnEmulator(): Boolean {
        var result = //
                (Build.FINGERPRINT.startsWith("generic")//

                        || Build.FINGERPRINT.startsWith("unknown")//

                        || Build.MODEL.contains("google_sdk")//

                        || Build.MODEL.contains("Emulator")//

                        || Build.MODEL.contains("Android SDK built for x86"))
        if (result)
            return true
        result = result or (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
        if (result)
            return true
        result = result or ("google_sdk" == Build.PRODUCT)
        return result
    }

    override fun onPurchasesUpdated(responseCode: Int, purchases: List<Purchase>?) {
        onNewPurchases(responseCode, purchases)
    }

    private fun updateProductStatus() {
        val trialStatus = trialModel?.getStatus()
        val deviceTrialStatus = deviceTrialModel?.getStatus()
        currentProductStatus = when {
            FirebaseAuth.getInstance().currentUser == null -> ProductStatus.NOT_LOGGED_IN
            purchasedFeatures?.asSequence()?.map { it.sku }?.any {
                Sku.getAllInAppSkuIDs().contains(it) || Sku.getAllSubSkuIDs().contains(it)
            } == true -> ProductStatus.VALID_BILLING
            trialStatus == null || deviceTrialStatus == null -> null
            trialStatus == ProductStatus.TRIAL_VALID && deviceTrialStatus == ProductStatus.TRIAL_VALID -> ProductStatus.TRIAL_VALID
            deviceTrialStatus == ProductStatus.TRIAL_NOT_STARTED && trialStatus != ProductStatus.TRIAL_NOT_STARTED -> {
                deviceTrialChecker.startTrial(trialLengthMillis)
                trialStatus
            }
            deviceTrialStatus == ProductStatus.TRIAL_EXPIRED || trialStatus == ProductStatus.TRIAL_EXPIRED -> ProductStatus.TRIAL_EXPIRED
            (purchasedFeatures != null || isRunningOnEmulator()) && trialModel != null -> trialStatus
            else -> null
        }
    }

    private fun onNewPurchases(responseCode: Int, purchases: List<Purchase>?) {
        testPurchaseToken = purchases?.find { it.sku == "android.test.purchased" }?.purchaseToken
        when (responseCode) {
            BillingClient.BillingResponse.OK -> {
                this.purchasedFeatures = purchases
            }
            BillingClient.BillingResponse.USER_CANCELED -> {
                updateProductStatus()
            }
            else -> AppLog.e(PurchaseService::class.java.simpleName, "BillingResponse: $responseCode")
        }//Do nothing
    }

    override fun onBillingSetupFinished(resultCode: Int) {
        if (resultCode == BillingClient.BillingResponse.OK) {
            getCurrentPurchasedFeatures()
        } else if (resultCode != BillingClient.BillingResponse.USER_CANCELED) {
            purchasedFeatures = emptyList()
            AppLog.e(PurchaseService::class.java.simpleName, "onBillingSetupFinished resultcode: $resultCode")
        }
    }

    override fun onBillingServiceDisconnected() {

    }

    private fun getCurrentPurchasedFeatures() {
        if (billingClient.isReady) {
            val subResults = billingClient.queryPurchases(BillingClient.SkuType.SUBS)
            val inAppResults = billingClient.queryPurchases(BillingClient.SkuType.INAPP)
            if (subResults.responseCode == BillingClient.BillingResponse.OK && inAppResults.responseCode == BillingClient.BillingResponse.OK) {
                onNewPurchases(BillingClient.BillingResponse.OK, subResults.purchasesList.plus(inAppResults.purchasesList))
            } else {
                AppLog.e(PurchaseService::class.java.simpleName, "getCurrentlyPurchasedFeatures unsuccessful, sub_code: ${subResults.responseCode}, inapp_code: ${inAppResults.responseCode} ")
            }

        }
    }

    private class SkuDetailsReponseListenerWrapper(val listener: SkuDetailsResponseListener) : SkuDetailsResponseListener {
        val skuDetailsList: MutableList<SkuDetails> = ArrayList()
        var responseCode: Int = BillingClient.BillingResponse.OK
        var countDown = 2

        override fun onSkuDetailsResponse(responseCode: Int, skuDetailsList: MutableList<SkuDetails>?) {
            if (responseCode == BillingClient.BillingResponse.OK) {
                if (skuDetailsList != null) {
                    this.skuDetailsList.addAll(skuDetailsList)
                }
            } else {
                this.responseCode = responseCode
            }
            countDown--
            if (countDown == 0) {
                listener.onSkuDetailsResponse(responseCode, if (responseCode == BillingClient.BillingResponse.OK) this.skuDetailsList else null)
            }
        }


    }


    inner class BillingServiceBinder : Binder() {

        fun startTrial() {
            trialChecker?.startTrial(trialLengthMillis)
            deviceTrialChecker.startTrial(trialLengthMillis)
        }

        fun launchBillingFlow(activity: Activity, sku: String, skuType: String) {
            val skuId = if (BuildConfig.DEBUG) "android.test.purchased" else sku
            val params = BillingFlowParams.newBuilder().setType(skuType).setSku(skuId).build()
            billingClient.launchBillingFlow(activity, params)
        }

        fun reconnect(){
            if (!billingClient.isReady) {
                billingClient.startConnection(this@PurchaseService)
            }
        }

        fun getSkuDetails(listener: SkuDetailsResponseListener) {
            val listenerWrapper = SkuDetailsReponseListenerWrapper(listener)
            billingClient.querySkuDetailsAsync(SkuDetailsParams.newBuilder().setSkusList(Sku.getAllInAppSkuIDs()).setType(BillingClient.SkuType.INAPP).build(), listenerWrapper)
            billingClient.querySkuDetailsAsync(SkuDetailsParams.newBuilder().setSkusList(Sku.getAllSubSkuIDs()).setType(BillingClient.SkuType.SUBS).build(), listenerWrapper)
        }

        fun getTrialModel(): TrialModel? {
            return trialModel
        }

    }

    companion object {


        fun getIntent(context: Context): Intent {
            return Intent(context, PurchaseService::class.java)
        }

    }
}
