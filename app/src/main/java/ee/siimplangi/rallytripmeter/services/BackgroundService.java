package ee.siimplangi.rallytripmeter.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.activities.MainActivity;
import ee.siimplangi.rallytripmeter.helpers.Settings;
import ee.siimplangi.rallytripmeter.managers.BackgroundManager;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Siim on 26.06.2017.
 */


/*
STATES
STARTED -> BOUND -> UNBOUND -> STOPPED
 */

public class BackgroundService extends Service {

    private static final String NOTIFICATION_CHANNEL_ID = "rallytripmeter_channel";

    private static final String SAVED_INSTANCE_STATE = "savedInstance";
    private static final int NOTIFICATION_ID = 89;
    public static boolean isStarted = false;
    private BackgroundManager backgroundManager;
    private Notification notification;
    private boolean foregroundStarted;


    public static Intent getIntent(Context context) {
        return new Intent(context, BackgroundService.class);
    }


    public static void start(Context context, Bundle savedInstanceState) {
        Intent intent = getIntent(context);
        intent.putExtra(SAVED_INSTANCE_STATE, savedInstanceState);
        context.startService(intent);
    }

    public static void stop(Context context) {
        context.stopService(getIntent(context));
    }

    public static boolean isBackgroundEnabled() {
        return Settings.INSTANCE.getRunInBackground();
    }

    private void onStart(Bundle savedInstanceState) {
        isStarted = true;
        Log.d(BackgroundService.class.getSimpleName(), "onStart, savedInstanceState == null ? " + (savedInstanceState != null));
        if (backgroundManager == null) {
            backgroundManager = new BackgroundManager(this, savedInstanceState);
        }
        if (!backgroundManager.isStarted()) {
            backgroundManager.onStart();
        }
    }

    private void onStop() {
        isStarted = false;
        Log.d(BackgroundService.class.getSimpleName(), "onStop");
        stopForeground(true);
        if (backgroundManager != null && backgroundManager.isStarted()) {
            backgroundManager.onStop();
        }
        backgroundManager = null;

    }

    public void onBound() {
        Log.d(BackgroundService.class.getSimpleName(), "onBound");
        if (!isStarted) {
            Log.d(BackgroundService.class.getSimpleName(), "onBound called without service started, it must have been closed through a notification and the activity was not closed yet! Let's start it up");
            BackgroundService.start(this, null);
        }
        if (foregroundStarted) {
            stopForeground(true);
            foregroundStarted = false;
        }
    }

    public void onUnBound() {
        Log.d(BackgroundService.class.getSimpleName(), "onUnBound");
        if (isBackgroundEnabled()) {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.P || EasyPermissions.hasPermissions(this, android.Manifest.permission.FOREGROUND_SERVICE)) {
                startForeground(NOTIFICATION_ID, notification);
                foregroundStarted = true;
            }
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        onBound();
        return new BackgroundServiceBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        onUnBound();
        return true;
    }

    @Override
    public void onRebind(Intent intent) {
        onBound();
        super.onRebind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notification = buildNotification();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            buildNotificationChannel();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        onStart(intent != null ? intent.getBundleExtra(SAVED_INSTANCE_STATE) : null);
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        onStop();
        super.onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void buildNotificationChannel() {
        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, getString(R.string.app_name), NotificationManager.IMPORTANCE_LOW);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        channel.enableLights(false);
        channel.enableVibration(false);
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);
    }


    private Notification buildNotification() {
        Intent intent = new Intent(this, StopReceiver.class);
        PendingIntent cancelEvent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent launchMainActivity = PendingIntent.getActivity(this, 0, MainActivity.Companion.getIntent(this).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP), PendingIntent.FLAG_CANCEL_CURRENT);
        return new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification_bg)
                .setContentTitle(getString(R.string.background_service_notification_title))
                //.setContentInfo(getString(R.string.background_service_notification_title))
                .addAction(new NotificationCompat.Action(R.drawable.ic_stat_power_settings_new, getString(R.string.turn_off), cancelEvent))
                .setOngoing(true)
                .setContentText(getString(R.string.background_service_notification_text))
                .setLocalOnly(true)
                .setContentIntent(launchMainActivity)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .build();
    }


    public static class StopReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            stop(context);
        }
    }

    public class BackgroundServiceBinder extends Binder {

        @Nullable
        public BackgroundManager getBackgroundManager() {
            return backgroundManager;
        }

    }
}
