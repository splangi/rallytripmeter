package ee.siimplangi.rallytripmeter.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import ee.siimplangi.rallytripmeter.AppLog;
import ee.siimplangi.rallytripmeter.events.PermissionUpdateEvent;
import ee.siimplangi.rallytripmeter.models.GpsAccuracy;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Siim on 12.06.2017.
 */

public class LocationService extends Service {

    private static final long interval = 250;
    private static final long fastestInterval = 100;
    private static final float minDistance = 0;

    public static final Criteria criteria;

    static {
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
    }

    public static final LocationRequest LOCATION_REQUEST = LocationRequest.create()
            .setInterval(interval)
            .setFastestInterval(fastestInterval)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY).setSmallestDisplacement(minDistance);
    private final EventBus eventBus = EventBus.getDefault();
    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            eventBus.postSticky(location);
            eventBus.postSticky(new GpsAccuracy(location));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            eventBus.postSticky(new GpsAccuracy(status));
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {
            eventBus.postSticky(new GpsAccuracy(GpsAccuracy.Accuracy.NONE));
        }
    };
    private final LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            eventBus.postSticky(locationResult.getLastLocation());
            eventBus.postSticky(new GpsAccuracy(locationResult.getLastLocation()));
        }

        @Override
        public void onLocationAvailability(LocationAvailability locationAvailability) {
            super.onLocationAvailability(locationAvailability);
            eventBus.postSticky(new GpsAccuracy(locationAvailability));
        }
    };
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationManager locationManager;
    private boolean isRegistered;
    private boolean useFallback = false;
    private boolean fallbackRegistered = false;

    public static Intent getIntent(Context context) {
        return new Intent(context, LocationService.class);
    }

    @Subscribe
    public void onPermissionUpdate(PermissionUpdateEvent event) {
        registerForLocationUpdates();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new Binder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        registerForLocationUpdates();
    }

    @SuppressLint("MissingPermission")
    private void registerFallbackLocationUpdates() {
        useFallback = true;
        if (!fallbackRegistered && EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            locationManager.requestLocationUpdates(interval, minDistance, criteria, locationListener, Looper.myLooper());
            fallbackRegistered = true;
        }
    }

    @SuppressLint("MissingPermission")
    private void registerForLocationUpdates() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (useFallback) {
                registerFallbackLocationUpdates();
            } else if (!isRegistered) {
                fusedLocationProviderClient.requestLocationUpdates(LOCATION_REQUEST, locationCallback, null).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        AppLog.e(this, "FusedLocationClient failure, falling back to legacy LocationManager!", e);
                        unregisterFused();
                        registerFallbackLocationUpdates();

                    }
                });
                isRegistered = true;
            }
        }
    }

    private void unregisterFused() {
        try {
            isRegistered = false;
            fusedLocationProviderClient.removeLocationUpdates(locationCallback);
        } catch (Exception e) {
            AppLog.e(this, "Failed to unregister fusedLocationProvider", e);
        }
    }

    private void unregisterFallback() {
        try {
            fallbackRegistered = false;
            locationManager.removeUpdates(locationListener);
        } catch (Exception e) {
            AppLog.e(this, "Failed to unregister locationManager", e);
        }
    }


    @Override
    public void onDestroy() {
        if (isRegistered) {
            unregisterFused();
        }
        if (fallbackRegistered) {
            unregisterFallback();
        }
        EventBus.getDefault().removeStickyEvent(Location.class);
        EventBus.getDefault().removeStickyEvent(GpsAccuracy.class);
        super.onDestroy();
    }


}
