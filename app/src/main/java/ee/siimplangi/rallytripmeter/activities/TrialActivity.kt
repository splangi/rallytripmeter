package ee.siimplangi.rallytripmeter.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants
import ee.siimplangi.rallytripmeter.services.PurchaseService
import kotlinx.android.synthetic.main.activity_trial.*

class TrialActivity : BillingActivity() {


    companion object {

        fun getIntent(ctx: Context): Intent {
            return Intent(ctx, TrialActivity::class.java);
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trial)
        explenation.text = getString(R.string.trial_activity_explenation, FirebaseRemoteConfig.getInstance().getLong(FirebaseConstants.CONFIG_TRIAL_LENGTH))
        startTrial.setOnClickListener {
            BillingActivity@ this.startTrial()
            FirebaseAnalytics.getInstance(this).logEvent(FirebaseConstants.EVENT_STARTED_TRIAL, null)
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    override fun onBinderAvailable(binder: PurchaseService.BillingServiceBinder) {
        super.onBinderAvailable(binder)

    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }


}