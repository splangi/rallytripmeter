package ee.siimplangi.rallytripmeter.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.Query
import ee.siimplangi.rallytripmeter.AppLog
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants
import ee.siimplangi.rallytripmeter.constants.StateViewConstants
import ee.siimplangi.rallytripmeter.enums.CRUDAction
import ee.siimplangi.rallytripmeter.listeners.OnStageSelectedListener
import ee.siimplangi.rallytripmeter.models.StageDef
import ee.siimplangi.rallytripmeter.viewholders.StageViewHolder
import sakout.mehdi.StateViews.StateView

/**
 * Created by Siim on 29.08.2017.
 */

class StagesActivity : AppCompatActivity(), OnStageSelectedListener {
    private lateinit var uid: String
    private lateinit var stageQuery: Query
    private lateinit var recyclerView: androidx.recyclerview.widget.RecyclerView
    private lateinit var adapter: FirebaseRecyclerAdapter<*, *>
    private lateinit var addStageButton: View
    private lateinit var defaultAction: CRUDAction
    private lateinit var stagesRef: DatabaseReference
    private lateinit var stateView: StateView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (FirebaseAuth.getInstance().currentUser == null) {
            AppLog.e(StagesActivity::class.java.simpleName, "User needs to be authenticated")
            finish()
            return
        }
        defaultAction = CRUDAction.valueOf(intent.getStringExtra(CRUD_ACTION))
        uid = FirebaseAuth.getInstance().currentUser!!.uid
        setContentView(R.layout.activity_stages)
        stateView = findViewById(R.id.wrapper)
        stateView.displayLoadingState()
        recyclerView = findViewById(R.id.recyclerView)
        stagesRef = FirebaseDatabase.getInstance().getReference(FirebaseConstants.STAGES_NODE)
        stageQuery = stagesRef.orderByChild(FirebaseConstants.USERS_NODE).equalTo(uid)
        val options = FirebaseRecyclerOptions.Builder<StageDef>().setQuery(stageQuery, StageDef::class.java).build()
        adapter = object : FirebaseRecyclerAdapter<StageDef, StageViewHolder>(options) {

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StageViewHolder {
                val v = LayoutInflater.from(parent.context).inflate(R.layout.view_stage_item, parent, false)
                return StageViewHolder(v, this@StagesActivity, defaultAction)
            }

            override fun onError(error: DatabaseError) {
                super.onError(error)
                stateView.displayState(StateViewConstants.EXCEPTION)
            }

            override fun onDataChanged() {
                super.onDataChanged()
                stateView.hideStates()
            }

            override fun onBindViewHolder(holder: StageViewHolder, position: Int, model: StageDef) {
                holder.bind(model, getRef(position).key)
            }
        }

        recyclerView.adapter = adapter
        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        addStageButton = findViewById(R.id.fab)
        addStageButton.setOnClickListener { startActivity(AddStageActivity.getIntent(this@StagesActivity, null)) }
    }

    override fun onStart() {
        super.onStart()
        adapter.startListening()
    }

    override fun onStop() {
        adapter.stopListening()
        super.onStop()
    }

    override fun onSelected(stageDef: StageDef, ref: String, action: CRUDAction) {
        when (action) {
            CRUDAction.CREATE -> throw IllegalArgumentException("Create defaultAction not supported")
            CRUDAction.READ -> {
                val data = Intent()
                data.putExtra(SPECIAL_STAGE_REF_KEY, ref)
                setResult(Activity.RESULT_OK, data)
                finish()
            }
            CRUDAction.UPDATE -> startActivity(AddStageActivity.getIntent(this, ref))
            CRUDAction.DELETE -> stagesRef!!.child(ref).removeValue()
        }
    }

    companion object {

        //public static final Sku SPECIAL_STAGE_KEY = "special_stage";
        val SPECIAL_STAGE_REF_KEY = "special_stage_ref"
        private val CRUD_ACTION = "crud"

        fun getIntent(context: Context, crudAction: CRUDAction): Intent {
            val intent = Intent(context, StagesActivity::class.java)
            intent.putExtra(CRUD_ACTION, crudAction.name)
            return intent
        }
    }

}
