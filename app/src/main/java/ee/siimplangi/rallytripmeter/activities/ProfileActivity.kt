package ee.siimplangi.rallytripmeter.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.models.UserData
import kotlinx.android.synthetic.main.activity_profile2.*

class ProfileActivity : AppCompatActivity() {

    lateinit var db: DatabaseReference
    private val currentUser = FirebaseAuth.getInstance().currentUser!!

    companion object {

        private const val EXTRA_USER_DATA = "user_data"

        fun getIntent(ctx: Context): Intent {
            return Intent(ctx, ProfileActivity::class.java)
        }

    }

    private val valueEventListener = object : ValueEventListener {

        override fun onCancelled(p0: DatabaseError) {
            Toast.makeText(this@ProfileActivity, R.string.check_internet_connection, Toast.LENGTH_SHORT).show()
            finish()
        }

        override fun onDataChange(p0: DataSnapshot) {
            val user = p0.getValue(UserData::class.java)
            if (user != null) {
                initViews(user)
            } else {
                initViews(UserData())
            }

        }

    }

    private var initialized: Boolean = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile2)
        db = FirebaseDatabase.getInstance().getReference("users/" + currentUser.uid)
        db.addListenerForSingleValueEvent(valueEventListener)
    }

    fun collectInfo(): UserData {
        return UserData(currentUser.email ?: currentUser.uid,
                driver.text.toString().trim(),
                codriver.text.toString().trim(),
                id.text.toString().trim(),
                car.text.toString().trim())
    }

    fun initViews(data: UserData?) {
        if (data != null) {
            driver.setText(data.driver)
            codriver.setText(data.codriver)
            id.setText(data.id)
            car.setText(data.vehicle)
            uid.setText(data.email ?: currentUser.uid)
            initialized = true
        } else {
            Toast.makeText(this@ProfileActivity, R.string.check_internet_connection, Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        db.removeEventListener(valueEventListener)
        db.setValue(collectInfo())
    }


}