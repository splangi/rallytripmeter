package ee.siimplangi.rallytripmeter.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ee.siimplangi.rallytripmeter.R
import kotlinx.android.synthetic.main.mock_layout.*

class MockActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        throw IllegalAccessError("THIS ACTIVITY IS A MOCK ACTIVITY, IT SHOULD NEVER BE ACCESSED!")
        setContentView(R.layout.mock_layout)
        mock.setBackgroundResource(R.drawable.mock_bg)
    }

}