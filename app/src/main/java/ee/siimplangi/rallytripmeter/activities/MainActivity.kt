package ee.siimplangi.rallytripmeter.activities

import android.Manifest
import android.app.Activity
import android.content.*
import android.content.res.Configuration
import android.database.DataSetObserver
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import com.firebase.ui.database.FirebaseListAdapter
import com.firebase.ui.database.FirebaseListOptions
import com.google.android.gms.appinvite.AppInviteInvitation
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import ee.siimplangi.rallytripmeter.AppLog
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.RallyTripmeter
import ee.siimplangi.rallytripmeter.activities.settings.SettingsActivity
import ee.siimplangi.rallytripmeter.activities.settings.UICustomizationActivity
import ee.siimplangi.rallytripmeter.adapters.FirebaseNoneAdapter
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants
import ee.siimplangi.rallytripmeter.dialogs.LikeTheAppDialog
import ee.siimplangi.rallytripmeter.enums.CRUDAction
import ee.siimplangi.rallytripmeter.enums.CustomLayoutMode
import ee.siimplangi.rallytripmeter.enums.TripType
import ee.siimplangi.rallytripmeter.events.PermissionUpdateEvent
import ee.siimplangi.rallytripmeter.events.ResetEvent
import ee.siimplangi.rallytripmeter.extensions.blankAsNull
import ee.siimplangi.rallytripmeter.extensions.prepend
import ee.siimplangi.rallytripmeter.fragments.modes.*
import ee.siimplangi.rallytripmeter.helpers.AuthHelper
import ee.siimplangi.rallytripmeter.helpers.IntentHelper
import ee.siimplangi.rallytripmeter.helpers.Settings
import ee.siimplangi.rallytripmeter.helpers.TimeHelper
import ee.siimplangi.rallytripmeter.managers.LocationSettingsManager
import ee.siimplangi.rallytripmeter.models.StageDef
import ee.siimplangi.rallytripmeter.models.UserData
import ee.siimplangi.rallytripmeter.services.BackgroundService
import ee.siimplangi.rallytripmeter.storage.Storage
import org.greenrobot.eventbus.EventBus
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, ServiceConnection {


    private var rationale: String? = null

    private lateinit var toolbar: Toolbar
    private lateinit var drawerLayout: androidx.drawerlayout.widget.DrawerLayout
    private lateinit var navigationView: NavigationView
    private lateinit var locationSettingsManager: LocationSettingsManager
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var number: TextView
    private lateinit var driver: TextView
    private lateinit var codriver: TextView
    private lateinit var trackingOn: Switch
    private lateinit var car: TextView
    private lateinit var authWrapper: View
    private lateinit var activeStageSpinner: Spinner
    private lateinit var container: View
    private lateinit var authHelper: AuthHelper

    private var currentScreen: String = RecceFragment::class.java.simpleName

    private var lastBackPressed: Long = 0L
    private val timeForSecondBackPress = 3000

    private var haveWarnedAboutExpiringTrial = false


    private var bgBinder: BackgroundService.BackgroundServiceBinder? = null

    private var uid: String? = null

    private var shutdownCalled = false;

    companion object {

        private const val KEY_HAVE_WARNED_ABOUT_EXPIERING_TRIAL = "key_expiring_trial"

        private const val REQUEST_CODE_APP_INVITES = 8
        private const val REQUEST_CODE_LOCATION_PERMISSION = 7
        private const val HAS_NOT_SEEN_DRAWER_KEY = "hasSeenDrawer2"
        private const val SERVICE_STATE = "serviceState"
        private const val CURRENT_SCREEN_KEY = "currentScreen"

        fun getIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    private var userDataDB: DatabaseReference? = null


    private val userDataEventListener = object : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            initNavHeader(dataSnapshot.getValue(UserData::class.java))
        }

        override fun onCancelled(databaseError: DatabaseError) {
            initNavHeader(null)
        }
    }

    private val authStateListener = FirebaseAuth.AuthStateListener {
        val curUser = it.currentUser
        if (curUser != null) {
            AppLog.d(this@MainActivity, "New user: ${curUser.uid}")
            uid = curUser.uid
            hideAndShowNavigationItems(curUser)
            userDataDB = FirebaseDatabase.getInstance().getReference("users/$uid")
            userDataDB?.addValueEventListener(userDataEventListener)
        } else {
            AppLog.d(this@MainActivity, "User logged out!")
            userDataDB?.removeEventListener(userDataEventListener)
            userDataDB = null
            initNavHeader(null)
            startActivity(StartActivity.getIntent(this))
            shutdownAndFinish()
        }

    }

    fun hideAndShowNavigationItems(user: FirebaseUser) {
        navigationView?.menu?.findItem(R.id.login)?.isVisible = user.isAnonymous
        navigationView?.menu?.findItem(R.id.logoff)?.isVisible = !user.isAnonymous
    }


    fun buildAdapter(uid: String): SpinnerAdapter {
        val stagesRef = FirebaseDatabase.getInstance().getReference(FirebaseConstants.STAGES_NODE)
        val stageQuery = stagesRef.orderByChild(FirebaseConstants.USERS_NODE).equalTo(uid)

        val opts = FirebaseListOptions.Builder<StageDef>().setLifecycleOwner(this).setQuery(stageQuery, StageDef::class.java).setLayout(R.layout.view_spinner_textitem).build()
        return object : FirebaseNoneAdapter<StageDef>(opts) {

            override fun getNoneView(viewGroup: ViewGroup?): View {
                return LayoutInflater.from(viewGroup!!.context).inflate(R.layout.view_spinner_textitem, viewGroup, false).also {
                    it.findViewById<TextView>(R.id.textView).setText(R.string.none)
                }
            }

            override fun populateView(v: View, model: StageDef, position: Int) {
                v.findViewById<TextView>(R.id.textView).text = getString(R.string.stage_desc, model.rallyName, model.stageName)
            }

        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenToAnalytics(currentScreen);
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        authHelper = AuthHelper()
        if (Storage.acceptedPrivacyPolicy) {
            RallyTripmeter.initializeServices(this)
        } else {
            startActivity(StartActivity.getIntent(this))
            shutdownAndFinish()
        }

        haveWarnedAboutExpiringTrial = savedInstanceState?.getBoolean(KEY_HAVE_WARNED_ABOUT_EXPIERING_TRIAL) ?: false
        BackgroundService.start(this, savedInstanceState?.getBundle(SERVICE_STATE))

        setContentView(R.layout.activity_main)
        initViews()
        setUpActionBar()
        setUpDrawer()
        initResources()

        if (savedInstanceState == null) {
            navigationView!!.setCheckedItem(R.id.recce)
            onNavigationItemSelected(R.id.recce)
            LikeTheAppDialog.show(this, supportFragmentManager)
        } else {
            currentScreen = savedInstanceState.getString(CURRENT_SCREEN_KEY, RecceFragment::class.java.simpleName);
        }
        locationSettingsManager = LocationSettingsManager(this)

        initializeActiveStageSpinner()


    }


    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    private fun hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        var options = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY else 0
        window.decorView.systemUiVisibility = options or
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_FULLSCREEN

    }

    // Shows the system bars by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private fun showSystemUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }

    fun shutdown() {
        shutdownCalled = true
        BackgroundService.stop(this)
    }

    fun shutdownAndFinish() {
        shutdown()
        finish()
    }

    override fun onDestroy() {
        if (!BackgroundService.isBackgroundEnabled() && !isChangingConfigurations) {
            BackgroundService.stop(this)
        }
        userDataDB?.removeEventListener(userDataEventListener)
        super.onDestroy()
    }

    private fun showHideNavItems() {
        val menu = navigationView!!.menu
        showHideCustomLayoutNavItem(menu, R.id.custom_1, true)
        showHideCustomLayoutNavItem(menu, R.id.custom_2, false)
        showHideCustomLayoutNavItem(menu, R.id.custom_3, false)
    }

    private fun showHideCustomLayoutNavItem(menu: Menu, menuID: Int, defValue: Boolean) {
        menu.findItem(menuID).isVisible = getSharedPreferences(CustomLayoutMode.findByNavId(menuID)!!.prefsFile).getBoolean(getString(R.string.prefs_key_enable_layout), defValue)
    }

    private fun getSharedPreferences(prefsStringId: Int): SharedPreferences {
        return getSharedPreferences(getString(prefsStringId), Context.MODE_PRIVATE)
    }

    private fun initNavHeader(userData: UserData?) {
        authWrapper.setOnClickListener {
            startActivity(ProfileActivity.getIntent(this@MainActivity))
        }
        driver.text = userData?.driver?.blankAsNull() ?: getString(R.string.anonymous)
        codriver.text = userData?.codriver?.blankAsNull() ?: getString(R.string.anonymous)
        car.text = userData?.vehicle?.blankAsNull() ?: getString(R.string.unknown)
        number.text = (userData?.id?.blankAsNull()?.prepend("#")
                ?: getString(R.string.NotAvailable))
        trackingOn.isChecked = Settings.isTrackingOn
        trackingOn.setOnCheckedChangeListener { _, isChecked ->
            Settings.isTrackingOn = isChecked
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE_APP_INVITES) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                try {
                    val ids = AppInviteInvitation.getInvitationIds(resultCode, data)
                    for (id in ids) {
                        FirebaseAnalytics.getInstance(this).logEvent(FirebaseConstants.EVENT_INVITE, Bundle().also { it.putString("ID", id) });
                    }
                    AppLog.i(MainActivity::class.java.simpleName, "App invite successful!")
                } catch (e: Exception) {
                    AppLog.e(MainActivity::class.java.simpleName, "Failed to log appinvites")
                }

            }
        } else {
            authHelper.onActivityResult(requestCode, resultCode, data, object : AuthHelper.OnAuthResultListener {
                override fun onAuthResult(result: AuthHelper.AuthResult) {
                    when (result) {
                        AuthHelper.AuthResult.ERROR, AuthHelper.AuthResult.UPDATE_REQUIRED, AuthHelper.AuthResult.NO_NETWORK -> Toast.makeText(this@MainActivity, R.string.exception_message, Toast.LENGTH_SHORT).show()
                    }
                }

            })
        }

    }

    private fun sendScreenToAnalytics(screen: String) {
        this.currentScreen = screen
        FirebaseAnalytics.getInstance(this).setCurrentScreen(this, screen, screen)
    }


    private fun initResources() {
        rationale = getString(R.string.location_rationale)
    }

    private fun initViews() {
        container = findViewById(R.id.container)
        drawerLayout = findViewById(R.id.drawer)
        navigationView = findViewById(R.id.navigation)
        navigationView!!.setNavigationItemSelectedListener(this)
        toolbar = findViewById(R.id.toolbar)
        val headerView = navigationView!!.getHeaderView(0)
        activeStageSpinner = headerView.findViewById(R.id.activeStage)
        //logOff = headerView.findViewById(R.id.logout)
        driver = headerView.findViewById(R.id.driver)
        codriver = headerView.findViewById(R.id.codriver)
        car = headerView.findViewById(R.id.car)
        number = headerView.findViewById(R.id.number)
        trackingOn = headerView.findViewById(R.id.trackingOn)
        authWrapper = headerView

    }

    private fun setUpActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    private fun setUpDrawer() {
        toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer)
        toggle!!.isDrawerIndicatorEnabled = true
        drawerLayout!!.addDrawerListener(toggle!!)
        if (getPreferences(Context.MODE_PRIVATE).getBoolean(HAS_NOT_SEEN_DRAWER_KEY, true)) {
            drawerLayout!!.openDrawer(GravityCompat.START, true)
            getPreferences(Context.MODE_PRIVATE).edit().putBoolean(HAS_NOT_SEEN_DRAWER_KEY, false).apply()
        }


    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        toggle!!.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        toggle!!.onConfigurationChanged(newConfig)
    }


    private fun onNavigationItemSelected(id: Int): Boolean {
        val fragment: androidx.fragment.app.Fragment
        when (id) {
            R.id.recce -> fragment = RecceFragment()
            R.id.liaison -> fragment = LiaisonFragment()
            R.id.tsd -> fragment = TSDFragment()
            R.id.racing -> fragment = RaceFragment()
            R.id.tsd_advanced -> fragment = TSDAdvancedFragment()
            R.id.custom_1, R.id.custom_2, R.id.custom_3 -> fragment = CustomLayout.newInstance(CustomLayoutMode.findByNavId(id)!!)
            R.id.customize_ui -> {
                startActivity(UICustomizationActivity.getIntent(this))
                return false
            }
            R.id.tester -> {
                AlertDialog.Builder(this)
                        .setTitle(getString(R.string.become_a_tester))
                        .setMessage(getString(R.string.become_a_tester_message))
                        .setPositiveButton(getString(R.string.join)) { dialog, _ ->
                            IntentHelper.startWebsiteActivity(MainActivity@ this, getString(R.string.beta_opt_in_link))
                            dialog.dismiss()
                        }
                        .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.dismiss() }
                        .show()
                return false;
            }
            R.id.weather -> {
                startActivity(WeatherActivity.WeatherDetailsActivity.getIntent(this))
                return false
            }
            R.id.login -> {
                authHelper.startAuthUIActivity(this)
                return true;
            }
            R.id.logoff -> {
                AuthHelper.signOut(this, OnCompleteListener {});
                return false;
            }

            R.id.stages -> {
                startActivity(StagesActivity.getIntent(this, CRUDAction.UPDATE))
                return false
            }
            R.id.invite -> {
                val intent = AppInviteInvitation.IntentBuilder(getString(R.string.invite))
                        .setMessage(getString(R.string.appinvite_message))
                        .setDeepLink(Uri.parse(getString(R.string.appinvite_deep_link)))
                        .setCustomImage(Uri.parse(getString(R.string.appinvite_custom_image)))
                        .setCallToActionText(getString(R.string.appinvite_call_to_action)).build()
                startActivityForResult(intent, REQUEST_CODE_APP_INVITES)
                return false
            }
            R.id.feedback -> {
                IntentHelper.startEmailActivity(this, getString(R.string.feedback))
                return false
            }
            R.id.settings -> {
                startActivity(SettingsActivity.getIntent(this))
                return false
            }
            R.id.policy -> {
                IntentHelper.startWebsiteActivity(this, getString(R.string.privacy_policy_address))
                return false
            }
            R.id.manual -> {
                IntentHelper.startWebsiteActivity(this, getString(R.string.user_manual_address))
                return false;
            }
            R.id.exit -> {
                shutdownAndFinish()
                return false;
            }
            R.id.language -> {
                AlertDialog.Builder(this)
                        .setTitle(getString(R.string.not_your_language))
                        .setMessage(getString(R.string.translate_message))
                        .setPositiveButton(getString(R.string.help)) { dialog, _ ->
                            IntentHelper.startWebsiteActivity(this, getString(R.string.translate_url))
                            dialog.dismiss()
                        }
                        .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.dismiss() }
                        .show()
                return false
            }
            else -> throw IllegalStateException("No Fragments to show")
        }
        supportFragmentManager.beginTransaction().replace(R.id.container, fragment).commitAllowingStateLoss()
        sendScreenToAnalytics(fragment::class.java.simpleName)
        drawerLayout!!.closeDrawers()
        return true
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return onNavigationItemSelected(item.itemId)
    }

    override fun onBackPressed() {
        val currentTime = TimeHelper.getElapsedTime()
        if (drawerLayout!!.isDrawerVisible(GravityCompat.START)) {
            drawerLayout!!.closeDrawers()
        } else if (currentTime - lastBackPressed > timeForSecondBackPress) {
            Toast.makeText(this, getString(R.string.back_twice_to_exit), Toast.LENGTH_SHORT).also {
                val view = it.view.findViewById<TextView>(android.R.id.message)
                view?.let {
                    view.gravity = Gravity.CENTER
                }
            }.show()
            lastBackPressed = currentTime
        } else {
            super.onBackPressed()
            shutdown()

        }
    }

    fun setBGColor() {
        drawerLayout.setBackgroundColor(Settings.bgColor)
    }

    override fun onStart() {
        super.onStart()
        bindService(BackgroundService.getIntent(this), this, Context.BIND_AUTO_CREATE or Context.BIND_IMPORTANT)
        FirebaseAuth.getInstance().addAuthStateListener(authStateListener)
        hideSystemUI()
        showHideNavItems()
        val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION).let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P && Settings.runInBackground) {
                it.plus(Manifest.permission.FOREGROUND_SERVICE)
            } else {
                it
            }
        }
        EasyPermissions.requestPermissions(this, rationale!!, REQUEST_CODE_LOCATION_PERMISSION, *permissions)
        setBGColor()
        locationSettingsManager!!.onStart()
    }

    @AfterPermissionGranted(REQUEST_CODE_LOCATION_PERMISSION)
    fun connectBackgroundService() {
        EventBus.getDefault().post(PermissionUpdateEvent())
        locationSettingsManager!!.requestEnableLocation()
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (bgBinder != null && bgBinder!!.backgroundManager != null) {
            val bundle = Bundle()
            bgBinder!!.backgroundManager!!.onSaveInstanceState(bundle)
            outState.putBundle(SERVICE_STATE, bundle)
        }
        outState.putString(CURRENT_SCREEN_KEY, currentScreen)
        outState.putBoolean(KEY_HAVE_WARNED_ABOUT_EXPIERING_TRIAL, haveWarnedAboutExpiringTrial)
    }

    override fun onStop() {
        FirebaseAuth.getInstance().removeAuthStateListener(authStateListener)
        locationSettingsManager!!.onStop()
        unbindService(this)
        super.onStop()
    }


    override fun onServiceConnected(name: ComponentName, service: IBinder) {
        bgBinder = service as BackgroundService.BackgroundServiceBinder
    }

    override fun onServiceDisconnected(name: ComponentName) {
        bgBinder = null
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        val action = event.action
        val keyCode = event.keyCode
        when (keyCode) {
            KeyEvent.KEYCODE_VOLUME_UP, KeyEvent.KEYCODE_MEDIA_NEXT -> {
                if (action == KeyEvent.ACTION_DOWN) {
                    EventBus.getDefault().post(ResetEvent(TripType.TRIP1))
                }
                return true
            }
            KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_MEDIA_PREVIOUS -> {
                if (action == KeyEvent.ACTION_DOWN && event.isLongPress) {
                    EventBus.getDefault().post(ResetEvent(TripType.TRIP2))
                    EventBus.getDefault().post(ResetEvent(TripType.TRIP1))
                }
                return true
            }
            else -> return super.dispatchKeyEvent(event)
        }

    }


    private val onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            Log.d(MainActivity::class.java.simpleName, "SpinnerListener - onNothingSelected")
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            if (position == 0) {
                Storage.activeStage = null

                Log.d(MainActivity::class.java.simpleName, "SpinnerListener - onNoneSelected")
            } else {
                parent?.adapter?.also {
                    (it as FirebaseListAdapter<StageDef>).also {
                        Storage.activeStage = it.getRef(position).key
                        Log.d(MainActivity::class.java.simpleName, "SpinnerListener - onItemSelected")
                    }
                }

            }

        }
    }

    private val dataSetObservable: DataSetObserver = object : DataSetObserver() {

        override fun onChanged() {
            super.onChanged()
            if (activeStageSpinner.adapter.count > 1) {
                Log.d(MainActivity::class.java.simpleName, "Synchronizing spinner state with Preferences")
                syncSpinner()
                activeStageSpinner.onItemSelectedListener = onItemSelectedListener
            } else if (activeStageSpinner.adapter.count <= 1) {
                activeStageSpinner.onItemSelectedListener = null
            }

        }
    }


    private fun syncSpinner() {
        val activeStage = Storage.activeStage
        // Try to sync the adapter
        if (activeStage == null && activeStageSpinner.selectedItemId != 0L) {
            activeStageSpinner.setSelection(0)
        } else if (activeStage != null && activeStageSpinner.selectedItemId != activeStage.hashCode().toLong()) {
            for (i in 0 until activeStageSpinner.adapter.count) {
                if (activeStageSpinner.adapter.getItemId(i) == activeStage.hashCode().toLong()) {
                    activeStageSpinner.setSelection(i)
                    break
                }
            }
        }
    }

    private fun initializeActiveStageSpinner() {
        FirebaseAuth.getInstance().uid?.also {
            activeStageSpinner.adapter = buildAdapter(it)
            activeStageSpinner.adapter.registerDataSetObserver(dataSetObservable)
        }
    }


}