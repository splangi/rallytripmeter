package ee.siimplangi.rallytripmeter.activities

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.IBinder
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.services.PurchaseService

/**
 * Created by Siim on 09.09.2017.
 */

open class BillingActivity : AppCompatActivity() {

    var binder: PurchaseService.BillingServiceBinder? = null

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            binder = iBinder as PurchaseService.BillingServiceBinder
            onBinderAvailable(iBinder)
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            binder = null
        }
    }

    open fun onBinderAvailable(binder: PurchaseService.BillingServiceBinder) {

    }

    override fun onResume() {
        super.onResume()
        startService(PurchaseService.getIntent(this))
        bindService(PurchaseService.getIntent(this), serviceConnection, Context.BIND_AUTO_CREATE or Context.BIND_IMPORTANT)
    }

    override fun onPause() {
        unbindService(serviceConnection)
        super.onPause()
    }


    fun showErrorSnackBar(tryAgainAction: (View) -> Unit) {
        Snackbar.make(window.decorView, R.string.service_unavailable, Snackbar.LENGTH_SHORT).setAction(R.string.try_again, tryAgainAction)
    }

    fun safeCommunicateBinder(action: (PurchaseService.BillingServiceBinder) -> Unit) {
        val binder = this.binder
        if (binder == null) {
            showErrorSnackBar { _ -> safeCommunicateBinder(action) }
        } else {
            action.invoke(binder)
        }
    }

    fun startTrial() {
        safeCommunicateBinder { binder ->
            binder.startTrial()
        }
    }

    fun launchBillingFlow(skuID: String, skuType: String) {
        safeCommunicateBinder { binder ->
            binder.launchBillingFlow(this, skuID, skuType)
        }
    }


}
