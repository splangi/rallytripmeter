package ee.siimplangi.rallytripmeter.activities.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.annotation.XmlRes
import androidx.appcompat.app.AppCompatActivity
import ee.siimplangi.rallytripmeter.AppLog
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.fragments.settings.ComponentSettingsFragment

/**
 * Created by Siim on 10.09.2017.
 */

class ComponentSettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        title = try {
            getString(R.string.customize_x_component, getString(intent.getIntExtra(HEADING, 0)))
        } catch (e: RuntimeException) {
            AppLog.e(this, "Failed to set title!", e)
            getString(R.string.customize_ui)
        }

        if (savedInstanceState == null) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.wrapper,
                            ComponentSettingsFragment.newInstance(intent.getStringExtra(getString(R.string.prefs_file_key)), intent.getIntArrayExtra(KEY_XML)),
                            ComponentSettingsFragment::class.java.name)
                    .commit()
        }

    }

    companion object {

        private const val KEY_XML = "xml_files"
        private const val HEADING = "heading"

        fun getIntent(context: Context, @StringRes heading: Int, @StringRes value: Int, @XmlRes vararg xmlValues: Int): Intent {
            val intent = Intent(context, ComponentSettingsActivity::class.java)
            intent.putExtra(context.getString(R.string.prefs_file_key), context.getString(value))
            intent.putExtra(HEADING, heading)
            intent.putExtra(KEY_XML, xmlValues)
            return intent
        }
    }

}
