package ee.siimplangi.rallytripmeter.activities

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.analytics.FirebaseAnalytics
import ee.siimplangi.rallytripmeter.AppLog
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.RallyTripmeter
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants
import ee.siimplangi.rallytripmeter.enums.ProductStatus
import ee.siimplangi.rallytripmeter.extensions.registerSafe
import ee.siimplangi.rallytripmeter.extensions.unregisterSafe
import ee.siimplangi.rallytripmeter.helpers.AuthHelper
import ee.siimplangi.rallytripmeter.services.PurchaseService
import ee.siimplangi.rallytripmeter.storage.Storage
import kotlinx.android.synthetic.main.activity_start.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class StartActivity : AppCompatActivity() {

    var binder: PurchaseService.BillingServiceBinder? = null
    private var serviceBound: Boolean = false;

    private var lastStatus: ProductStatus? = null
        set(value) {
            if (value == null) {
                field = null
            } else if (field != value) {
                field = value
                Log.d(StartActivity::class.java.simpleName, "AccessDetails: ${value.name}")
                when (value) {
                    ProductStatus.NOT_LOGGED_IN -> {
                        flipper.displayedChild = 1
                    }
                    ProductStatus.TRIAL_NOT_STARTED -> {
                        startActivityForResult(TrialActivity.getIntent(this), REQUEST_START_TRIAL)
                    }
                    ProductStatus.TRIAL_EXPIRED -> {
                        startActivityForResult(PurchaseActivity.getIntent(this), REQUEST_START_PURCHASE)
                    }
                    ProductStatus.TRIAL_ABOUT_TO_EXPIRE -> {
                        startActivityForResult(PurchaseActivity.getIntent(this), REQUEST_START_PURCHASE_EARLY)

                    }
                    ProductStatus.VALID_BILLING -> startMainActivity()
                    ProductStatus.TRIAL_VALID -> startMainActivity()
                }
            }
        }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            binder = iBinder as PurchaseService.BillingServiceBinder
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            binder = null
        }
    }

    val authHelper: AuthHelper = AuthHelper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(StartActivity::class.java.simpleName, "onCreate: ${this.toString()}")
        setContentView(R.layout.activity_start)
        logIn.setOnClickListener {
            flipper.displayedChild = 0
            authHelper.startAuthUIActivity(this)

        }
        logInAnonymously.setOnClickListener {
            flipper.displayedChild = 0
            AuthHelper.signInAnonymously()
            FirebaseAnalytics.getInstance(this).logEvent(FirebaseConstants.EVENT_LOGGED_IN_ANONYMOUSLY, null);
        }
        accept.setOnClickListener {
            flipper.displayedChild = 0
            RallyTripmeter.initializeServices(this)
            Storage.acceptedPrivacyPolicy = true
            EventBus.getDefault().registerSafe(StartActivity@ this)
            bindServiceSafe()
        }
        if (!Storage.acceptedPrivacyPolicy) {
            privacyPolicyText.movementMethod = LinkMovementMethod.getInstance()
            privacyPolicyText.text = Html.fromHtml(getString(R.string.privacy_policy_text, getString(R.string.privacy_policy_address)))
            flipper.displayedChild = 2
        } else {
            RallyTripmeter.initializeServices(this)
        }

    }

    override fun finish() {
        super.finish()
        if (!Storage.acceptedPrivacyPolicy) {
            // DO nothing!
        } else if (!AuthHelper.isLoggedIn) {
            FirebaseAnalytics.getInstance(this).logEvent(FirebaseConstants.EVENT_DID_NOT_LOG_IN, null)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

    }

    private fun bindServiceSafe() {
        if (!serviceBound) {
            startService(PurchaseService.getIntent(this))
            bindService(PurchaseService.getIntent(this), serviceConnection, Context.BIND_AUTO_CREATE or Context.BIND_IMPORTANT)
            serviceBound = true
        }

    }

    private fun unBindServiceSafe() {
        if (serviceBound) {
            unbindService(serviceConnection)
            serviceBound = false
        }
    }


    override fun onResume() {
        if (Storage.acceptedPrivacyPolicy) {
            EventBus.getDefault().registerSafe(this)
            bindServiceSafe()
        }
        super.onResume()
    }

    override fun onPause() {
        EventBus.getDefault().unregisterSafe(this)
        lastStatus = null
        unBindServiceSafe()
        super.onPause()
    }

    @Subscribe(sticky = true)
    fun onAccessDetailsUpdated(status: ProductStatus) {
        this.lastStatus = status
    }

    private fun startMainActivity() {
        EventBus.getDefault().unregisterSafe(this)
        startActivity(MainActivity.getIntent(this))
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        finish()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_START_TRIAL && resultCode == Activity.RESULT_CANCELED) {
            finish()
        } else if (requestCode == REQUEST_START_PURCHASE && resultCode == Activity.RESULT_CANCELED) {
            finish()
        } else if (requestCode == REQUEST_START_PURCHASE_EARLY && resultCode == Activity.RESULT_CANCELED) {
            startMainActivity()
        } else {
            authHelper.onActivityResult(requestCode, resultCode, data, object : AuthHelper.OnAuthResultListener {
                override fun onAuthResult(result: AuthHelper.AuthResult) {
                    val toastText: Int = when (result) {
                        AuthHelper.AuthResult.ERROR -> R.string.exception_message
                        AuthHelper.AuthResult.NO_NETWORK -> R.string.check_internet_connection
                        AuthHelper.AuthResult.UPDATE_REQUIRED -> {
                            FirebaseAnalytics.getInstance(this@StartActivity).logEvent(FirebaseConstants.EVENT_NEEDS_GOOGLE_PLAY_UPDATES, null)
                            R.string.common_google_play_services_update_text

                        }
                        AuthHelper.AuthResult.ANONYMOUS_UPGRADE_FAILED -> {
                            AppLog.e(StartActivity@ this, "Tried to anonymous upgrade, but this was not supposed to happen")
                            return
                        }
                        AuthHelper.AuthResult.CANCELED -> {
                            flipper.displayedChild = 1
                            return
                        }
                        AuthHelper.AuthResult.AUTHENTICATED -> {
                            FirebaseAnalytics.getInstance(this@StartActivity).logEvent(FirebaseConstants.EVENT_LOGGED_IN, null)
                            return
                        }

                    }
                    flipper.displayedChild = 1
                    Toast.makeText(this@StartActivity, toastText, Toast.LENGTH_SHORT).show()
                }
            })
        }

    }

    companion object {
        private const val REQUEST_START_PURCHASE = 10
        private const val REQUEST_START_PURCHASE_EARLY = 11
        private const val REQUEST_START_TRIAL = 9

        fun getIntent(ctx: Context): Intent {
            return Intent(ctx, StartActivity::class.java)
        }
    }

}