package ee.siimplangi.rallytripmeter.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.SkuDetails
import com.android.billingclient.api.SkuDetailsResponseListener
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants
import ee.siimplangi.rallytripmeter.constants.StateViewConstants
import ee.siimplangi.rallytripmeter.enums.ProductStatus
import ee.siimplangi.rallytripmeter.helpers.IntentHelper
import ee.siimplangi.rallytripmeter.models.TrialModel
import ee.siimplangi.rallytripmeter.services.PurchaseService
import kotlinx.android.synthetic.main.activity_purchase.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import sakout.mehdi.StateViews.StateView

class PurchaseActivity : BillingActivity() {

    lateinit var stateView: StateView

    companion object {

        fun getIntent(ctx: Context): Intent {
            return Intent(ctx, PurchaseActivity::class.java)
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_purchase)
        stateView = findViewById(R.id.stateView)
        stateView.displayLoadingState()
        stateView.setOnStateButtonClicked {
            binder?.reconnect()
            querySkuDetails(binder)
        }
        emailButton.setOnClickListener {
            IntentHelper.startEmailActivity(PurchaseActivity@ this, getString(R.string.about_rally_tripmeter))
        }

    }


    override fun onBinderAvailable(binder: PurchaseService.BillingServiceBinder) {
        super.onBinderAvailable(binder)
        querySkuDetails(binder)
    }

    @Subscribe(sticky = true)
    fun onNewAccessDetail(productStatus: ProductStatus) {
        when (productStatus) {
            ProductStatus.TRIAL_NOT_STARTED -> {
                Log.e(PurchaseActivity::class.java.simpleName, "Trial was not even started, wrong screen?")
                setResult(Activity.RESULT_OK)
                finish()
            }
            ProductStatus.TRIAL_VALID -> {
                Log.e(PurchaseActivity::class.java.simpleName, "Trial was valid, wrong screen?")
                setResult(Activity.RESULT_OK)
                finish()
            }
            ProductStatus.VALID_BILLING -> {
                FirebaseAnalytics.getInstance(this).logEvent(FirebaseConstants.EVENT_VALID_BILLING, null);
                // Yay, we have a paying customer! Thank you lord for that beautiful soul!
                setResult(Activity.RESULT_OK)
                finish()
            }
            ProductStatus.TRIAL_EXPIRED -> {
                FirebaseAnalytics.getInstance(this).logEvent(FirebaseConstants.EVENT_TRIAL_OVER, null);
                initTrialExpired()
            }
            ProductStatus.TRIAL_ABOUT_TO_EXPIRE -> {
                FirebaseAnalytics.getInstance(this).logEvent(FirebaseConstants.EVENT_TRIAL_ALMOST_OVER, null);
                initTrialAboutToExpire(binder?.getTrialModel())
            }
        }
    }


    private fun initTrialExpired() {
        trialTitle.text = getString(R.string.your_trial_has_expired)
        trialIcon.setImageResource(R.drawable.ic_times_up)
    }

    private fun initTrialAboutToExpire(trialModel: TrialModel?) {
        trialTitle.text = getString(R.string.your_trial_expires_in_x_days, trialModel?.getTimeToExpireInDays()
                ?: 0L)
        trialIcon.setImageResource(R.drawable.ic_hourglass_ticking)
    }

    private fun querySkuDetails(binder: PurchaseService.BillingServiceBinder?) {
        binder?.getSkuDetails(SkuDetailsResponseListener { responseCode, skuDetailsList ->
            when (responseCode) {
                BillingClient.BillingResponse.OK -> initButtons(skuDetailsList)
                BillingClient.BillingResponse.SERVICE_UNAVAILABLE -> stateView.displayState(StateViewConstants.INTERNET_OFF)
                else -> stateView.displayState(StateViewConstants.EXCEPTION)
            }
        }) ?: stateView.displayState(StateViewConstants.EXCEPTION)
    }


    private fun initButtons(skuDetailList: List<SkuDetails>) {
        val lifetimeSku = FirebaseRemoteConfig.getInstance().getString(FirebaseConstants.CONFIG_ACTIVE_LIFETIME)
        val discountLifetimeSku = FirebaseRemoteConfig.getInstance().getString(FirebaseConstants.CONFIG_ACTIVE_LIFETIME_DISCOUNT)
        val yearlySku = FirebaseRemoteConfig.getInstance().getString(FirebaseConstants.CONFIG_ACTIVE_YEARLY)
        val quarterlySku = FirebaseRemoteConfig.getInstance().getString(FirebaseConstants.CONFIG_ACTIVE_QUARTERLY)
        initButtonSubs(R.id.yearly, getString(R.string.yearly), skuDetailList.find { it.sku == yearlySku })
        initButtonSubs(R.id.quarterly, getString(R.string.quarterly), skuDetailList.find { it.sku == quarterlySku })
        initButtonInApp(R.id.lifetime, getString(R.string.lifetime), skuDetailList.find { it.sku == lifetimeSku }, skuDetailList.find { it.sku == discountLifetimeSku })
        stateView.hideStates()
    }

    private fun initButtonInApp(id: Int, headline: String, skuDetails: SkuDetails?, skuDetailsDiscount: SkuDetails?) {
        val viewGroup = findViewById<ViewGroup>(id)
        if (skuDetails == null) {
            viewGroup.visibility = View.GONE
        } else {
            initButton(viewGroup, skuDetails.sku, skuDetails.type, headline, skuDetails.price, skuDetailsDiscount?.price)
            viewGroup.visibility = View.VISIBLE
            viewGroup.setOnClickListener {
                launchBillingFlow(skuDetailsDiscount?.sku
                        ?: skuDetails.sku, skuDetailsDiscount?.type ?: skuDetails.type)
            }
        }
    }

    private fun initButtonSubs(id: Int, headline: String, skuDetails: SkuDetails?) {
        val viewGroup = findViewById<ViewGroup>(id)
        if (skuDetails == null) {
            viewGroup.visibility = View.GONE
        } else {
            initButton(viewGroup, skuDetails.sku, skuDetails.type, headline, skuDetails.price, skuDetails.introductoryPrice)
            viewGroup.visibility = View.VISIBLE
            viewGroup.setOnClickListener {
                launchBillingFlow(skuDetails.sku, skuDetails.type)
            }
        }
    }

    private fun initButton(viewGroup: ViewGroup, sku: String, skuType: String, headline: String, realPrice: String, discountPrice: String?) {
        val realPriceText = viewGroup.findViewById<TextView>(R.id.realPrice)
        val stripedThroughText = viewGroup.findViewById<TextView>(R.id.stripedTroughText)
        val headlineText = viewGroup.findViewById<TextView>(R.id.title)
        headlineText.text = headline
        viewGroup.findViewById<TextView>(R.id.title).text = headline
        stripedThroughText.paintFlags = stripedThroughText.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        if (discountPrice != null) {
            realPriceText.text = discountPrice
            stripedThroughText.text = realPrice
            stripedThroughText.visibility = View.VISIBLE
        } else {
            stripedThroughText.visibility = View.GONE
            realPriceText.text = realPrice
        }
        viewGroup.visibility = View.VISIBLE

    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }

}