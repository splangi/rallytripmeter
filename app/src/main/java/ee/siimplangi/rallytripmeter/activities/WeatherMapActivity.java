package ee.siimplangi.rallytripmeter.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.ContentLoadingProgressBar;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants;
import ee.siimplangi.rallytripmeter.helpers.Settings;

/**
 * Created by Siim on 28.09.2017.
 */

public class WeatherMapActivity extends AppCompatActivity {

    private WebView webView;
    private ContentLoadingProgressBar progressBar;

    public static Intent getIntent(Context context) {
        return new Intent(context, WeatherMapActivity.class);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        webView = findViewById(R.id.webContainer);
        progressBar = findViewById(R.id.progress);
        initWebView();
    }

    private void initWebView() {
        progressBar.show();
        webView.setVisibility(View.GONE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.hide();
                view.setVisibility(View.VISIBLE);
                view.loadUrl("javascript:(function() { " +
                        "document.getElementById('slider').style.display='none';})()");
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, true);
            }


        });
        webView.getSettings().setGeolocationEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        try {
            LocationServices.getFusedLocationProviderClient(this).getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    String url = getUrl(location, Settings.INSTANCE.getRallyTime().getRallyTimeCalendar());
                    webView.loadUrl(url);
                }
            });

        } catch (SecurityException e) {
            String url = getUrl(EventBus.getDefault().getStickyEvent(Location.class), Settings.INSTANCE.getRallyTime().getRallyTimeCalendar());
            webView.loadUrl(url);
        }
    }

    private String getUrl(Location location, Calendar calendar) {
        if (location == null || calendar == null) {
            return FirebaseRemoteConfig.getInstance().getValue(FirebaseConstants.CONFIG_WEATHER).asString();
        }
        String url = FirebaseRemoteConfig.getInstance().getValue(FirebaseConstants.CONFIG_WEATHER_DETAILS).asString();
        url = url.replace("$lat", location.getLatitude() + "");
        url = url.replace("$lng", location.getLongitude() + "");
        url = url.replace("$year", calendar.get(Calendar.YEAR) + "");
        url = url.replace("$month", calendar.get(Calendar.MONTH) + "");
        url = url.replace("$day", calendar.get(Calendar.DAY_OF_MONTH) + "");
        return url;
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }

    }


}
