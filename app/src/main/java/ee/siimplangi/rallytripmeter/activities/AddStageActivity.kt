package ee.siimplangi.rallytripmeter.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import ee.siimplangi.rallytripmeter.AppLog
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.adapters.LimitAdapter
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants
import ee.siimplangi.rallytripmeter.constants.StateViewConstants
import ee.siimplangi.rallytripmeter.enums.Unit
import ee.siimplangi.rallytripmeter.extensions.validate
import ee.siimplangi.rallytripmeter.helpers.Settings
import ee.siimplangi.rallytripmeter.helpers.converters.UnitConverter
import ee.siimplangi.rallytripmeter.models.FirebaseTimeStamp
import ee.siimplangi.rallytripmeter.models.Section
import ee.siimplangi.rallytripmeter.models.StageDef
import ee.siimplangi.rallytripmeter.viewholders.SectionViewHolder
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import sakout.mehdi.StateViews.StateView
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

/**
 * Created by Siim on 30.08.2017.
 */

class AddStageActivity : AppCompatActivity(), ValueEventListener {

    private var stageNodeId: String? = null
    private lateinit var stateView: StateView
    private lateinit var addSpecialStageButton: View
    private lateinit var ref: DatabaseReference
    private lateinit var limits: MutableList<Section>
    private lateinit var specialStageName: EditText
    private lateinit var rallyName: EditText
    private lateinit var speedLimits: androidx.recyclerview.widget.RecyclerView
    private lateinit var adapter: LimitAdapter
    private lateinit var addLimitButton: View
    var postClick: View.OnClickListener = View.OnClickListener { postData() }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_stage)
        stageNodeId = intent.getStringExtra(KEY_SPECIAL_STAGE)
        ref = FirebaseDatabase.getInstance().getReference(FirebaseConstants.STAGES_NODE)
        initViews()
        when {
            savedInstanceState != null -> init(savedInstanceState.getParcelable(KEY_SPECIAL_STAGE) as StageDef)
            stageNodeId != null -> {
                ref.child(stageNodeId!!).addListenerForSingleValueEvent(this)
                stateView.displayLoadingState()
            }
            else -> init(StageDef())
        }
    }

    private fun initViews() {
        stateView = findViewById(R.id.wrapper)
        specialStageName = findViewById(R.id.stageName)
        //specialStageLength = findViewById(R.id.stageLength)
        rallyName = findViewById(R.id.rallyName)
        addLimitButton = findViewById(R.id.addLimit)
        speedLimits = findViewById(R.id.speedLimits)
        addLimitButton.setOnClickListener {
            limits.add(Section())
            adapter.notifyItemInserted(limits.size - 1)
        }
        //maxSpeed = findViewById(R.id.maxSpeedMs)
        addSpecialStageButton = findViewById(R.id.fab)
        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        layoutManager.isAutoMeasureEnabled = true
        speedLimits.layoutManager = layoutManager
        addSpecialStageButton.setOnClickListener(postClick)
        stateView.setOnStateButtonClicked(postClick)

    }

    private fun init(stageDef: StageDef) {
        val speedFormat = DecimalFormat("0")
        val distanceFormat = DecimalFormat("0.00", DecimalFormatSymbols(Locale.US))
        val unitConverter = UnitConverter(Unit.METRIC, Settings.unit)
        specialStageName.setText(stageDef.stageName)
        //specialStageLength.setText(Tools.formatFloat(stageDef.getStageLengthMeters(), distanceFormat, UnitConverter.METERS_TO_KILOMETERS));
        //maxSpeed.setText(Tools.formatFloat(stageDef.getMaxSpeedMs(), speedFormat, unitConverter, UnitConverter.MPS_TO_KPH));
        rallyName.setText(stageDef.rallyName)
        limits = stageDef.sections.toMutableList()
        adapter = LimitAdapter(limits)
        speedLimits.adapter = adapter
        stateView.hideStates()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(KEY_SPECIAL_STAGE, collectInfo())
    }

    private fun validateFields(): Boolean {
        var valid = rallyName.validate({ it.isNotBlank() })
        valid = valid && specialStageName.validate({ it.isNotBlank() })
        var lastDistance = 0.0
        for (i in 0 until adapter.itemCount) {
            if (i > 0) {
                lastDistance = limits[i - 1].to?.toDouble() ?: lastDistance
            }
            val vh = speedLimits.findViewHolderForAdapterPosition(i) as SectionViewHolder
            valid = valid && vh.validateAndSetError(lastDistance)
            vh.saveData()
        }
        return valid
    }

    private fun collectInfo(): StageDef {
        for (i in 0 until adapter.itemCount) {
            val vh = speedLimits.findViewHolderForAdapterPosition(i) as SectionViewHolder
            vh.saveData()
        }
        return StageDef(
                FirebaseAuth.getInstance().currentUser!!.uid,
                specialStageName.text.toString().trim(),
                rallyName.text.toString().trim(),
                limits,
                FirebaseTimeStamp.getServerTimeStamp()

        )
    }

    @Subscribe
    fun onUnitChange(unit: Unit) {
        adapter.notifyDataSetChanged()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    fun postData() {
        if (validateFields()) {
            stateView.displayLoadingState()
            val postingRef: DatabaseReference
            if (stageNodeId == null) {
                postingRef = ref.push()
            } else {
                postingRef = ref.child(stageNodeId!!)
            }
            postingRef.setValue(collectInfo()).addOnSuccessListener {
                finish()
            }.addOnFailureListener { e ->
                stateView.displayState(StateViewConstants.EXCEPTION)
                AppLog.e(AddStageActivity::class.java.simpleName, "Failed to post value", e)
            }
        } else {
            stateView.hideStates()
        }

    }


    override fun onDataChange(dataSnapshot: DataSnapshot) {
        init(dataSnapshot.getValue(StageDef::class.java)!!)
        stateView.hideStates()

    }

    override fun onCancelled(databaseError: DatabaseError) {
        stateView.displayState(StateViewConstants.EXCEPTION)
    }

    companion object {

        private val KEY_SPECIAL_STAGE = "specialStage"

        fun getIntent(context: Context, stageNodeId: String? = null): Intent {
            val intent = Intent(context, AddStageActivity::class.java)
            intent.putExtra(KEY_SPECIAL_STAGE, stageNodeId)
            return intent
        }
    }
}
