package ee.siimplangi.rallytripmeter.activities.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.fragments.settings.CustomLayoutSettingFragment
import ee.siimplangi.rallytripmeter.fragments.settings.UICustomizationFragment

/**
 * Created by Siim on 10.09.2017.
 */

class UICustomizationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        if (savedInstanceState == null) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.wrapper,
                            UICustomizationFragment.newInstance(),
                            CustomLayoutSettingFragment::class.java.name)
                    .commit()
        }

    }

    companion object {

        fun getIntent(context: Context): Intent {
            val intent = Intent(context, UICustomizationActivity::class.java)
            return intent
        }
    }

}
