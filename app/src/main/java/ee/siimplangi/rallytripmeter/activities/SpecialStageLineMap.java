package ee.siimplangi.rallytripmeter.activities;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import ee.siimplangi.rallytripmeter.R;

/**
 * Created by Siim on 29.09.2017.
 */

public class SpecialStageLineMap extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ss_line);
        getSupportFragmentManager().findFragmentById(R.id.map);
    }
}
