package ee.siimplangi.rallytripmeter.activities.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity

import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.fragments.settings.CustomLayoutSettingFragment

/**
 * Created by Siim on 10.09.2017.
 */

class CustomLayoutSettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        if (savedInstanceState == null) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.wrapper,
                            CustomLayoutSettingFragment.newInstance(intent.getStringExtra(getString(R.string.prefs_file_key))),
                            CustomLayoutSettingFragment::class.java.name)
                    .commit()
        }

    }

    companion object {

        fun getIntent(context: Context, @StringRes value: Int): Intent {
            val intent = Intent(context, CustomLayoutSettingsActivity::class.java)
            intent.putExtra(context.getString(R.string.prefs_file_key), context.getString(value))
            return intent
        }
    }

}
