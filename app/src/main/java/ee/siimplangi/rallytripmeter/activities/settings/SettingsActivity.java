package ee.siimplangi.rallytripmeter.activities.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.fragments.settings.SettingsFragment;

/**
 * Created by Siim on 10.09.2017.
 */

public class SettingsActivity extends AppCompatActivity {

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, SettingsActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().replace(R.id.wrapper, new SettingsFragment(), SettingsFragment.class.getName()).commit();
        }


    }

}
