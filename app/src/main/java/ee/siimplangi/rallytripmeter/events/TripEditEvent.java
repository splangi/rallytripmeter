package ee.siimplangi.rallytripmeter.events;

import ee.siimplangi.rallytripmeter.enums.TripType;

/**
 * Created by Siim on 07.09.2017.
 */

public class TripEditEvent {

    public final float newTrip;
    public final TripType tripType;

    public TripEditEvent(float newTrip, TripType tripType) {
        this.newTrip = newTrip;
        this.tripType = tripType;
    }
}
