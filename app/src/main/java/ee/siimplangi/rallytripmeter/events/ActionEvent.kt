package ee.siimplangi.rallytripmeter.events

enum class ActionEvent {

    RESET_TRIP1,
    RESET_TRIP2,
    RESET_SECTION,
    END_STAGE,
    QUICK_START_STAGE,


}