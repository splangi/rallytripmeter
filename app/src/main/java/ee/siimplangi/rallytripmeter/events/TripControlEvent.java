package ee.siimplangi.rallytripmeter.events;

import ee.siimplangi.rallytripmeter.enums.TripType;
import ee.siimplangi.rallytripmeter.models.TripControl;

/**
 * Created by Siim on 07.09.2017.
 */

public class TripControlEvent {

    public final TripType tripType;
    public final TripControl tripControl;

    public TripControlEvent(TripType tripType, TripControl tripControl) {
        this.tripType = tripType;
        this.tripControl = tripControl;
    }
}
