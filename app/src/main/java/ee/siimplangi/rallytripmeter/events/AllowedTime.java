package ee.siimplangi.rallytripmeter.events;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

/**
 * Created by Siim on 24.06.2017.
 */

public class AllowedTime implements Parcelable {

    public final int MINUTES;
    public static final Creator<AllowedTime> CREATOR = new Creator<AllowedTime>() {
        @Override
        public AllowedTime createFromParcel(Parcel in) {
            return new AllowedTime(in);
        }

        @Override
        public AllowedTime[] newArray(int size) {
            return new AllowedTime[size];
        }
    };
    public final int SECONDS;
    public final int HOUR;

    public AllowedTime(int HOUR, int MINUTES, int SECONDS) {
        this.SECONDS = SECONDS;
        this.MINUTES = MINUTES;
        this.HOUR = HOUR;
    }


    protected AllowedTime(Parcel in) {
        MINUTES = in.readInt();
        SECONDS = in.readInt();
        HOUR = in.readInt();
    }

    public Calendar asCalendar() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, SECONDS);
        cal.set(Calendar.MINUTE, MINUTES);
        cal.set(Calendar.HOUR_OF_DAY, HOUR);
        return cal;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(MINUTES);
        dest.writeInt(SECONDS);
        dest.writeInt(HOUR);
    }
}
