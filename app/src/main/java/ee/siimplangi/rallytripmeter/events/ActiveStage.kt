package ee.siimplangi.rallytripmeter.events

import ee.siimplangi.rallytripmeter.models.StageDef

/**
 * Created by Siim on 11.09.2017.
 */

class ActiveStage(val activeStageKey: String?, val stageDef: StageDef?)
