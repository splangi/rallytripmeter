package ee.siimplangi.rallytripmeter.events;

import java.util.Calendar;

import ee.siimplangi.rallytripmeter.helpers.Settings;
import ee.siimplangi.rallytripmeter.helpers.TimeHelper;

/**
 * Created by Siim on 24.06.2017.
 */

public class NewStage {

    public final long START_TIME;

    public NewStage(long START_TIME) {
        this.START_TIME = START_TIME;
    }

    public NewStage(int hourOfDay, int minutes, int seconds) {
        Calendar calendar = TimeHelper.getMidnightRallyCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minutes);
        calendar.set(Calendar.SECOND, seconds);
        if (calendar.before(Settings.INSTANCE.getRallyTime().getRallyTimeCalendar())) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }
        START_TIME = calendar.getTimeInMillis();
    }
}
