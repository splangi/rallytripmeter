package ee.siimplangi.rallytripmeter.events;

import ee.siimplangi.rallytripmeter.enums.TripType;

/**
 * Created by Siim on 24.06.2017.
 */

public class ResetEvent {

    public final TripType TYPE;

    public ResetEvent(TripType tripType) {
        this.TYPE = tripType;
    }


}
