package ee.siimplangi.rallytripmeter.events

enum class SectionAdvancer {

    PREV, NEXT

}