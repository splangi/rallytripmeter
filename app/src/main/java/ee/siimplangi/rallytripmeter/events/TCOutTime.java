package ee.siimplangi.rallytripmeter.events;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

import ee.siimplangi.rallytripmeter.helpers.TimeHelper;

/**
 * Created by Siim on 24.06.2017.
 */

public class TCOutTime implements Parcelable {

    public static final Creator<TCOutTime> CREATOR = new Creator<TCOutTime>() {
        @Override
        public TCOutTime createFromParcel(Parcel source) {
            return new TCOutTime(source);
        }

        @Override
        public TCOutTime[] newArray(int size) {
            return new TCOutTime[size];
        }
    };
    public final int hour;
    public final int minute;
    public final int second;


    public TCOutTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    protected TCOutTime(Parcel in) {
        this.hour = in.readInt();
        this.minute = in.readInt();
        this.second = in.readInt();
    }

    public Calendar getCalendar() {
        Calendar calendar = TimeHelper.getMidnightRallyCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
        return calendar;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.hour);
        dest.writeInt(this.minute);
        dest.writeInt(this.second);
    }
}

