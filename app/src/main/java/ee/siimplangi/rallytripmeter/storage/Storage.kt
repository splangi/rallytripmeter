package ee.siimplangi.rallytripmeter.storage

import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.RallyTripmeter
import ee.siimplangi.rallytripmeter.events.ChangeActiveStage
import ee.siimplangi.rallytripmeter.helpers.AuthHelper
import org.greenrobot.eventbus.EventBus
import java.util.*

object Storage {

    private const val installationIDKey = "installation_id"
    private const val activeStageKey = "activeStageDef"
    private const val privacyPolicyVersion = "privacyPolicy"

    init {
    }

    private val sharedPreferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(RallyTripmeter.app)
    }

    val installationID: String by lazy {
        sharedPreferences.let {
            if (!it.contains(installationIDKey)) {
                it.edit().putString(installationIDKey, UUID.randomUUID().toString()).apply()
            }
            it.getString(installationIDKey, "")
        }
    }

    var acceptedPrivacyPolicy
        get() = sharedPreferences.getInt(privacyPolicyVersion, 0) >= RallyTripmeter.app.resources.getInteger(R.integer.privacy_policy_version)
        set(value) {
            if (value) {
                sharedPreferences.edit().putInt(privacyPolicyVersion, RallyTripmeter.app.resources.getInteger(R.integer.privacy_policy_version)).apply()
            } else {
                sharedPreferences.edit().putInt(privacyPolicyVersion, 0).apply()
            }
        }

    var activeStage: String?
        get() {
            return if (AuthHelper.isLoggedIn) {
                sharedPreferences.getString(activeStageKey, null)
            } else {
                null
            }
        }
        set(value) {
            if (activeStage != value) {
                sharedPreferences.edit().putString(activeStageKey, value).apply()
                EventBus.getDefault().post(ChangeActiveStage(activeStage))
            }

        }

}