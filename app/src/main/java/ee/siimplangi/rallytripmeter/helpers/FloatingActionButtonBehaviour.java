package ee.siimplangi.rallytripmeter.helpers;

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

/**
 * Created by Siim on 04.09.2017.
 */

public class FloatingActionButtonBehaviour extends FloatingActionButton.Behavior {

    public FloatingActionButtonBehaviour() {
    }

    public FloatingActionButtonBehaviour(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


}
