package ee.siimplangi.rallytripmeter.helpers

import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.net.Uri
import android.os.Build
import com.google.firebase.auth.FirebaseAuth
import ee.siimplangi.rallytripmeter.R
import android.content.pm.PackageManager
import androidx.core.content.pm.PackageInfoCompat
import android.R.attr.versionCode
import com.firebase.ui.auth.AuthUI.getApplicationContext




object IntentHelper {

    fun startEmailActivity(ctx: Context, subject: CharSequence) {
        val emailIntent = Intent(Intent.ACTION_VIEW, Uri.parse("mailto:?subject="
                + subject + "&to="
                + ctx.getString(R.string.feedback_email)
                + "&body="
                + "\n\n\n----- DEBUG INFO -----\n"
                + "User ID: " + FirebaseAuth.getInstance().currentUser?.uid
                + "\nAndroid ID: " + UniqueDeviceIDGenerator.getUniqueId(ctx)
                + "\nDevice: " + Build.PRODUCT
                + "\nAndroid Version: " + Build.VERSION.SDK_INT
        + "\nApp Version: " + getAppVersion(ctx)))
        ctx.startActivity(Intent.createChooser(emailIntent, ctx.getString(R.string.send_email)))
    }


    private fun getAppVersion(ctx: Context) : String{
        val context = ctx.applicationContext
        val manager = context.packageManager
        var myVersionName : String = "Unknown"
        try {
            val info = manager.getPackageInfo(context.packageName, 0)
            myVersionName = info.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return myVersionName;

    }

    fun startWebsiteActivity(ctx: Context, uri: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        ctx.startActivity(browserIntent)
    }

}