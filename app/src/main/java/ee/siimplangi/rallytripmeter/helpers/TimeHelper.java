package ee.siimplangi.rallytripmeter.helpers;

import android.location.Location;
import android.os.Build;
import android.os.SystemClock;

import java.util.Calendar;

/**
 * Created by Siim on 27.07.2017.
 */

public class TimeHelper {

    private TimeHelper() {
    }

    public static long getOffsetWithSystemTime(int hours, int minutes, int seconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.HOUR_OF_DAY, hours);
        calendar.set(Calendar.MINUTE, minutes);
        calendar.set(Calendar.SECOND, seconds);
        return calendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
    }


    public static long getLocationElapsedTime(Location location) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return location.getElapsedRealtimeNanos() / 1000000L;
        } else {
            return location.getTime();
        }
    }

    public static long getElapsedTime() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return SystemClock.elapsedRealtime();
        } else {
            return System.currentTimeMillis();
        }
    }

    public static Calendar getMidnightRallyCalendar() {
        Calendar calendar = Settings.INSTANCE.getRallyTime().getRallyTimeCalendar();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        return calendar;
    }


}
