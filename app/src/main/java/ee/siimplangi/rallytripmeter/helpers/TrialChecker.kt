package ee.siimplangi.rallytripmeter.helpers

import android.util.Log
import com.google.firebase.database.*
import ee.siimplangi.rallytripmeter.AppLog
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants
import ee.siimplangi.rallytripmeter.enums.ProductStatus
import ee.siimplangi.rallytripmeter.listeners.OnTrialStatus
import ee.siimplangi.rallytripmeter.models.FirebaseTimeStamp
import ee.siimplangi.rallytripmeter.models.TrialModel


class TrialChecker(private val idKey: String, private val listener: OnTrialStatus) {


    private val trialDbRef: DatabaseReference = FirebaseDatabase.getInstance().getReference(FirebaseConstants.TRIAL_REGISTRATION_NODE + "/" + idKey)

    private val latestReadNode: DatabaseReference = FirebaseDatabase
            .getInstance()
            .getReference(FirebaseConstants.TRIAL_REGISTRATION_NODE + "/"
                    + idKey + "/"
                    + FirebaseConstants.TRIAL_LATEST_READ_NODE)


    var model: TrialModel? = null
        private set(value) {
            field = value ?: TrialModel()
            Log.d(TrialChecker::class.java.simpleName, "Trial Model updated, notifying listeners!")
            notifyListener()
        }

    private val modelListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {
            AppLog.e(TrialChecker::class.java.simpleName, p0)
        }

        override fun onDataChange(p0: DataSnapshot) {
            model = p0.getValue(TrialModel::class.java)
            AppLog.d(TrialChecker::class.java.simpleName, "onTrialModelUpdate: " + model.toString())
        }
    }

    private fun notifyListener() {
        listener.onTrialStatus(model?.getStatus() ?: ProductStatus.TRIAL_NOT_STARTED, model)
    }


    fun startTrial(trialLengthMillis: Long) {
        trialDbRef.setValue(TrialModel(idKey, trialLengthMillis))
    }


    fun onStart() {
        trialDbRef.addValueEventListener(modelListener)
    }

    fun onStop() {
        trialDbRef.removeEventListener(modelListener)
        latestReadNode.setValue(FirebaseTimeStamp.getServerTimeStamp())
    }


}