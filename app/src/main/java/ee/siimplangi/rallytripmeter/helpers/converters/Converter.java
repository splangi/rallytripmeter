package ee.siimplangi.rallytripmeter.helpers.converters;

/**
 * Created by Siim on 16.09.2017.
 */

public interface Converter<T> {

    T convert(T convertable);

}
