package ee.siimplangi.rallytripmeter.helpers

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.res.ResourcesCompat
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.RallyTripmeter

class ComponentSettings(val prefs: SharedPreferences) {

    companion object {

        private val context: Context
            get() = RallyTripmeter.app

        private fun getKey(@StringRes res: Int): String {
            return context.getString(res)
        }

        private val KEY_COMPONENT_BG by lazy {
            getKey(R.string.prefs_key_component_bg_color)
        }

        private val KEY_TEXT_COLOR by lazy {
            getKey(R.string.prefs_key_component_main_text_color)
        }

        private val KEY_HEADING_COLOR by lazy {
            getKey(R.string.prefs_key_component_heading_text_color)
        }

        private val KEY_DRAW_STROKE by lazy {
            getKey(R.string.prefs_key_component_draw_stroke)
        }

        private val KEY_STROKE_COLOR by lazy {
            getKey(R.string.prefs_key_component_stroke_color)
        }

        private val KEY_STROKE_WIDTH by lazy {
            getKey(R.string.prefs_key_component_stroke_width)
        }

        private val KEY_ENABLE_THRESHOLD by lazy {
            getKey(R.string.prefs_key_enable_threshold)
        }

        private val KEY_THRESHOLD_VALUE by lazy {
            getKey(R.string.prefs_key_threshold)
        }

        private val KEY_OVER_THRESHOLD_TEXT_COLOR by lazy {
            getKey(R.string.prefs_key_text_color_over)
        }

        private val KEY_UNDER_THRESHOLD_TEXT_COLOR by lazy {
            getKey(R.string.prefs_key_text_color_under)
        }

        private val KEY_UNDER_THRESHOLD_BG_COLOR by lazy {
            getKey(R.string.prefs_key_bg_color_under)
        }

        private val KEY_OVER_THRESHOLD_BG_COLOR by lazy {
            getKey(R.string.prefs_key_bg_color_over)
        }

    }

    private fun getColorDefaultRes(key: String, @ColorRes default: Int): Int {
        return getColor(key, ResourcesCompat.getColor(context.resources, default, null))
    }

    private fun getColor(key: String, @ColorInt default: Int): Int {
        return prefs.getInt(key, default)
    }

    val buttonBG
        get() = getColorDefaultRes(KEY_COMPONENT_BG, R.color.grey_800)

    val componentBG
        get() = getColorDefaultRes(KEY_COMPONENT_BG, R.color.prefs_default_component_bg_color)

    val componentTextColor
        get() = getColor(KEY_TEXT_COLOR, Settings.textColor)

    val componentHeadingColor
        get() = getColor(KEY_HEADING_COLOR, Settings.headingColor)

    val strokeWidth
        get() = prefs.getString(KEY_STROKE_WIDTH, "0")?.toInt() ?: 0

    val strokeColor
        get() = getColorDefaultRes(KEY_STROKE_COLOR, R.color.prefs_default_component_stoke_color)

    val drawStroke
        get() = prefs.getBoolean(KEY_DRAW_STROKE, false)

    val enableThreshold
        get() = prefs.getBoolean(KEY_ENABLE_THRESHOLD, true)

    val threshold
        get() = prefs.getString(KEY_THRESHOLD_VALUE, "3")?.toInt() ?: 0

    val txtColorUnder
        get() = getColorDefaultRes(KEY_UNDER_THRESHOLD_TEXT_COLOR, R.color.prefs_default_main_text_color)

    val txtColorOver
        get() = getColorDefaultRes(KEY_OVER_THRESHOLD_TEXT_COLOR, R.color.prefs_default_main_text_color)

    val bgColorUnder
        get() = getColorDefaultRes(KEY_UNDER_THRESHOLD_BG_COLOR, R.color.deep_purple_500)

    val bgColorOver
        get() = getColorDefaultRes(KEY_OVER_THRESHOLD_BG_COLOR, R.color.red_500)
}