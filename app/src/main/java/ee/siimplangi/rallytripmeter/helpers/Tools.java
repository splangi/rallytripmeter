package ee.siimplangi.rallytripmeter.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.widget.EditText;

import java.text.DecimalFormat;

import ee.siimplangi.rallytripmeter.activities.BillingActivity;
import ee.siimplangi.rallytripmeter.helpers.converters.Converter;

/**
 * Created by Siim on 04.09.2017.
 */

public class Tools {

    private Tools() {

    }

    public static BillingActivity getBillingActivity(Context context) {
        Activity activity = getActivity(context);
        if (activity instanceof BillingActivity) {
            return (BillingActivity) activity;
        } else {
            return null;
        }
    }

    public static Activity getActivity(Context context) {
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }
        return null;
    }

    /*public static Float extractFloatData(EditText editText) {
        return extractFloatData(extractStringData(editText), 1);
    }

    public static Float extractFloatData(EditText editText, float multiplier) {
        return extractFloatData(extractStringData(editText), multiplier);
    }

    public static Integer extractIntegerData(EditText editText) {
        return extractIntegerData(extractStringData(editText), 1);
    }

    public static Integer extractIntegerData(Sku text, Converter<>) {
        try {
            return (int) (Integer.parseInt(text) * multiplier);
        } catch (Exception e) {
            return null;
        }
    }*/

    public static Float extractData(String text, Converter<Float>... converters) {
        try {
            Float f = Float.parseFloat(text);
            f = convertFloat(f, converters);
            return f;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static Float extractData(EditText editText, Converter<Float>... converters) {
        return extractData(extractStringData(editText), converters);
    }

    public static Float convertFloat(Float f, Converter<Float>... converters) {
        if (f == null) {
            return null;
        }
        for (Converter<Float> converter : converters) {
            f = converter.convert(f);
        }
        return f;
    }

    public static String formatFloat(Float f, DecimalFormat df, Converter<Float>... converters) {
        f = convertFloat(f, converters);
        if (f == null) return "";
        return df.format(f);
    }


    /*public static Float extractFloatData(Sku text, float multiplier) {
        try {
            return Float.parseFloat(text) * multiplier;
        } catch (Exception e) {
            return null;
        }
    }

    public static Float extractFloatData(Sku text) {
        return extractFloatData(text, 1f);
    }
    */
    public static String extractStringData(EditText editText) {
        return editText.getText().toString().trim();
    }
    /*

    @Deprecated
    public static Sku formatInt(Float number) {
        if (number == null) return "";
        return formatInt(Math.round(number));
    }


    @Deprecated
    public static Sku formatInt(Integer number) {
        if (number == null) return "";
        return Sku.format(Locale.getDefault(), "%d", number);
    }


    @Deprecated
    public static Sku formatInt(Integer number, Float multiplier) {
        if (number == null) return "";
        return Sku.format(Locale.getDefault(), "%d", number * multiplier);
    }


    @Deprecated
    public static Sku formatFloat(Float number) {
        if (number == null) return "";
        return Sku.format(Locale.getDefault(), "%.02f", number);
    }


    @Deprecated
    public static Sku formatFloat(Float number, float multiplier) {
        if (number == null) return "";
        return Sku.format(Locale.getDefault(), "%.02f", number * multiplier);
    }*/



}
