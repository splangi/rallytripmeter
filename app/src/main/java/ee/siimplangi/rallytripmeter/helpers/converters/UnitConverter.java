package ee.siimplangi.rallytripmeter.helpers.converters;

import ee.siimplangi.rallytripmeter.enums.Unit;

/**
 * Created by Siim on 16.09.2017.
 */

public class UnitConverter implements Converter<Float> {

    public static final UnitConverter KILOMETERS_TO_METERS = new UnitConverter(1000f);
    public static final UnitConverter METERS_TO_KILOMETERS = new UnitConverter(1 / 1000f);
    public static final UnitConverter MPS_TO_KPH = new UnitConverter(3.6f);
    public static final UnitConverter KPH_TO_MPS = new UnitConverter((1f / 3.6f));

    private float multiplier;

    public UnitConverter(float conversionFloats) {
        this.multiplier = conversionFloats;
    }

    public UnitConverter(Unit fromUnit, Unit toUnit, float... conversion) {
        this((toUnit.MULTIPLIER / fromUnit.MULTIPLIER) * listMult(conversion));
    }

    private static float listMult(float[] list) {
        float sum = 1;
        for (float n : list) {
            sum = sum * n;
        }
        return sum;
    }

    @Override
    public Float convert(Float convertable) {
        if (convertable == null) {
            return null;
        } else {
            return convertable * multiplier;
        }
    }
}
