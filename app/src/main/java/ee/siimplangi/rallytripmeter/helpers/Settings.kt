package ee.siimplangi.rallytripmeter.helpers

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.ColorRes
import androidx.core.content.res.ResourcesCompat
import androidx.preference.PreferenceManager
import ee.siimplangi.rallytripmeter.AppLog
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.RallyTripmeter
import ee.siimplangi.rallytripmeter.enums.Font
import ee.siimplangi.rallytripmeter.enums.Unit
import ee.siimplangi.rallytripmeter.events.RunBackgroundEventUpdated
import ee.siimplangi.rallytripmeter.events.TrackingStateEvent
import ee.siimplangi.rallytripmeter.models.RallyTime
import org.greenrobot.eventbus.EventBus

/**
 * Created by Siim on 06.08.2017.
 */

object Settings : SharedPreferences.OnSharedPreferenceChangeListener {

    private fun getColor(key: String, @ColorRes default: Int): Int {
        return preferences.getInt(key, ResourcesCompat.getColor(context.resources, default, null))
    }

    //DO NOT CHANGE UNLESS YOU UNDERSTAND THE CONSEQUENCES TO THE USERS!
    private const val SETTINGS_VERS = 3

    val DEVICE_UNIQUE_ID by lazy {
        UniqueDeviceIDGenerator.getUniqueId(context)
    }

    private val context: Context
        get() = RallyTripmeter.app


    private val KEY_UNITS by lazy { context.getString(R.string.prefs_key_units) }
    private val KEY_RUN_BACKGROUND by lazy { context.getString(R.string.prefs_key_run_background) }
    private val KEY_SHOW_TOOLBAR by lazy { context.getString(R.string.prefs_key_show_toolbar) }
    private val KEY_STAGE_END_CLICKLISTENER by lazy { context.getString(R.string.prefs_key_end_stage) }
    private val KEY_SPEEDLIMIT_ALARM by lazy { context.getString(R.string.prefs_key_speedlimit_beep) }
    private val KEY_SHOW_SPEED_LIMIT by lazy { context.getString(R.string.prefs_key_show_speedlimit) }
    private val KEY_RALLYTIME by lazy { context.getString(R.string.prefs_key_rallytime) }
    private val KEY_TRACKING_ON by lazy { context.getString(R.string.prefs_key_tracking_on) }
    private val KEY_GLOBAL_TEXT_COLOR by lazy { context.getString(R.string.prefs_key_global_main_text_color) }
    private val KEY_HEADING_TEXT_COLOR by lazy { context.getString(R.string.prefs_key_global_heading_text_color) }
    private val KEY_GLOBAL_MAIN_FONT by lazy { context.getString(R.string.prefs_key_global_main_text_font) }
    private val KEY_GLOBAL_HEADING_FONT by lazy { context.getString(R.string.prefs_key_global_heading_text_font) }
    private val KEY_GLOBAL_BG_COLOR by lazy { context.getString(R.string.prefs_key_global_bg_color) }
    private const val KEY_VERS = "vers"

    private val preferences: SharedPreferences by lazy {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        PreferenceManager.setDefaultValues(context, R.xml.prefs, prefs.getInt(KEY_VERS, 0) < SETTINGS_VERS)
        prefs.edit().putInt(KEY_VERS, SETTINGS_VERS).apply()
        prefs.registerOnSharedPreferenceChangeListener(this)
        prefs
    }

    private fun getFontByKey(key: String): Font {
        try {
            return Font.valueOf(preferences.getString(key, Font.SYSTEM_NORMAL.name)
                    ?: Font.SYSTEM_NORMAL.name)
        } catch (e: IllegalArgumentException) {
            AppLog.e(Settings::class.java.simpleName, "Could not find font!")
        }
        return Font.SYSTEM_NORMAL
    }

    val textColor
        get() = getColor(KEY_GLOBAL_TEXT_COLOR, R.color.prefs_default_main_text_color)

    val headingColor
        get() = getColor(KEY_HEADING_TEXT_COLOR, R.color.prefs_default_heading_text_color)

    val bgColor: Int
        get() = getColor(KEY_GLOBAL_BG_COLOR, R.color.prefs_default_global_bg_color)

    val textFont: Font
        get() = getFontByKey(KEY_GLOBAL_MAIN_FONT)

    val headingFont: Font
        get() = getFontByKey(KEY_GLOBAL_HEADING_FONT)

    val runInBackground: Boolean
        get() = preferences.getBoolean(KEY_RUN_BACKGROUND, true)

    val unit: Unit
        get() = Unit.getUnitByKey(Integer.parseInt(preferences.getString(KEY_UNITS, "0")))

    var isTrackingOn: Boolean
        get() = preferences.getBoolean(KEY_TRACKING_ON, false)
        set(value) {
            preferences.edit().putBoolean(KEY_TRACKING_ON, value).apply()
            EventBus.getDefault().post(TrackingStateEvent(value))
        }

    public var rallyTime: RallyTime = RallyTime(preferences.getLong(KEY_RALLYTIME, 0L))
        set(value) {
            preferences.edit().putLong(KEY_RALLYTIME, field.offset).apply()
            field = value
            EventBus.getDefault().postSticky(field)
        }


    val showToolbar: Boolean
        get() = preferences.getBoolean(KEY_SHOW_TOOLBAR, true)

    val showSpeedLimit: Boolean
        get() = preferences.getBoolean(KEY_SHOW_SPEED_LIMIT, true)


    val endStageOnLongClick: Boolean = preferences.getBoolean(KEY_STAGE_END_CLICKLISTENER, false)


    fun beepOn(): Boolean {
        return preferences.getBoolean(KEY_SPEEDLIMIT_ALARM, false)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, s: String) {
        var changed: Any? = null
        when (s) {
            KEY_UNITS -> changed = unit
            KEY_RUN_BACKGROUND -> changed = RunBackgroundEventUpdated()
        }
        if (changed != null) {
            EventBus.getDefault().post(changed)
        }
    }


}

