package ee.siimplangi.rallytripmeter.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import java.util.*


object UniqueDeviceIDGenerator {


    fun getUniqueId(ctx: Context): String {
        return getAndroidID(ctx)?.takeIf { checkAndroidID(it) } ?: getUniqueId()
    }

    private fun getUniqueId(): String {
        var devIDShort = Build.BOARD + Build.BRAND

        devIDShort += if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Build.SUPPORTED_ABIS[0]
        } else {
            Build.CPU_ABI
        }

        devIDShort += Build.DEVICE + Build.MANUFACTURER + Build.MODEL + Build.PRODUCT

        var serial: String
        try {
            serial = Build::class.java.getField("SERIAL").get(null).toString()
            return UUID(devIDShort.hashCode().toLong(), serial.hashCode().toLong()).toString()
        } catch (e: Exception) {

            serial = "ENSV12444" // some value
        }

        // Finally, combine the values we have found by using the UUID class to create a unique identifier
        return UUID(devIDShort.hashCode().toLong(), serial.hashCode().toLong()).toString()
    }

    private fun checkAndroidID(androidID: String?): Boolean {
        return androidID != null && androidID != "" && androidID.length == 16 && androidID != "9774D56D682E549C" && androidID != "0000000000000000"
    }

    @SuppressLint("HardwareIds")
    private fun getAndroidID(context: Context): String? {
        return android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
    }

}