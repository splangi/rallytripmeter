package ee.siimplangi.rallytripmeter.helpers

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import ee.siimplangi.rallytripmeter.R
import java.util.*


/**
 * Created by Siim on 29.08.2017.
 */

class AuthHelper {

    fun startAuthUIActivity(activity: Activity) {
        activity.startActivityForResult(authUiIntent, AUTH_UI_REUQEST_CODE)

    }

    fun startAuthUIActivity(fragment: androidx.fragment.app.Fragment) {
        fragment.startActivityForResult(authUiIntent, AUTH_UI_REUQEST_CODE)
    }

    fun startMergeActivity() {

    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?, onAuthResultListener: OnAuthResultListener) {
        if (requestCode == AUTH_UI_REUQEST_CODE) {
            val idpResponse = IdpResponse.fromResultIntent(data)
            val error = idpResponse?.error
            val result = if (resultCode == Activity.RESULT_OK && idpResponse != null && error == null) {
                AuthResult.AUTHENTICATED
            } else if (idpResponse == null || error?.errorCode == ErrorCodes.PROVIDER_ERROR) {
                AuthResult.CANCELED
            } else if (error?.errorCode == ErrorCodes.ANONYMOUS_UPGRADE_MERGE_CONFLICT) {
                idpResponse.credentialForLinking?.let {
                    FirebaseAuth.getInstance().signInWithCredential(it)
                    AuthResult.ANONYMOUS_UPGRADE_FAILED
                } ?: AuthResult.ERROR
            } else if (error?.errorCode == ErrorCodes.NO_NETWORK) {
                AuthResult.NO_NETWORK
            } else if (error?.errorCode == ErrorCodes.PLAY_SERVICES_UPDATE_CANCELLED) {
                AuthResult.UPDATE_REQUIRED
            } else {
                AuthResult.ERROR
            }
            onAuthResultListener.onAuthResult(result)
        }
    }


    enum class AuthResult {

        AUTHENTICATED, CANCELED, ERROR, NO_NETWORK, UPDATE_REQUIRED, ANONYMOUS_UPGRADE_FAILED

    }

    interface OnAuthResultListener {

        fun onAuthResult(result: AuthResult)

    }

    companion object {

        private val configs: MutableList<AuthUI.IdpConfig>
        private const val AUTH_UI_REUQEST_CODE = 99
        private val auth: FirebaseAuth by lazy {
            FirebaseAuth.getInstance()
        }

        init {
            configs = ArrayList<AuthUI.IdpConfig>()
            //configs.add(new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build());
            //configs.add(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build());
            configs.add(AuthUI.IdpConfig.GoogleBuilder().build())
            configs.add(AuthUI.IdpConfig.EmailBuilder().setRequireName(false).setAllowNewAccounts(true).build())
        }

        fun signOut(activity: AppCompatActivity, onCompleteListener: OnCompleteListener<Void>) {
            AuthUI.getInstance().signOut(activity).addOnCompleteListener(onCompleteListener)
        }

        fun signInAnonymously() {
            auth.signInAnonymously()
        }

        fun trySigining() {
        }

        val isAnonymous: Boolean
            get() = auth.currentUser?.isAnonymous == true

        val isLoggedIn: Boolean
            get() = auth.currentUser != null

        private val authUiIntent: Intent
            get() = AuthUI.getInstance().createSignInIntentBuilder().setAvailableProviders(configs).enableAnonymousUsersAutoUpgrade().setLogo(R.mipmap.ic_launcher).build()
    }
}
