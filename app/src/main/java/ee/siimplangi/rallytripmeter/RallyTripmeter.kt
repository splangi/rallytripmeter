package ee.siimplangi.rallytripmeter

import android.app.Application
import android.content.Context
import android.graphics.Color
import androidx.appcompat.content.res.AppCompatResources
import com.crashlytics.android.Crashlytics
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.Logger
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants
import ee.siimplangi.rallytripmeter.constants.StateViewConstants
import ee.siimplangi.rallytripmeter.extensions.getColorFromAttr
import ee.siimplangi.rallytripmeter.storage.Storage
import io.fabric.sdk.android.Fabric
import org.greenrobot.eventbus.EventBus
import sakout.mehdi.StateViews.StateViewsBuilder

/**
 * Created by Siim on 24.07.2017.
 */

class RallyTripmeter : Application() {

    companion object {

        var initialized: Boolean = false

        var crashLyticsEnabled: Boolean = false

        lateinit var app: RallyTripmeter

        fun initializeServices(context: Context) {
            if (!initialized) {
                RallyTripmeter.crashLyticsEnabled = context.resources.getBoolean(R.bool.useCrashLytics)
                if (RallyTripmeter.crashLyticsEnabled) {
                    Fabric.with(context, Crashlytics())
                }
                val database = FirebaseDatabase.getInstance()
                if (BuildConfig.DEBUG) {
                    database.setLogLevel(Logger.Level.DEBUG)
                }
                database.setPersistenceEnabled(true)
                if (BuildConfig.DEBUG) {
                    FirebaseRemoteConfig.getInstance().setConfigSettings(FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(true).build());
                }
                FirebaseAnalytics.getInstance(app).setUserId(Storage.installationID)
                FirebaseRemoteConfig.getInstance().setDefaults(FirebaseConstants.configDefaults)
                //How long the config cache is valid (in seconds)
                val cacheDuration = if (BuildConfig.DEBUG) 60L else 12 * 60 * 60
                FirebaseRemoteConfig.getInstance().fetch(cacheDuration).addOnCompleteListener { FirebaseRemoteConfig.getInstance().activateFetched() }
                initialized = true
            }

        }
    }


    override fun onCreate() {
        super.onCreate()
        app = this
        if (Storage.acceptedPrivacyPolicy) {
            RallyTripmeter.initializeServices(this)
        }
        EventBus.builder().logNoSubscriberMessages(false).installDefaultEventBus()
        initStateViewBuilder()

    }


    private fun initStateViewBuilder() {
        StateViewsBuilder
                .init(this)
                .setIconColor(Color.parseColor("#FFFFFF"))
                .setButtonBackgroundColor(getColorFromAttr(androidx.appcompat.R.attr.buttonTint))
                .setButtonTextColor(Color.parseColor("#FFFFFF"))
                .setIconSize(resources.getDimensionPixelSize(R.dimen.state_icon_size))
                .addState(StateViewConstants.INTERNET_OFF,
                        getString(R.string.internet_off_title),
                        getString(R.string.internet_off_message),
                        AppCompatResources.getDrawable(this, R.drawable.ic_cloud_off_white),
                        getString(R.string.retry)
                )
                .addState(StateViewConstants.EXCEPTION,
                        getString(R.string.exception_title),
                        getString(R.string.exception_message),
                        AppCompatResources.getDrawable(this, R.drawable.ic_error_outline_white),
                        getString(R.string.retry))


    }


}
