package ee.siimplangi.rallytripmeter.models

import android.location.Location
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants

data class TrackingData(val userId: String?,
                        val lat: Double,
                        val lng: Double,
                        val speedMS: Float,
                        val heading: Float,
                        val altitude: Double,
                        val disconnected: Boolean = false,
                        val deviceTimeStamp: Long = System.currentTimeMillis(),
                        val timeStamp: FirebaseTimeStamp = FirebaseTimeStamp.getServerTimeStamp()) {

    constructor(userId: String?, location: Location) : this(userId,
            location.latitude,
            location.longitude,
            location.speed,
            location.bearing,
            location.altitude)


    fun hasMajorChanged(other: TrackingData): Boolean {
        if (Math.abs(this.deviceTimeStamp - other.deviceTimeStamp) < FirebaseRemoteConfig.getInstance().getLong(FirebaseConstants.CONFIG_MAX_RECORDING_FREQUENCY, "500")) return false
        if (userId != other.userId) return true
        if (FloatArray(3).also { it -> Location.distanceBetween(lat, lng, other.lat, other.lng, it) }.let { it[0] } > 10f) return true
        if (Math.abs(speedMS - other.speedMS) > 2f) return true
        if (Math.abs(heading - other.heading) > 5) return true
        if (Math.abs(altitude - other.altitude) > 10) return true
        if (Math.abs(this.deviceTimeStamp - other.deviceTimeStamp) > FirebaseRemoteConfig.getInstance().getLong(FirebaseConstants.CONFIG_STANDING_RECORDING_FREQUENCY, "10000")) return true
        return false
    }


}