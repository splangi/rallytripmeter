package ee.siimplangi.rallytripmeter.models

import android.os.Parcelable
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.parcel.Parcelize


@Parcelize
data class UserData(val email: String? = FirebaseAuth.getInstance().currentUser?.email,
                    val driver: String? = FirebaseAuth.getInstance().currentUser?.displayName,
                    val codriver: String? = null,
                    val id: String? = null,
                    val vehicle: String? = null) : Parcelable {


}