package ee.siimplangi.rallytripmeter.models

import android.os.Parcelable
import ee.siimplangi.rallytripmeter.events.ActiveStage
import kotlinx.android.parcel.Parcelize
import org.greenrobot.eventbus.EventBus

@Parcelize
class SpecialStage(var startTime: Long? = null,
                   var endTime: Long? = null,
                   var stageDef: StageDef? = EventBus.getDefault().getStickyEvent(ActiveStage::class.java)?.stageDef,
                   var currentSection: Int = 0) : Parcelable {


    fun reset() {
        startTime = null
        endTime = null
        currentSection = 0
        stageDef = EventBus.getDefault().getStickyEvent(ActiveStage::class.java).stageDef
    }

    fun reset(newStart: Long) {
        reset()
        startTime = newStart
    }

    fun end() {
        val currentTime = System.currentTimeMillis()
        this.startTime?.let {
            if (currentTime < it) this.endTime = this.startTime else endTime = currentTime
        }

    }

    fun getTimeToStart(): Long? {
        return startTime?.let { it - System.currentTimeMillis() }
    }

    fun isRunning(): Boolean {
        val startTime = this.startTime
        return startTime != null && startTime <= System.currentTimeMillis() && endTime == null
    }

    fun isLessThanTimeToStart(time: Long): Boolean {
        val startTime = this.startTime
        return startTime != null && time >= startTime - System.currentTimeMillis()
    }

    fun isSet(): Boolean {
        return startTime != null
    }

    fun getCurrentSection(): Section? {
        return stageDef?.sections?.getOrNull(currentSection)
    }

    fun getElapsedTime(): Long? {
        val startTime = this.startTime
        val endTime = this.endTime
        if (startTime != null && endTime != null) {
            return endTime - startTime
        } else if (startTime != null) {
            return System.currentTimeMillis() - startTime
        } else {
            return null
        }

    }

    fun hasEnded(): Boolean {
        return startTime != null && endTime != null
    }

}