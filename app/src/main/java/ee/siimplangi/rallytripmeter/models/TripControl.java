package ee.siimplangi.rallytripmeter.models;

/**
 * Created by Siim on 07.09.2017.
 */

public enum TripControl {

    READ_UP, PAUSE, READ_DOWN

}
