package ee.siimplangi.rallytripmeter.models


import java.util.*

/**
 * Created by Siim on 08.08.2017.
 */


class RallyTime(val offset: Long) {

    val timezone: TimeZone by lazy {
        val tz = TimeZone.getDefault()
        tz.rawOffset = (tz.rawOffset + offset).toInt()
        tz
    }

    val rallyTimeCalendar: Calendar
        get() = Calendar.getInstance(timezone)


    fun currentRallyTimeMillis(): Long {
        val date = System.currentTimeMillis()
        return date + timezone.getOffset(date)
    }

}
