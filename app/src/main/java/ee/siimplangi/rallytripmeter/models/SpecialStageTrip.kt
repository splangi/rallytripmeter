package ee.siimplangi.rallytripmeter.models

import android.os.Parcel
import android.os.Parcelable


class SpecialStageTrip : StageTrip, Parcelable {

    constructor() : super()
    constructor(startTimeSystemMillis: Long?) : super(startTimeSystemMillis)
    constructor(parcel: Parcel?) : super(parcel)

}
