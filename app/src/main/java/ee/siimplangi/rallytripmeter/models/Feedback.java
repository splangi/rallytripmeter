package ee.siimplangi.rallytripmeter.models;

import com.google.firebase.auth.FirebaseUser;

/**
 * Created by Siim on 27.09.2017.
 */

public class Feedback {

    private String user;
    private String email;
    private String name;
    private float rating;
    private FirebaseTimeStamp firebaseTimeStamp;
    private String message;

    public Feedback(float rating, String message, FirebaseUser firebaseUser) {
        this.rating = rating;
        this.message = message;
        firebaseTimeStamp = new FirebaseTimeStamp();
        if (firebaseUser != null) {
            this.user = firebaseUser.getUid();
            this.email = firebaseUser.getEmail();
            this.name = firebaseUser.getDisplayName();
        }
    }

    public String getUser() {
        return user;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public float getRating() {
        return rating;
    }

    public FirebaseTimeStamp getFirebaseTimeStamp() {
        return firebaseTimeStamp;
    }

    public String getMessage() {
        return message;
    }
}
