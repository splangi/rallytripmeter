package ee.siimplangi.rallytripmeter.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ServerValue;

import java.util.Map;

/**
 * Created by Siim on 02.09.2017.
 */

@IgnoreExtraProperties
public class FirebaseTimeStamp implements Parcelable {

    public static final Parcelable.Creator<FirebaseTimeStamp> CREATOR = new Parcelable.Creator<FirebaseTimeStamp>() {
        @Override
        public FirebaseTimeStamp createFromParcel(Parcel source) {
            return new FirebaseTimeStamp(source);
        }

        @Override
        public FirebaseTimeStamp[] newArray(int size) {
            return new FirebaseTimeStamp[size];
        }
    };
    private Object timeStamp;

    public static FirebaseTimeStamp getServerTimeStamp() {
        return new FirebaseTimeStamp(ServerValue.TIMESTAMP);
    }

    public FirebaseTimeStamp() {
    }

    public FirebaseTimeStamp(Map<String, String> serverValue) {
        this.timeStamp = serverValue;
    }


    protected FirebaseTimeStamp(Parcel in) {
        timeStamp = in.readValue(null);
        /*Sku clazz = in.readString();
        if (clazz != null){
            if (Long.class.getName().equals(clazz)){
                timeStamp = in.readLong();
            } else{
                timeStamp = ServerValue.TIMESTAMP;
            }
        }*/
    }

    public Object getTimeStamp() {
        return timeStamp;
    }

    @Exclude
    public Long getTimeStampLong() {
        return (long) timeStamp;
    }

    public void overWriteTimestamp() {
        timeStamp = ServerValue.TIMESTAMP;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(timeStamp);
        /*if (timeStamp != null){
            dest.writeString(timeStamp.getClass().getName());
        } else{
            dest.writeString(null);
        }
        if (timeStamp instanceof Map){
            dest.writeMap((Map) this.timeStamp);
        } else{
            dest.writeLong((Long) this.timeStamp);
        }*/
    }
}
