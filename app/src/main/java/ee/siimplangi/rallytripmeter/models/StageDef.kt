package ee.siimplangi.rallytripmeter.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlin.math.roundToLong

@Parcelize
data class StageDef(val uid: String? = null,
                    val stageName: String = "",
                    val rallyName: String = "",
                    val sections: List<Section> = ArrayList(),
                    val firebaseTimeStamp: FirebaseTimeStamp = FirebaseTimeStamp.getServerTimeStamp()) : Parcelable {


    fun getCurrentSectionByDistance(distance: Double): Section? {
        var lastSection: Section? = null
        for (section in sections) {
            if (section.isInArea(distance, lastSection)) {
                return section
            }
            lastSection = section
        }
        return null
    }

    fun getCurrentSectionByChoice(choice: Int): Section? {
        return sections.getOrNull(choice)
    }

    fun getIdealTime(distance: Double): Long? {
        var time = 0L
        var from = 0.0
        for (section in sections) {
            val to = section.to
            val speed = section.speed

            if (to == null || speed == null || speed == 0.0) return null

            val distanceTraveled: Double

            distanceTraveled = if (distance > to) {
                to - from
            } else {
                distance - from
            }
            time += (((distanceTraveled) / speed) * 1000).roundToLong()
            if (distance <= to) {
                break
            }
            from = to;
        }
        return time
    }

    fun getFromValueBySection(section: Int): Double? {
        if (section == 0) return 0.0
        return sections.getOrNull(section - 1)?.to
    }

    fun getToValueBySection(section: Int): Double? {
        return sections.getOrNull(section)?.to
    }

    fun getSpeedConstraints(section: Int): Double? {
        return sections.getOrNull(section)?.speed
    }

    fun areLimitsViolated(speedMS: Double, distance: Double): Boolean {
        return getCurrentSectionByDistance(distance)?.speed ?: Double.MAX_VALUE < speedMS
    }

}

@Parcelize
data class Section(var to: Double? = null,
                   var speed: Double? = null) : Parcelable {

    fun isOverSpeed(distance: Double, speedMS: Double, lastSection: Section?): Boolean {
        return isInArea(distance, lastSection) && this.speed ?: Double.MAX_VALUE < speedMS
    }

    fun isInArea(distance: Double, lastSection: Section?): Boolean {
        return lastSection?.to ?: 0.0 <= distance && (to ?: Double.MIN_VALUE) > distance
    }

}