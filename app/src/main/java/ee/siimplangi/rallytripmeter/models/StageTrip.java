package ee.siimplangi.rallytripmeter.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

/**
 * Created by Siim on 27.06.2017.
 */

public abstract class StageTrip extends TripModel implements Parcelable {

    private Long startTime;
    private Long endTime;

    public StageTrip() {
        super();
    }

    public StageTrip(Long startTimeSystemMillis) {
        this.startTime = startTimeSystemMillis;
    }

    protected StageTrip(Parcel in) {
        super(in);
        this.endTime = (Long) in.readValue(Long.class.getClassLoader());
        this.startTime = (Long) in.readValue(Long.class.getClassLoader());
    }

    @Override
    @Nullable
    public Float getAverageSpeed() {
        if (startTime == null) return null;
        long timeA = startTime;
        long timeB = endTime != null ? endTime : System.currentTimeMillis();
        float timePassed = (float) ((timeB - timeA) / (60 * 60 * 1000.0));
        if (timePassed <= 0) return null;
        float avg = (getTrip() / 1000.0f) / timePassed / 3.6f;
        return Math.max(0, avg);
    }


    @Override
    public TripControl getTripControl() {
        if (isRunning()) {
            return super.getTripControl();
        } else {
            return TripControl.PAUSE;
        }
    }

    public Long getEndTime() {
        return endTime;
    }

    public void end() {
        this.endTime = System.currentTimeMillis();
        if (this.endTime < this.startTime) this.endTime = this.startTime;
    }

    public boolean isRunning() {
        return startTime != null && startTime <= System.currentTimeMillis() && endTime == null;
    }

    public boolean isLessThanTimeToStart(long time) {
        return startTime != null && time >= startTime - System.currentTimeMillis();
    }

    public boolean isSet() {
        return startTime != null;
    }

    public long getElapsedTime() {
        if (hasEnded()) {
            return endTime - startTime;
        } else {
            return System.currentTimeMillis() - startTime;
        }

    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public long getTimeToStart() {
        return startTime - System.currentTimeMillis();
    }

    @Override
    public void resetTrip() {
        super.resetTrip();
    }

    public void reset() {
        resetTrip();
        this.startTime = null;
        this.endTime = null;
    }

    public void reset(long startTime) {
        reset();
        this.startTime = startTime;
    }


    public boolean hasEnded() {
        return startTime != null && endTime != null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(this.endTime);
        dest.writeValue(this.startTime);
    }

}

