package ee.siimplangi.rallytripmeter.models;

import android.location.Location;
import android.location.LocationProvider;

import com.google.android.gms.location.LocationAvailability;

/**
 * Created by Siim on 12.06.2017.
 */

public class GpsAccuracy {

    public final Accuracy ACCURACY;

    public GpsAccuracy(Location location) {
        this.ACCURACY = determineAccuracy(location);
    }

    public GpsAccuracy(LocationAvailability locationAvailability) {
        this.ACCURACY = determineAccuracy(locationAvailability);
    }

    public GpsAccuracy(int status) {
        this.ACCURACY = determineAccuracy(status);
    }

    public GpsAccuracy(Accuracy ACCURACY) {
        this.ACCURACY = ACCURACY;
    }

    private static Accuracy determineAccuracy(LocationAvailability locationAvailability) {
        if (locationAvailability.isLocationAvailable()) {
            return Accuracy.UNKOWN;
        } else {
            return Accuracy.NONE;
        }
    }

    private static Accuracy determineAccuracy(int status) {
        switch (status) {
            case LocationProvider.AVAILABLE:
                return Accuracy.UNKOWN;
            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                return Accuracy.SEARCHING;
            default:
                return Accuracy.NONE;
        }
    }

    private static Accuracy determineAccuracy(Location location) {
        if (!location.hasAccuracy()) {
            return Accuracy.UNKOWN;
        } else if (location.getAccuracy() < 20) {
            return Accuracy.GOOD;
        } else if (location.getAccuracy() < 50) {
            return Accuracy.AVERAGE;
        } else {
            return Accuracy.BAD;
        }
    }

    public enum Accuracy {
        UNKOWN, GOOD, AVERAGE, BAD, SEARCHING, NONE;
    }

}
