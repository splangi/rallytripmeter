package ee.siimplangi.rallytripmeter.models

import android.os.Parcel
import android.os.Parcelable


class SectionTrip : StageTrip, Parcelable {

    constructor() : super()
    constructor(startTimeSystemMillis: Long?) : super(startTimeSystemMillis)
    constructor(parcel: Parcel?) : super(parcel)

    override fun getAverageSpeed(): Float? {
        if (startTime == null) return null
        val timeA = if (startTime > lastResetTime) startTime else lastResetTime
        val timeB = if (endTime != null) endTime else System.currentTimeMillis()
        val timePassed = ((timeB - timeA) / (60.0 * 60.0 * 1000.0)).toFloat()
        if (timePassed <= 0f) return null
        val avg = trip / 1000.0f / timePassed / 3.6f
        return Math.max(0f, avg)
    }
}
