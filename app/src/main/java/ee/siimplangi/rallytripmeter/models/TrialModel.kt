package ee.siimplangi.rallytripmeter.models

import com.google.firebase.database.Exclude
import ee.siimplangi.rallytripmeter.enums.ProductStatus
import ee.siimplangi.rallytripmeter.helpers.Settings


data class TrialModel(
        val identifier: String? = null,
        val trialLength: Long? = null,
        val deviceID: String? = Settings.DEVICE_UNIQUE_ID,
        val systemTimeStamp: Long = System.currentTimeMillis(),
        val timeStamp: FirebaseTimeStamp? = FirebaseTimeStamp.getServerTimeStamp(),
        val latestReadTS: FirebaseTimeStamp = FirebaseTimeStamp.getServerTimeStamp()
) {

    @Exclude
    fun getStatus(): ProductStatus {
        return when {
            trialLength == null || timeStamp == null || identifier == null -> ProductStatus.TRIAL_NOT_STARTED
            Math.max(latestReadTS.timeStampLong, System.currentTimeMillis()) - Math.min(timeStamp.timeStampLong, systemTimeStamp) > trialLength -> ProductStatus.TRIAL_EXPIRED
            Math.max(latestReadTS.timeStampLong, System.currentTimeMillis()) - Math.min(timeStamp.timeStampLong, systemTimeStamp) > (2.0 / 3.0) * trialLength -> ProductStatus.TRIAL_ABOUT_TO_EXPIRE
            else -> ProductStatus.TRIAL_VALID
        }
    }

    @Exclude
    fun getTimeToExpire(): Long? {
        return timeStamp?.timeStampLong?.let { Math.max(latestReadTS.timeStampLong, System.currentTimeMillis()) - it }
    }

    @Exclude
    fun getTimeToExpireInDays(): Long? {
        return getTimeToExpire()?.div(1000 * 60 * 60 * 24)
    }


}