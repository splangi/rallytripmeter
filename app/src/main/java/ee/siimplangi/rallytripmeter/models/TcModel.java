package ee.siimplangi.rallytripmeter.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

import androidx.annotation.Nullable;
import ee.siimplangi.rallytripmeter.events.AllowedTime;
import ee.siimplangi.rallytripmeter.events.TCOutTime;

/**
 * Created by Siim on 24.06.2017.
 */

public class TcModel implements Parcelable {

    public static final Parcelable.Creator<TcModel> CREATOR = new Parcelable.Creator<TcModel>() {
        @Override
        public TcModel createFromParcel(Parcel source) {
            return new TcModel(source);
        }

        @Override
        public TcModel[] newArray(int size) {
            return new TcModel[size];
        }
    };
    private AllowedTime allowedTime;
    private TCOutTime tcOutTime;

    public TcModel() {
    }

    protected TcModel(Parcel in) {
        this.allowedTime = in.readParcelable(AllowedTime.class.getClassLoader());
        this.tcOutTime = in.readParcelable(TCOutTime.class.getClassLoader());
    }

    @Nullable
    public Calendar getNextTcInRallyCalendar() {
        if (!isComplete()) return null;
        Calendar cal = tcOutTime.getCalendar();
        cal.add(Calendar.MINUTE, allowedTime.MINUTES);
        cal.add(Calendar.SECOND, allowedTime.SECONDS);
        cal.add(Calendar.HOUR_OF_DAY, allowedTime.HOUR);
        return cal;
    }

    public boolean isComplete() {
        return !(allowedTime == null || tcOutTime == null);
    }

    public
    @Nullable
    AllowedTime getAllowedTime() {
        return allowedTime;
    }

    public void setAllowedTime(AllowedTime allowedTime) {
        this.allowedTime = allowedTime;
    }

    public
    @Nullable
    TCOutTime getTcOutTime() {
        return tcOutTime;
    }

    public void setTcOutTime(TCOutTime tcOutTime) {
        this.tcOutTime = tcOutTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.allowedTime, flags);
        dest.writeParcelable(this.tcOutTime, flags);
    }
}
