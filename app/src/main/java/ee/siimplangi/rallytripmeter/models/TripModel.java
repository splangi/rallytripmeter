package ee.siimplangi.rallytripmeter.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;
import ee.siimplangi.rallytripmeter.enums.TripType;


public abstract class TripModel implements Parcelable {

    public static final Creator<TripModel> CREATOR = new Creator<TripModel>() {
        @Override
        public TripModel createFromParcel(Parcel source) {
            return createNewModel(TripType.valueOf(source.readString()), source);
        }

        @Override
        public TripModel[] newArray(int size) {
            return new TripModel[size];
        }
    };
    private float trip;
    protected long lastResetTime;
    private float maxSpeedMS;
    private TripControl tripControl;

    protected TripModel() {
        trip = 0;
        maxSpeedMS = 0;
        this.lastResetTime = System.currentTimeMillis();
        tripControl = TripControl.READ_UP;
    }

    protected TripModel(Parcel in) {
        this.trip = in.readFloat();
        this.lastResetTime = in.readLong();
        this.maxSpeedMS = in.readFloat();
        this.tripControl = TripControl.valueOf(in.readString());
    }

    public static TripModel createNewModel(TripType tripType, Parcel parcel) {
        switch (tripType) {
            case TRIP1:
                return new Trip1(parcel);
            case TRIP2:
                return new Trip2(parcel);
            case STAGE:
                return new SpecialStageTrip(parcel);
            case SECTION:
                return new SectionTrip(parcel);
        }
        throw new IllegalStateException("No Tripmodel");
    }


    public float getMaxSpeedMS() {
        return maxSpeedMS;
    }

    public void setSpeed(float maxSpeedMS) {
        if (this.maxSpeedMS < maxSpeedMS) {
            this.maxSpeedMS = maxSpeedMS;
        }
    }

    public float getTrip() {
        return trip;
    }


    public void add(float meters) {
        this.trip = this.trip + meters;
    }

    @Nullable
    public Float getAverageSpeed() {
        float timePassed = (float) ((System.currentTimeMillis() - lastResetTime) / (60 * 60 * 1000.0));
        if (timePassed == 0) return null;
        return (getTrip() / 1000.0f) / timePassed / 3.6f;
    }

    public void setTrip(float newTrip) {
        this.trip = newTrip;
    }

    public void resetTrip() {
        this.trip = 0;
        this.maxSpeedMS = 0;
        this.lastResetTime = System.currentTimeMillis();
    }

    public TripControl getTripControl() {
        return tripControl;
    }

    public void setTripControl(TripControl tripControl) {
        this.tripControl = tripControl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(TripType.getTripTypeForClass(this.getClass()).name());
        dest.writeFloat(this.trip);
        dest.writeLong(this.lastResetTime);
        dest.writeFloat(this.maxSpeedMS);
        dest.writeString(this.tripControl.name());
    }

    public static class Trip1 extends TripModel {

        public Trip1() {
            super();
        }

        private Trip1(Parcel in) {
            super(in);
        }
    }

    public static class Trip2 extends TripModel {

        public Trip2() {
            super();
        }

        private Trip2(Parcel in) {
            super(in);
        }
    }

    public static class Average extends TripModel {

        public Average() {
            super();
        }

        private Average(Parcel in) {
            super(in);
        }
    }


}
