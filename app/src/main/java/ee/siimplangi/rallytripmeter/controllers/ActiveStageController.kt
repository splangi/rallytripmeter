package ee.siimplangi.rallytripmeter.controllers

import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import ee.siimplangi.rallytripmeter.AppLog
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants
import ee.siimplangi.rallytripmeter.events.ActiveStage
import ee.siimplangi.rallytripmeter.events.ChangeActiveStage
import ee.siimplangi.rallytripmeter.models.StageDef
import ee.siimplangi.rallytripmeter.storage.Storage
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class ActiveStageController(savedInstanceState: Bundle? = null) : BaseController(savedInstanceState) {

    val KEY_STAGE = "active_stage"
    val KEY_SECTION = "active_section"

    var activeStageDef: StageDef? = null
    //var activeSection: Int = savedInstanceState?.getInt(KEY_SECTION, 0) ?: 0
    var activeStageKey: String? = Storage.activeStage

    var ref: DatabaseReference? = null


    init {

    }


    override fun onSaveInstanceState(): Bundle {
        return Bundle().also {
            //it.putInt(KEY_SECTION, activeSection)
        }
    }

    override fun onStart() {
        super.onStart()
        activeStageKey = Storage.activeStage
        FirebaseAuth.getInstance().addAuthStateListener(authStateListener)
        registerNewStageListener()

    }

    override fun onStop() {
        Storage.activeStage = activeStageKey
        FirebaseAuth.getInstance().removeAuthStateListener(authStateListener)
        ref?.removeEventListener(activeStageListener)
        ref = null
        super.onStop()
    }

    private val authStateListener = FirebaseAuth.AuthStateListener {
        if (it.currentUser == null) {
            activeStageDef = null
            activeStageKey = null
            notifyListeners()
        }
    }

    private val activeStageListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {
            AppLog.e(ActiveStageController::class.java.simpleName, p0)
        }

        override fun onDataChange(p0: DataSnapshot) {
            activeStageDef = p0.getValue(StageDef::class.java)
            notifyListeners()
        }

    }

    private fun registerNewStageListener() {
        activeStageDef = null
        ref?.removeEventListener(activeStageListener)
        ref = null
        if (activeStageKey != null) {
            ref = FirebaseDatabase.getInstance().getReference(FirebaseConstants.STAGES_NODE + "/" + activeStageKey)
            ref?.addValueEventListener(activeStageListener)
        } else {
            notifyListeners()
        }
    }

    /*@Subscribe
    fun onChangeSection (sectionAdvancer: SectionAdvancer){
        val stageDef = activeStageDef
        if (stageDef != null){
            if (sectionAdvancer == SectionAdvancer.NEXT && stageDef.sections.size > activeSection + 1){
                activeSection += 1
                notifyListeners()
            } else if (sectionAdvancer == SectionAdvancer.PREV && activeSection > 0){
                activeSection -= 1
                notifyListeners()
            }
        }

    }*/

    fun notifyListeners() {
        EventBus.getDefault().postSticky(ActiveStage(activeStageKey, activeStageDef))
    }

    @Subscribe
    fun onActiveStageChange(change: ChangeActiveStage) {
        this.activeStageKey = change.activeStageKey
        registerNewStageListener()
    }


}