package ee.siimplangi.rallytripmeter.controllers;

import android.os.Bundle;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Siim on 25.06.2017.
 */

public abstract class BaseController {

    public BaseController() {

    }

    public BaseController(Bundle savedInstance) {

    }

    public void onStart() {
        EventBus.getDefault().register(this);
    }

    public void onStop() {
        EventBus.getDefault().unregister(this);
    }

    public abstract Bundle onSaveInstanceState();

}
