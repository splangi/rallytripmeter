package ee.siimplangi.rallytripmeter.controllers;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.annotation.CallSuper;
import ee.siimplangi.rallytripmeter.enums.TripType;
import ee.siimplangi.rallytripmeter.enums.Unit;
import ee.siimplangi.rallytripmeter.events.ResetEvent;
import ee.siimplangi.rallytripmeter.events.TripControlEvent;
import ee.siimplangi.rallytripmeter.events.TripEditEvent;
import ee.siimplangi.rallytripmeter.events.TripPlus;
import ee.siimplangi.rallytripmeter.helpers.Settings;
import ee.siimplangi.rallytripmeter.helpers.converters.UnitConverter;
import ee.siimplangi.rallytripmeter.interpolators.DistanceInterpolator;
import ee.siimplangi.rallytripmeter.models.TripControl;
import ee.siimplangi.rallytripmeter.models.TripModel;

/**
 * Created by Siim on 23.06.2017.
 */

public class TripController<T extends TripModel> extends BaseController {

    private static final String KEY_LOCATION = "location";
    private static final String KEY_TRIPMODEL = "model";
    private static final String KEY_PAUSED = "paused";
    private static final String KEY_SCHEDULER = "scheduler";
    private static final String KEY_INTERPOLATOR = "interpolator";
    private static final String KEY_LAST_PREDICITION_DISTANCE = "last_dist";
    private T tripModel;
    private float lastDistance = 0;

    private TripType tripType;

    private Scheduler scheduler;

    private Location lastLocation;
    private boolean paused = false;

    private DistanceInterpolator distanceInterpolator;
    private Runnable updater = new Runnable() {
        @Override
        public void run() {
            updateModel();
        }
    };

    public TripController(Bundle instanceState) {
        this((T) instanceState.getParcelable(KEY_TRIPMODEL), instanceState);
    }

    public TripController(T tripModel, Bundle instanceState) {
        this.tripModel = tripModel;
        lastLocation = instanceState.getParcelable(KEY_LOCATION);
        tripType = TripType.getTripTypeForClass(tripModel.getClass());
        distanceInterpolator = instanceState.getParcelable(KEY_INTERPOLATOR);
        scheduler = getSchedulerForType(UpdateSchedulerType.valueOf(instanceState.getString(KEY_SCHEDULER)));
        lastDistance = instanceState.getFloat(KEY_LAST_PREDICITION_DISTANCE);
    }

    public TripController(T tripModel, DistanceInterpolator distanceInterpolator, UpdateSchedulerType type) {
        this.tripType = TripType.getTripTypeForClass(tripModel.getClass());
        this.tripModel = tripModel;
        this.distanceInterpolator = distanceInterpolator;
        this.scheduler = getSchedulerForType(type);
        lastDistance = distanceInterpolator.predict();
    }


    protected T getTripModel() {
        return tripModel;
    }

    /*public TripController(TripType tripType, DistanceInterpolator distanceInterpolator, UpdateSchedulerType type) {
        this(TripModel.createNewModel(tripType), distanceInterpolator, type);
    }*/

    protected void setTripModel(T tripModel) {
        this.tripModel = tripModel;
        updateModel();
    }

    private Scheduler getSchedulerForType(UpdateSchedulerType type) {
        switch (type) {
            case SPEED:
                return new SpeedScheduler(updater);
            case SIMPLE:
                return new SimpleScheduler(updater);
        }
        throw new IllegalStateException("No Schedulers for Type");
    }

    @Subscribe(sticky = true)
    @CallSuper
    public void onLocation(Location location) {
        distanceInterpolator.onNewRealLocation(location);
        lastLocation = location;
        scheduler.onLocation(location);
        updateModelSpeed();
    }

    @Subscribe
    public void on(TripPlus add) {
        if (tripModel.getTripControl() != TripControl.PAUSE) {
            updateModel();
            this.tripModel.add(add.getMeters());
            updateModel();
        }
    }

    protected void updateTotalDistance() {
        lastDistance = distanceInterpolator.predict();
    }

    protected void updateModelSpeed() {
        float speed = lastLocation != null ? lastLocation.getSpeed() : 0;
        switch (tripModel.getTripControl()) {
            case READ_UP:
            case READ_DOWN:
                tripModel.setSpeed(speed);
                break;
        }
        EventBus.getDefault().postSticky(tripModel);

    }

    protected void updateModel() {
        float totalDistance = distanceInterpolator.predict();
        float add = 0;
        float speed = lastLocation != null ? lastLocation.getSpeed() : 0;
        switch (tripModel.getTripControl()) {
            case READ_UP:
                add = (totalDistance - lastDistance);
                tripModel.setSpeed(speed);
                break;
            case READ_DOWN:
                add = -(totalDistance - lastDistance);
                tripModel.setSpeed(speed);
                break;
            case PAUSE:
                add = 0;
                break;
        }
        lastDistance = totalDistance;
        tripModel.add(add);
        EventBus.getDefault().postSticky(tripModel);
        scheduler.onTripModelUpdate(tripModel);
    }

    @Subscribe
    public void onResetEvent(ResetEvent resetEvent) {
        if (tripType.equals(resetEvent.TYPE)) {
            resetTrip();
        }
    }

    public void setTrip(float trip) {
        updateTotalDistance();
        tripModel.setTrip(trip);
        updateModel();
    }

    @Subscribe
    public void onTripControlEvent(TripControlEvent tripControl) {
        if (tripControl.tripType.equals(tripType)) {
            //Fix the new distance at the time of change of direction
            updateModel();
            tripModel.setTripControl(tripControl.tripControl);
            updateModel();
        }
    }

    @Subscribe
    public void onTripEditEvent(TripEditEvent tripEditEvent) {
        if (tripEditEvent.tripType.equals(tripType)) {
            updateTotalDistance();
            tripModel.setTrip(tripEditEvent.newTrip);
            updateModel();
        }
    }

    @Subscribe
    public void onUnitChangeEvent(Unit unit) {
        scheduler.onUnitChange(unit);
    }

    protected void resetTrip() {
        updateTotalDistance();
        tripModel.resetTrip();
        updateModel();
    }

    @Override
    public Bundle onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_LOCATION, lastLocation);
        bundle.putParcelable(KEY_TRIPMODEL, tripModel);
        bundle.putBoolean(KEY_PAUSED, paused);
        bundle.putParcelable(KEY_INTERPOLATOR, distanceInterpolator);
        bundle.putString(KEY_SCHEDULER, scheduler.getType().name());
        bundle.putFloat(KEY_LAST_PREDICITION_DISTANCE, lastDistance);
        return bundle;
    }

    @Override
    public void onStart() {
        super.onStart();
        updateModel();
        scheduler.schedule();
    }

    @Override
    public void onStop() {
        super.onStop();
        scheduler.cancel();
    }

    public enum UpdateSchedulerType {

        SPEED, SIMPLE, TEST

    }

    private abstract class Scheduler {

        protected final Runnable scheduledRunnable;

        private Scheduler(Runnable runnable) {
            this.scheduledRunnable = runnable;
        }

        public abstract void cancel();

        public abstract void onLocation(Location location);

        public abstract void onUnitChange(Unit unit);

        public abstract void onTripModelUpdate(TripModel tripModel);

        protected abstract void schedule();

        protected abstract UpdateSchedulerType getType();

    }


    private class SimpleScheduler extends Scheduler {

        private SimpleScheduler(Runnable runnable) {
            super(runnable);
        }

        @Override
        public void cancel() {

        }

        @Override
        public void onLocation(Location location) {
            schedule();
        }

        @Override
        public void onUnitChange(Unit unit) {
            schedule();
        }

        @Override
        public void onTripModelUpdate(TripModel tripModel) {

        }

        @Override
        protected void schedule() {
            scheduledRunnable.run();
        }

        @Override
        protected UpdateSchedulerType getType() {
            return UpdateSchedulerType.SIMPLE;
        }
    }


    private class SpeedScheduler extends Scheduler {

        private static final float additionToNextInterpolation = 0;
        private static final float interpolationFrequency = 10F;
        private static final long maxSchedulingTime = 1000L;
        private UnitConverter unitConverter;

        private Handler handler = new Handler(Looper.getMainLooper());

        private SpeedScheduler(Runnable runnable) {
            super(runnable);
            onUnitChange(Settings.INSTANCE.getUnit());
        }


        @Override
        public void cancel() {
            handler.removeCallbacks(scheduledRunnable);
        }

        @Override
        public void onLocation(Location location) {
            scheduledRunnable.run();
            schedule();
        }

        @Override
        public void onUnitChange(Unit unit) {
            unitConverter = new UnitConverter(unit, Unit.METRIC);
        }

        @Override
        public void onTripModelUpdate(TripModel tripModel) {
            schedule();
        }

        @Override
        protected void schedule() {
            cancel();
            float distanceToNextPrediciton;
            boolean negative = tripModel.getTrip() < 0;
            TripControl tripControl = tripModel.getTripControl();
            if (tripControl.equals(TripControl.READ_DOWN) && !negative || tripControl.equals(TripControl.READ_UP) && negative) {
                distanceToNextPrediciton = (Math.abs(tripModel.getTrip()) % getInterpolationFrequency()) + additionToNextInterpolation;
            } else if (tripControl.equals(TripControl.READ_UP) && !negative || tripControl.equals(TripControl.READ_DOWN) && negative) {
                distanceToNextPrediciton = (getInterpolationFrequency() - (Math.abs(tripModel.getTrip()) % getInterpolationFrequency())) + additionToNextInterpolation;
            } else {
                return;
            }
            long timeToNextPrediction = distanceInterpolator.getTimeToNextDistance(lastDistance + distanceToNextPrediciton);
            handler.postDelayed(scheduledRunnable, Math.min(timeToNextPrediction, maxSchedulingTime));
            /*if (timeToNextPrediction < maxSchedulingTime && TimeHelper.getElapsedTime() - TimeHelper.getLocationElapsedTime(lastLocation) < maxSchedulingTime) {

            }*/
        }

        @Override
        protected UpdateSchedulerType getType() {
            return UpdateSchedulerType.SPEED;
        }

        private float getInterpolationFrequency() {
            return unitConverter.convert(interpolationFrequency);
        }


    }

}
