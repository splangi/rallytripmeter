package ee.siimplangi.rallytripmeter.controllers;

import android.os.Bundle;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import ee.siimplangi.rallytripmeter.events.AllowedTime;
import ee.siimplangi.rallytripmeter.events.TCOutTime;
import ee.siimplangi.rallytripmeter.models.RallyTime;
import ee.siimplangi.rallytripmeter.models.TcModel;

public class TCController extends BaseController {

    private static final String TCMODEL_KEY = "tcmodel";
    private TcModel tcModel;

    public TCController() {
        super();
        tcModel = new TcModel();
        updateTcModel();
    }

    public TCController(Bundle savedInstaneState) {
        super(savedInstaneState);
        tcModel = savedInstaneState.getParcelable(TCMODEL_KEY);
        updateTcModel();
    }

    @Subscribe
    public void onNewTimeSet(AllowedTime allowedTime) {
        tcModel.setAllowedTime(allowedTime);
        updateTcModel();
    }

    @Subscribe
    public void onNewTCOutTime(TCOutTime outTime) {
        tcModel.setTcOutTime(outTime);
        updateTcModel();
    }

    @Subscribe
    public void onNewRallyTime(RallyTime rallyTime) {
        updateTcModel();
    }

    private void updateTcModel() {
        EventBus.getDefault().postSticky(tcModel);
    }

    @Override
    public Bundle onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TCMODEL_KEY, tcModel);
        return bundle;
    }
}
