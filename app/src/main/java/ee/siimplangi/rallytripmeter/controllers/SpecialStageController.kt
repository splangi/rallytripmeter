package ee.siimplangi.rallytripmeter.controllers

import android.os.Bundle
import android.os.Handler
import ee.siimplangi.rallytripmeter.events.ActiveStage
import ee.siimplangi.rallytripmeter.events.NewStage
import ee.siimplangi.rallytripmeter.events.SectionAdvancer
import ee.siimplangi.rallytripmeter.events.StopStage
import ee.siimplangi.rallytripmeter.interpolators.LinearDistanceInterpolator
import ee.siimplangi.rallytripmeter.models.RallyTime
import ee.siimplangi.rallytripmeter.models.SectionTrip
import ee.siimplangi.rallytripmeter.models.SpecialStage
import ee.siimplangi.rallytripmeter.models.SpecialStageTrip
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Created by Siim on 24.06.2017.
 */

class SpecialStageController(savedInstance: Bundle? = null) : BaseController(savedInstance) {

    companion object {

        private const val KEY_SS_TRIP = "ssTrip"
        private const val KEY_SECTION_TRIP = "sectionTrip"
        private const val KEY_SS_SECTION = "currentSection"
        private const val KEY_CURRENT_SS = "currentStage"

    }

    private val handler = Handler()

    private var ssSection: Int = savedInstance?.getInt(KEY_SS_SECTION) ?: 0

    private val ss: SpecialStage = savedInstance?.getParcelable(KEY_CURRENT_SS) ?: SpecialStage()


    override fun onSaveInstanceState(): Bundle {
        return Bundle().also {
            it.putBundle(KEY_SS_TRIP, ssTripController.onSaveInstanceState())
            it.putBundle(KEY_SECTION_TRIP, ssSectionTripController.onSaveInstanceState())
            it.putInt(KEY_SS_SECTION, ssSection)
            it.putParcelable(KEY_CURRENT_SS, ss)
        }
    }

    private val ssTripController: StageTripController<SpecialStageTrip>
    private val ssSectionTripController: StageTripController<SectionTrip>


    init {
        this.ssTripController = savedInstance?.getBundle(KEY_SS_TRIP)?.let { StageTripController<SpecialStageTrip>(it) } ?: StageTripController(SpecialStageTrip(), LinearDistanceInterpolator(), TripController.UpdateSchedulerType.SPEED)
        this.ssSectionTripController = savedInstance?.getBundle(KEY_SECTION_TRIP)?.let { StageTripController<SectionTrip>(it) } ?: StageTripController(SectionTrip(), LinearDistanceInterpolator(), TripController.UpdateSchedulerType.SPEED)
    }

    override fun onStart() {
        super.onStart()
        ssTripController.onStart()
        ssSectionTripController.onStart()
        updateModel()
        scheduleTicker()
    }

    override fun onStop() {
        ssTripController.onStop()
        ssSectionTripController.onStop()
        handler.removeCallbacksAndMessages(null)
        super.onStop()
    }

    private fun scheduleTicker() {
        val startTime = ss.startTime
        if (!ss.isRunning() && !ss.hasEnded() && startTime != null) {
            val delay = startTime - System.currentTimeMillis() + 1
            handler.postDelayed({ onStageStart() }, Math.max(1, delay))
        }
    }

    fun updateModel() {
        EventBus.getDefault().postSticky(ss)
    }

    @Subscribe
    fun onNewStage(newStage: NewStage) {
        ssTripController.reset(newStage.START_TIME)
        ssSectionTripController.reset(newStage.START_TIME)
        ss.reset(newStage.START_TIME)
        updateModel()
        scheduleTicker()
    }

    @Subscribe
    fun onRallyTimeUpdate(rallyTime: RallyTime) {
        ssTripController.reset()
        ssSectionTripController.reset()
        ss.reset()
        updateModel()
    }

    @Subscribe
    fun onStopStage(stage: StopStage) {
        ssTripController.stopStage()
        ssSectionTripController.stopStage()
        ss.end()
        updateModel()
    }

    @Subscribe(sticky = true)
    fun onActiveStageChange(change: ActiveStage) {
        ss.stageDef = change.stageDef
        ss.currentSection = 0
        updateModel()

    }

    @Subscribe
    fun onChangeSection(sectionAdvancer: SectionAdvancer) {
        val stageDef = ss.stageDef
        if (ss.isRunning() && stageDef != null) {
            if (sectionAdvancer == SectionAdvancer.NEXT && stageDef.sections.size > ss.currentSection + 1) {
                stageDef.sections.getOrNull(ss.currentSection)?.to?.toFloat()?.let { ssTripController.setTrip(it) }
                ssSectionTripController.reset(ss.startTime!!)
                ss.currentSection += 1
                updateModel()
            } else if (sectionAdvancer == SectionAdvancer.PREV && ss.currentSection >= 0) {
                ssTripController.setTrip(stageDef.sections.getOrNull(ss.currentSection - 1)?.to?.toFloat()
                        ?: 0F)
                ssSectionTripController.reset(ss.startTime!!)
                if (ss.currentSection > 0) {
                    ss.currentSection -= 1
                }
                updateModel()
            }
        }

    }

    private fun onStageStart() {
        ssTripController.startStage()
        ssSectionTripController.startStage()
        updateModel()
    }


}
