package ee.siimplangi.rallytripmeter.controllers;

import android.os.Bundle;
import android.os.Handler;

import ee.siimplangi.rallytripmeter.interpolators.DistanceInterpolator;
import ee.siimplangi.rallytripmeter.models.StageTrip;

/**
 * Created by Siim on 24.06.2017.
 */

public class StageTripController<T extends StageTrip> extends TripController<T> {

    private Handler handler = new Handler();

    public StageTripController(Bundle instanceState) {
        super(instanceState);
    }

    public StageTripController(T tripModel, Bundle instanceState) {
        super(tripModel, instanceState);
    }

    public StageTripController(T tripModel, DistanceInterpolator distanceInterpolator, UpdateSchedulerType type) {
        super(tripModel, distanceInterpolator, type);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateModel();
    }

    @Override
    public void onStop() {
        handler.removeCallbacksAndMessages(null);
        super.onStop();
    }


    public void reset() {
        getTripModel().reset();
        updateModel();
    }

    public void reset(long newStageTime) {
        getTripModel().reset(newStageTime);
        updateModel();
    }

    public void stopStage() {
        getTripModel().end();
        updateModel();
    }

    public void startStage() {
        updateTotalDistance();
        updateModel();
    }


}