package ee.siimplangi.rallytripmeter.enums;

import android.content.Context;

import androidx.annotation.StringRes;
import ee.siimplangi.rallytripmeter.R;

public enum Unit {

    METRIC(1.0f, R.string.km, R.string.kph, 0), IMPERIAL(1 / 1.609344f, R.string.mi, R.string.mph, 1);

    public final float MULTIPLIER;
    @StringRes
    private final int distanceStringResId;
    @StringRes
    private final int speedStringResId;
    private final int prefsValue;

    Unit(float multiplier, int distanceStringResId, int speedStringResId, int prefsValue) {
        this.MULTIPLIER = multiplier;
        this.distanceStringResId = distanceStringResId;
        this.speedStringResId = speedStringResId;
        this.prefsValue = prefsValue;
    }

    public static Unit getUnitByKey(int value) {
        for (Unit unit : Unit.values()) {
            if (unit.prefsValue == value) {
                return unit;
            }
        }
        return METRIC;
    }


    public String getDistanceString(Context context) {
        return context.getResources().getString(distanceStringResId);
    }

    public String getSpeedString(Context context) {
        return context.getResources().getString(speedStringResId);
    }

    /*public Sku getDistanceString(Context context, float meters) {
        return (df.format((meters * MULTIPLIER))) + getDistanceString(context);
    }

    public Sku getSpeedString(Context context, float speedMS) {
        return (df.format((speedMS * 3.6f) * MULTIPLIER)) + getSpeedString(context);
    }*/

}


