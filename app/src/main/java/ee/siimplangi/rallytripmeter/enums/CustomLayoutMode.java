package ee.siimplangi.rallytripmeter.enums;

import androidx.annotation.IdRes;
import androidx.annotation.StringRes;
import ee.siimplangi.rallytripmeter.R;

/**
 * Created by Siim on 18.09.2017.
 */

public enum CustomLayoutMode {

    LAYOUT_1(R.string.prefs_file_custom_layout_1, R.id.custom_1, R.string.custom_layout_1),
    LAYOUT_2(R.string.prefs_file_custom_layout_2, R.id.custom_2, R.string.custom_layout_2),
    LAYOUT_3(R.string.prefs_file_custom_layout_3, R.id.custom_3, R.string.custom_layout_3);

    @StringRes
    private int prefsFile;

    @IdRes
    private int navigationId;

    @StringRes
    private int title;

    CustomLayoutMode(@StringRes int prefsFile, @IdRes int navigationId, @StringRes int title) {
        this.prefsFile = prefsFile;
        this.navigationId = navigationId;
        this.title = title;
    }

    public static CustomLayoutMode findByNavId(@IdRes int navigationId) {
        for (CustomLayoutMode mode : CustomLayoutMode.values()) {
            if (mode.navigationId == navigationId) {
                return mode;
            }
        }
        return null;
    }

    public int getPrefsFile() {
        return prefsFile;
    }

    public int getNavigationId() {
        return navigationId;
    }

    public int getTitle() {
        return title;
    }
}
