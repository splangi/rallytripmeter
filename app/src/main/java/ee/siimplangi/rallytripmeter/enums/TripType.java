package ee.siimplangi.rallytripmeter.enums;

import ee.siimplangi.rallytripmeter.models.SectionTrip;
import ee.siimplangi.rallytripmeter.models.SpecialStageTrip;
import ee.siimplangi.rallytripmeter.models.TripModel;

/**
 * Created by Siim on 27.06.2017.
 */

public enum TripType {

    TRIP1(TripModel.Trip1.class), TRIP2(TripModel.Trip2.class), STAGE(SpecialStageTrip.class), SECTION(SectionTrip.class);

    private Class<? extends TripModel> clazz;

    TripType(Class<? extends TripModel> clazz) {
        this.clazz = clazz;
    }

    public static TripType getTripTypeForClass(Class clazz) {
        for (TripType tripType : TripType.values()) {
            if (tripType.clazz.equals(clazz)) {
                return tripType;
            }
        }
        throw new IllegalStateException("No TripType found");
    }

    public Class<? extends TripModel> getModelClass() {
        return clazz;
    }
}
