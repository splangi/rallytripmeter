package ee.siimplangi.rallytripmeter.enums

import androidx.annotation.StringRes
import androidx.annotation.XmlRes
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.fragments.components.*
import kotlin.reflect.KClass

enum class ComponentType(val isCustom: Boolean,
                         @StringRes val heading: Int,
                         @StringRes val prefsFile: Int?,
                         val clazz: KClass<out BaseComponentFragment>,
                         @XmlRes vararg val prefsXMLs: Int) {

    TRIP1(true, R.string.component_trip1, R.string.prefs_file_trip1, Trip1::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    TRIP2(true, R.string.component_trip2, R.string.prefs_file_trip2, Trip2::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    SPEED_LIMIT(true, R.string.component_speed_limit, R.string.prefs_file_speed_limit, SpeedLimitFragment::class, R.xml.prefs_component_base),
    TRIP1_AVG(true, R.string.component_trip1_avg, R.string.prefs_file_trip1_avg, Average1::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    TRIP2_AVG(true, R.string.component_trip2_avg, R.string.prefs_file_trip2_avg, Average2::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    CURRENT_SPEED(true, R.string.component_current_speed, R.string.prefs_file_current_speed, CurrentSpeed::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    LAST_TC(true, R.string.component_last_TC, R.string.prefs_file_last_TC, LastTcOutComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    ALLOWED_TIME(true, R.string.component_allowed_time, R.string.prefs_file_allowed_time, AllowedTimeComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    NEXT_TC(true, R.string.component_next_TC, R.string.prefs_file_next_TC, NextTcInComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    TIME_REMAINING(true, R.string.component_time_remaining, R.string.prefs_file_time_remaining, NextTcInTimerComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    STAGE_TIMER(true, R.string.component_stage_timer, R.string.prefs_file_stage_time, StageStopperComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    STAGE_TRIP(true, R.string.component_stage_trip, R.string.prefs_file_stage_trip, StageTripComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text, R.xml.prefs_component_section),
    STAGE_MAX(true, R.string.component_stage_max_speed, R.string.prefs_file_stage_max_speed, StageMaxSpeedComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    STAGE_AVG(true, R.string.component_stage_avg, R.string.prefs_file_stage_avg, StageAverageSpeedComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    HEADING(true, R.string.component_heading, R.string.prefs_file_heading, HeadingComponentFragment::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    PLUS100(true, R.string.component_trip_edit_plus100, R.string.prefs_file_trip_edit_plus100, Add100::class, R.xml.prefs_component_button),
    PLUS10(true, R.string.component_trip_edit_plus10, R.string.prefs_file_trip_edit_plus10, Add10::class, R.xml.prefs_component_button),
    MINUS100(true, R.string.component_trip_edit_minus100, R.string.prefs_file_trip_edit_minus100, Remove100::class, R.xml.prefs_component_button),
    MINUS10(true, R.string.component_trip_edit_minus10, R.string.prefs_file_trip_edit_minus10, Remove10::class, R.xml.prefs_component_button),
    SECTION_TRIP(true, R.string.component_section_trip, R.string.prefs_file_section_trip, SectionTripComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    SECTION_AVG(true, R.string.component_section_avg, R.string.prefs_file_section_avg, SectionAverageSpeedComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    RESET_TRIP1(true, R.string.component_reset_trip1, R.string.prefs_file_reset_trip_1, ResetTrip1::class, R.xml.prefs_component_button),
    RESET_TRIP2(true, R.string.component_reset_trip2, R.string.prefs_file_reset_trip_2, ResetTrip2::class, R.xml.prefs_component_button),
    RESET_SECTION(true, R.string.component_reset_section, R.string.prefs_file_reset_section, ResetSectionTrip::class, R.xml.prefs_component_button),
    NEXT_SECTION(true, R.string.component_next_section, R.string.prefs_file_next_section, NextSectionButton::class, R.xml.prefs_component_button, R.xml.prefs_component_section),
    PREV_SECTION(true, R.string.component_prev_section, R.string.prefs_file_prev_section, PrevSectionButton::class, R.xml.prefs_component_button),
    SECTION_FROM(true, R.string.component_section_from, R.string.prefs_file_section_from, SectionFromComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    SECTION_TO(true, R.string.component_section_to, R.string.prefs_file_section_to, SectionToComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    TARGET_SPEED(true, R.string.component_section_target, R.string.prefs_file_target_speed, SectionSpeedComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text),
    IDEAL_TIME(true, R.string.component_ideal_time, R.string.prefs_file_ideal_time, TimeToIdeal::class, R.xml.prefs_component_base, R.xml.prefs_component_text, R.xml.prefs_component_ideal_time),
    SELECT_STAGE(true, R.string.component_select_stage, R.string.prefs_file_active_stage_selection, SelectActiveStageComponent::class, R.xml.prefs_component_base, R.xml.prefs_component_text),

    RALLY_TIME(false, R.string.component_rally_time, null, RallyTimeComponent::class),
    TRIP_TOOLBAR(false, R.string.component_trip_toolbar, null, TripControlToolbar::class),
    GPS_SIGNAL(false, R.string.component_gps_signal, null, GpsSignalComponent::class)

}
