package ee.siimplangi.rallytripmeter.enums;

/**
 * Created by Siim on 04.09.2017.
 */

public enum CRUDAction {

    CREATE, READ, UPDATE, DELETE

}
