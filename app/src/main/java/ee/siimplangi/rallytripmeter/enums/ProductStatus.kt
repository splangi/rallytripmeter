package ee.siimplangi.rallytripmeter.enums

enum class ProductStatus {

    NOT_LOGGED_IN,
    TRIAL_NOT_STARTED,
    TRIAL_VALID,
    TRIAL_EXPIRED,
    TRIAL_ABOUT_TO_EXPIRE,
    VALID_BILLING;

    fun isProductActivated(): Boolean {
        return this == TRIAL_VALID || this == VALID_BILLING
    }

}