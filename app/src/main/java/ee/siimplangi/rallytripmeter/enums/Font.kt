package ee.siimplangi.rallytripmeter.enums

import android.graphics.Typeface
import androidx.annotation.FontRes
import androidx.annotation.StringRes
import androidx.core.content.res.ResourcesCompat
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.RallyTripmeter

object FontHelper {
    fun creatorByRes(@FontRes id: Int): () -> Typeface? {
        return {
            getFont(id)
        }
    }

    fun creatorByNameAndStyle(name: String, style: Int = Typeface.NORMAL): () -> Typeface? {
        return {
            Typeface.create(name, style)
        }
    }

    fun getFont(@FontRes id: Int): Typeface? = ResourcesCompat.getFont(RallyTripmeter.app, id)
}

enum class Font(private val fontCreator: () -> Typeface?, @StringRes val heading: Int) {

    CARTOON(FontHelper.creatorByRes(R.font.cartoon), R.string.cartoon),
    CALCULATOR(FontHelper.creatorByRes(R.font.calculator), R.string.calculator),
    HANDWRITING(FontHelper.creatorByRes(R.font.handwriting), R.string.handwriting),
    GAME(FontHelper.creatorByRes(R.font.old_videogame), R.string.video_game),
    WRC_NORMAL(FontHelper.creatorByRes(R.font.wrc_normal), R.string.wrc_normal),
    WRC_BOLD(FontHelper.creatorByRes(R.font.wrc_bold), R.string.wrc_bold),
    SYSTEM_NORMAL(FontHelper.creatorByNameAndStyle("sans-serif"), R.string.system_normal),
    SYSTEM_LIGHT(FontHelper.creatorByNameAndStyle("sans-serif-light"), R.string.system_light),
    SYSTEM_BOLD(FontHelper.creatorByNameAndStyle("sans-serif", Typeface.BOLD), R.string.system_bold);

    val typeFace: Typeface? by lazy {
        fontCreator.invoke()
    }


}