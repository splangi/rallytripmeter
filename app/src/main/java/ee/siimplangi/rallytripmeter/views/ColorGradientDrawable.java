package ee.siimplangi.rallytripmeter.views;

import android.graphics.drawable.GradientDrawable;

public class ColorGradientDrawable extends GradientDrawable {

    private Integer currentColor = null;

    @Override
    public void setColor(int argb) {
        super.setColor(argb);
        currentColor = argb;
    }


    @Override
    public void setColors(int[] colors) {
        super.setColors(colors);
        currentColor = null;
    }

    public Integer getCurrentColor() {
        return currentColor;
    }
}
