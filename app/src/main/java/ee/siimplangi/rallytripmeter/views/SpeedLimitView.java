package ee.siimplangi.rallytripmeter.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import ee.siimplangi.rallytripmeter.R;

/**
 * Created by Siim on 20.08.2017.
 */

public class SpeedLimitView extends FrameLayout {

    private DecimalFormat decimalFormat = new DecimalFormat("0");

    private TextView limitTextView;
    private ImageView bg;

    public SpeedLimitView(@NonNull Context context) {
        super(context);
        init();
    }

    public SpeedLimitView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SpeedLimitView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        inflate(getContext(), R.layout.view_speed_limit, this);
        limitTextView = findViewById(R.id.speedLimit);
        limitTextView.setSingleLine();
        bg = findViewById(R.id.bg);
    }

    public void setLimit(@Nullable Float limit) {
        if (limit == null) {
            limitTextView.setText(R.string.NotAvailable);
        } else {
            limitTextView.setText(decimalFormat.format(limit));
        }
    }

    @Override
    public void setActivated(boolean activated) {
        super.setActivated(activated);
        bg.setImageResource(activated ? R.drawable.bg_speed_limit_active : R.drawable.bg_speed_limit_inactive);
    }
}
