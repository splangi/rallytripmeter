package ee.siimplangi.rallytripmeter.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.widget.ContentLoadingProgressBar;
import ee.siimplangi.rallytripmeter.R;

/**
 * Created by Siim on 02.09.2017.
 */

public class LoaderView extends FrameLayout {

    private ContentLoadingProgressBar progressBar;

    public LoaderView(@NonNull Context context) {
        super(context);
        init(null);
    }

    public LoaderView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public LoaderView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attributeSet) {
        LayoutInflater.from(getContext()).inflate(R.layout.loader_layout, this);
        progressBar = findViewById(R.id.progressBar);
        TypedArray typedArray = getContext().obtainStyledAttributes(attributeSet, R.styleable.LoaderView);
        if (typedArray.hasValue(R.styleable.LoaderView_progressbar_tint)) {
            @ColorInt int color = typedArray.getColor(R.styleable.LoaderView_progressbar_tint, 0);
            tintProgressBar(color);
        }
        if (typedArray.hasValue(R.styleable.LoaderView_progressbar_size)) {
            int pixelSize = typedArray.getDimensionPixelSize(R.styleable.LoaderView_progressbar_size, -1);
            resizeProgressBar(pixelSize);
        }

    }

    public void tintProgressBar(@ColorInt int color) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable drawableProgress = DrawableCompat.wrap(progressBar.getIndeterminateDrawable());
            DrawableCompat.setTint(drawableProgress, color);
            progressBar.setIndeterminateDrawable(drawableProgress);
        } else {
            progressBar.getIndeterminateDrawable().setTint(color);
        }
    }

    public void resizeProgressBar(int pixelSize) {
        progressBar.getLayoutParams().height = pixelSize;
        progressBar.getLayoutParams().width = pixelSize;
        progressBar.invalidate();
    }

    public void hide() {
        progressBar.hide();
    }

    public void show() {
        progressBar.show();
    }
}
