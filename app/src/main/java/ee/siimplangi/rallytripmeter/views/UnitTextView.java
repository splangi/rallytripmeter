package ee.siimplangi.rallytripmeter.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.enums.Unit;
import ee.siimplangi.rallytripmeter.helpers.Settings;

/**
 * Created by Siim on 30.08.2017.
 */

public class UnitTextView extends AppCompatTextView {

    private int mode = 0;

    public UnitTextView(Context context) {
        super(context);
        init(null);
    }

    public UnitTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public UnitTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(@Nullable AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attributeSet, R.styleable.UnitTextView);
            mode = typedArray.getInt(R.styleable.UnitTextView_unit_mode, 0);
        }
        if (isInEditMode()) {
            refresh(Unit.METRIC);
        } else {
            refresh(Settings.INSTANCE.getUnit());
        }

    }

    private void refresh(Unit unit) {
        if (mode == 1) {
            setText(unit.getSpeedString(getContext()));
        } else {
            setText(unit.getDistanceString(getContext()));
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        EventBus.getDefault().unregister(this);
        super.onDetachedFromWindow();
    }

    @Subscribe
    public void onUnitChange(Unit unit) {
        refresh(unit);
    }
}
