package ee.siimplangi.rallytripmeter.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.NumberPicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.Nullable;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.helpers.Settings;

/**
 * Created by Siim on 07.08.2017.
 */

public class TimePicker extends FrameLayout {

    private NumberPicker hours;
    private NumberPicker minutes;
    private NumberPicker seconds;
    private View secondsWrapper;
    private boolean showSeconds = false;
    private int secondSequence = 60;


    public TimePicker(Context context) {
        this(context, true, 30, true);
    }

    public TimePicker(Context context, boolean showSeconds, int secondSequence, boolean initializeCurrentTime) {
        super(context);
        this.secondSequence = secondSequence;
        this.showSeconds = showSeconds;
        initializeViews(context);
        initializeNumberPickers(initializeCurrentTime);
    }


    public TimePicker(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initializeAttributes(context, attrs);
        initializeViews(context);
        initializeNumberPickers(true);
    }

    public TimePicker(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeAttributes(context, attrs);
        initializeViews(context);
        initializeNumberPickers(true);
    }

    private static String[] generateSequence(int beginning, int end, int sequence) {
        List<String> StringArray = new ArrayList<>();
        for (int i = beginning; i < end; i = i + sequence) {
            StringArray.add(String.format("%02d", i));
        }
        return StringArray.toArray(new String[StringArray.size()]);
    }

    private void initializeNumberPickers(boolean initializeCurrentTime) {
        Calendar calendar;
        calendar = Settings.INSTANCE.getRallyTime().getRallyTimeCalendar();
        calendar.add(Calendar.MINUTE, 1);
        hours.setMinValue(0);
        hours.setMaxValue(23);
        minutes.setMinValue(0);
        minutes.setMaxValue(59);
        minutes.setDisplayedValues(generateSequence(0, 60, 1));
        if (showSeconds()) {
            seconds.setMinValue(0);
            seconds.setMaxValue(59 / secondSequence);
            seconds.setValue(0);
        }
        if (initializeCurrentTime) {
            hours.setValue(calendar.get(Calendar.HOUR_OF_DAY));
            minutes.setValue(calendar.get(Calendar.MINUTE));
            if (showSeconds()) {
                seconds.setDisplayedValues(generateSequence(0, 60, secondSequence));
            }
        }
    }


    public int getHours() {
        return hours.getValue();
    }

    public int getMinutes() {
        return minutes.getValue();
    }

    public void setMinutes(int minutes) {
        this.minutes.setValue(minutes);
    }

    public int getSeconds() {
        return seconds.getValue() * secondSequence;
    }

    public void setSeconds(int seconds) {
        this.seconds.setValue(seconds / secondSequence);
    }

    public void setHour(int hour) {
        this.hours.setValue(hour);
    }

    private void initializeAttributes(Context context, AttributeSet as) {
        TypedArray ta = context.obtainStyledAttributes(as, R.styleable.TimePicker);
        showSeconds = ta.getBoolean(R.styleable.TimePicker_showSeconds, showSeconds);
        secondSequence = ta.getInt(R.styleable.TimePicker_secondSequence, secondSequence);
        ta.recycle();
    }

    private void initializeViews(Context context) {
        View v = LayoutInflater.from(context).inflate(R.layout.timepicker, this, true);
        hours = v.findViewById(R.id.hours);
        minutes = v.findViewById(R.id.minutes);
        seconds = v.findViewById(R.id.seconds);
        secondsWrapper = v.findViewById(R.id.secondWrapper);
        if (!showSeconds()) {
            secondsWrapper.setVisibility(GONE);
        }
    }

    private boolean showSeconds() {
        if (isInEditMode()) {
            return showSeconds;
        } else {
            //PremiumFeatures premiumFeatures = Settings.getPremiumFeatures();
            return showSeconds; //&& premiumFeatures.hasPaidFeature(PaidFeature.ADVANCED_TIMECONTROL);
        }
    }


}
