package ee.siimplangi.rallytripmeter.extensions

fun String.blankAsNull(): String? {
    return if (this.trim().isNullOrBlank()) null else this
}

fun String.prepend(other: String): String {
    return other + this
}