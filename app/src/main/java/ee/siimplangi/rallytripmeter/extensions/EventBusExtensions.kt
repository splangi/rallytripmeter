package ee.siimplangi.rallytripmeter.extensions

import org.greenrobot.eventbus.EventBus

fun EventBus.registerSafe(item: Any) {
    if (!this.isRegistered(item)) {
        this.register(item)
    }
}


fun EventBus.unregisterSafe(item: Any) {
    if (this.isRegistered(item)) {
        this.unregister(item)
    }
}