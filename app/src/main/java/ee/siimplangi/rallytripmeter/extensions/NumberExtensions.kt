package ee.siimplangi.rallytripmeter.extensions

fun Number.toMetersFrom(currentUnit: ee.siimplangi.rallytripmeter.enums.Unit): Double {
    return (ee.siimplangi.rallytripmeter.enums.Unit.METRIC.MULTIPLIER / currentUnit.MULTIPLIER) * this.toDouble() * 1000
}

fun Number.convertMetersTo(currentUnit: ee.siimplangi.rallytripmeter.enums.Unit): Double {
    return this.toDouble() / 1000 * (ee.siimplangi.rallytripmeter.enums.Unit.METRIC.MULTIPLIER * currentUnit.MULTIPLIER)
}

fun Number.convertMPSTo(currentUnit: ee.siimplangi.rallytripmeter.enums.Unit): Double {
    return this.toDouble() * 3.6 * (ee.siimplangi.rallytripmeter.enums.Unit.METRIC.MULTIPLIER * currentUnit.MULTIPLIER)
}

fun Number.toMPSfrom(currentUnit: ee.siimplangi.rallytripmeter.enums.Unit): Double {
    return (ee.siimplangi.rallytripmeter.enums.Unit.METRIC.MULTIPLIER / currentUnit.MULTIPLIER) * this.toDouble() / 3.6
}