package ee.siimplangi.rallytripmeter.extensions

import android.graphics.drawable.Drawable
import android.widget.EditText

fun EditText.validate(validator: (String) -> Boolean, message: String = "", drawable: Drawable? = null): Boolean {
    val valid = validator.invoke(this.text.toString().trim())
    if (valid) {
        this.error = null
    } else {
        if (drawable == null) {
            this.error = message
        } else {
            this.setError(message, drawable)
        }

    }
    return valid
}

fun EditText.extractNumber(): Double? {
    return this.text.trim().toString().toDoubleOrNull()
}