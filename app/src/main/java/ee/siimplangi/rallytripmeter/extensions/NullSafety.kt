package ee.siimplangi.rallytripmeter.extensions


fun <A, B> safe(a: A?, b: B?, function: (A, B) -> Unit): Unit? {
    return a?.let { a ->
        b?.let { b ->
            function.invoke(a, b)
        }
    }
}


fun <A, B, C> safe(a: A?, b: B?, c: C?, function: (A, B, C) -> Unit): Unit? {
    return a?.let { a ->
        b?.let { b ->
            c?.let { c ->
                function.invoke(a, b, c)
            }
        }
    }
}