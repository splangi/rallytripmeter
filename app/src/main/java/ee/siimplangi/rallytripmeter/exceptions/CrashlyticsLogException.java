package ee.siimplangi.rallytripmeter.exceptions;

/**
 * Created by Siim on 09.09.2017.
 */

public class CrashlyticsLogException extends Throwable {

    public CrashlyticsLogException(String tag, String msg) {
        super("TAG: " + tag + " MSG: " + msg);
    }

    public CrashlyticsLogException(String tag, String msg, Throwable cause) {
        super("TAG: " + tag + " MSG: " + msg, cause);
    }
}
