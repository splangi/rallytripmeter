package ee.siimplangi.rallytripmeter.listeners;

import android.content.DialogInterface;

/**
 * Created by Siim on 07.09.2017.
 */

public interface OnDialogAcceptedListener<T> {

    void onClick(DialogInterface dialogInterface, T data);

}
