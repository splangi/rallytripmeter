package ee.siimplangi.rallytripmeter.listeners

import ee.siimplangi.rallytripmeter.enums.ProductStatus
import ee.siimplangi.rallytripmeter.models.TrialModel

interface OnTrialStatus {

    fun onTrialStatus(productStatus: ProductStatus, trialModel: TrialModel?)

}
