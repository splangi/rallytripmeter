package ee.siimplangi.rallytripmeter.listeners;

import ee.siimplangi.rallytripmeter.enums.CRUDAction;
import ee.siimplangi.rallytripmeter.models.StageDef;

/**
 * Created by Siim on 04.09.2017.
 */

public interface OnStageSelectedListener {

    void onSelected(StageDef stageDef, String ref, CRUDAction action);

}
