package ee.siimplangi.rallytripmeter.constants

object StateViewConstants {

    public const val INTERNET_OFF: String = "internet_off"
    public const val EXCEPTION: String = "exception"
}

