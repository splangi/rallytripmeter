package ee.siimplangi.rallytripmeter.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Siim on 29.08.2017.
 */

public class FirebaseConstants {

    public static final Map<String, Object> configDefaults;

    public static final String CONNECTED_NODE = ".info/connected";

    public static final String TELEMETRY_NODE = "tracking";
    public static final String RACER_LOCATIONS = "geolocations";

    public static final String TRIAL_REGISTRATION_NODE = "trial";
    public static final String TRIAL_DEVICE_REGISTRATION_NODE = "trial_device";
    public static final String TRIAL_LATEST_READ_NODE = "latestReadTS";

    public static final String STAGES_NODE = "stagedef";
    public static final String USERS_NODE = "uid";
    public static final String FEEDBACK_NODE = "feedback";

    public static final String EVENT_NEEDS_GOOGLE_PLAY_UPDATES = "google_play_updates_required";
    public static final String EVENT_DID_NOT_LOG_IN = "did_not_log_in";
    public static final String EVENT_DID_NOT_ACCEPT_TERMS = "did_not_accept_terms";
    public static final String EVENT_LOGGED_IN = "logged_in";
    public static final String EVENT_LOGGED_IN_ANONYMOUSLY = "logged_in_anonymous";
    public static final String EVENT_VALID_BILLING = "valid_billing";
    public static final String EVENT_TRIAL_OVER = "trial_over";
    public static final String EVENT_TRIAL_ALMOST_OVER = "trial_almost_over";
    public static final String EVENT_STARTED_TRIAL = "started_trial";
    public static final String EVENT_INVITE = "invite_successful";


    public static final String CONFIG_MAX_RECORDING_FREQUENCY = "max_recording_frequency";
    public static final String CONFIG_STANDING_RECORDING_FREQUENCY = "standing_recording_frequency";
    public static final String CONFIG_WEATHER = "weather_forecast";
    public static final String CONFIG_WEATHER_DETAILS = "weather_forecast_detail";
    public static final String CONFIG_WEATHER_MAP = "weather_maps";
    public static final String CONFIG_WEATHER_MAPS_NO_PARAMS = "weather_maps_no_params";
    public static final String CONFIG_TRIAL_LENGTH = "trial_length_days";
    public static final String CONFIG_ACCESS_INAPP_SKUS = "inapp_access_sku_list";
    public static final String CONFIG_ACCESS_SUBS_SKUS = "subs_access_sku_list";
    public static final String CONFIG_ACTIVE_LIFETIME_DISCOUNT = "product_lifetime_discount";
    public static final String CONFIG_ACTIVE_YEARLY = "product_subscription_yearly";
    public static final String CONFIG_ACTIVE_LIFETIME = "product_lifetime";
    public static final String CONFIG_ACTIVE_QUARTERLY = "product_subscription_quarterly";

    static {
        configDefaults = new HashMap<>();
        configDefaults.put(CONFIG_WEATHER_MAPS_NO_PARAMS, "https://maps.darksky.net/@precipitation_rate?defaultUnits=_c");
        configDefaults.put(CONFIG_WEATHER, "https://darksky.net/forecast");
        configDefaults.put(CONFIG_WEATHER_MAP, "https://maps.darksky.net/@precipitation_rate,$date,$lat,$lng,$zoom?defaultUnits=_c");
        configDefaults.put(CONFIG_WEATHER_DETAILS, "https://darksky.net/details/$lat,$lng/$date/si24/en");
        configDefaults.put(CONFIG_STANDING_RECORDING_FREQUENCY, 10000);
        configDefaults.put(CONFIG_MAX_RECORDING_FREQUENCY, 2500);
        configDefaults.put(CONFIG_TRIAL_LENGTH, 90);
        configDefaults.put(CONFIG_ACCESS_INAPP_SKUS, "product_lifetime_access,android.test.purchased");
        configDefaults.put(CONFIG_ACCESS_SUBS_SKUS, "premium_yearly,premium_yearly_free_trial,pro_v1,product_subscription_yearly,product_subscription_quarterly,android.test.purchased");
        configDefaults.put(CONFIG_ACTIVE_YEARLY, "product_subscription_yearly");
        configDefaults.put(CONFIG_ACTIVE_LIFETIME, "product_lifetime_access");
        configDefaults.put(CONFIG_ACTIVE_QUARTERLY, "product_subscription_quarterly");
        configDefaults.put(CONFIG_ACTIVE_LIFETIME_DISCOUNT, "product_lifetime_access_discount");
    }

}
