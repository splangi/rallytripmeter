package ee.siimplangi.rallytripmeter.constants

import com.google.firebase.remoteconfig.FirebaseRemoteConfig

/**
 * Created by Siim on 06.09.2017.
 */

object Sku {

    /*PRO_V1("pro_v1", BillingClient.SkuType.SUBS),
    PREMIUM_YEARLY("premium_yearly", BillingClient.SkuType.SUBS),
    PREMIUM_YEARLY_TRIAL("premium_yearly_free_trial", BillingClient.SkuType.SUBS),
    PRODUCT_SUBSCRIPTION("product_subscription_yearly", BillingClient.SkuType.SUBS),
    PRODUCT_LIFETIME("product_lifetime_access", BillingClient.SkuType.INAPP);

    var id: String = id*/

    fun getAllSubSkuIDs(): List<String> {
        return FirebaseRemoteConfig.getInstance().getString(FirebaseConstants.CONFIG_ACCESS_SUBS_SKUS).split(",")
    }

    fun getAllInAppSkuIDs(): List<String> {
        return FirebaseRemoteConfig.getInstance().getString(FirebaseConstants.CONFIG_ACCESS_INAPP_SKUS).split(",")
    }


}
