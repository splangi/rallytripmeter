package ee.siimplangi.rallytripmeter;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.database.DatabaseError;

import ee.siimplangi.rallytripmeter.exceptions.CrashlyticsLogException;

/**
 * Created by Siim on 24.07.2017.
 */

public class AppLog {

    public static void e(Object announcer, DatabaseError databaseError) {
        if (databaseError.getCode() != DatabaseError.NETWORK_ERROR) {
            e(announcer, databaseError.getMessage() + " DETAILS: " + databaseError.getDetails(), databaseError.toException());
        }
    }

    public static void e(Object announcer, String msg) {
        e(announcer.getClass().getSimpleName(), msg);
    }

    public static void e(Object announcer, String msg, Throwable e) {
        e(announcer.getClass().getSimpleName(), msg, e);
    }

    public static void e(String tag, String msg) {
        Log.e(tag, msg);
        if (RallyTripmeter.Companion.getCrashLyticsEnabled()) {
            Crashlytics.logException(new CrashlyticsLogException(tag, msg));
        }

    }

    public static void e(String tag, String msg, Throwable throwable) {
        Log.e(tag, msg, throwable);
        if (RallyTripmeter.Companion.getCrashLyticsEnabled()) {
            Crashlytics.logException(new CrashlyticsLogException(tag, msg, throwable));
        }

    }

    public static void i(String tag, String msg) {
        Log.i(tag, msg);
    }

    public static void d(String tag, String msg) {
        Log.d(tag, msg);
    }

    public static void d(Object object, String msg) {
        Log.d(object.getClass().getSimpleName(), msg);
    }

    public static void w(String tag, String msg) {
        Log.w(tag, msg);
    }


}
