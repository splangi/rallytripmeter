package ee.siimplangi.rallytripmeter.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.models.Section
import ee.siimplangi.rallytripmeter.viewholders.SectionViewHolder

/**
 * Created by Siim on 02.09.2017.
 */

class LimitAdapter(private val sections: MutableList<Section>) : androidx.recyclerview.widget.RecyclerView.Adapter<SectionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SectionViewHolder {
        return SectionViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.speed_limit_item2, parent, false), ::onRemoveSection)
    }

    override fun onBindViewHolder(holder: SectionViewHolder, position: Int) {
        holder.bind(sections[position])
    }

    override fun getItemCount(): Int {
        return sections.size
    }

    override fun onViewRecycled(holder: SectionViewHolder) {
        holder.saveData()
        if (holder.itemView.hasFocus()) {
            holder.itemView.clearFocus()
        }
        super.onViewRecycled(holder)

    }

    fun onRemoveSection(section: Section) {
        val index = sections.indexOf(section)
        val success = sections.remove(section)
        if (success) {
            notifyItemRemoved(index)
        }

    }


}
