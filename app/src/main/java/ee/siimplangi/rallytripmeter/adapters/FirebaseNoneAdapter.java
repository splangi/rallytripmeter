package ee.siimplangi.rallytripmeter.adapters;

import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

import androidx.annotation.NonNull;

public abstract class FirebaseNoneAdapter<T> extends FirebaseListAdapter<T> {

    private static final int VIEW_TYPE_NONE = 0;
    private static final int VIEW_TYPE_REGULAR = 1;

    public FirebaseNoneAdapter(@NonNull FirebaseListOptions<T> options) {
        super(options);
    }

    @NonNull
    @Override
    public T getItem(int position) {
        if (position == 0) {
            return null;
        } else {
            return super.getItem(position - 1);
        }

    }

    @Override
    public void onChildChanged(@NonNull ChangeEventType type,
                               @NonNull DataSnapshot snapshot,
                               int newIndex,
                               int oldIndex) {
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return super.getCount() + 1;
    }

    @NonNull
    @Override
    public DatabaseReference getRef(int position) {
        return super.getRef(position - 1);
    }

    @Override
    public long getItemId(int i) {
        if (i == 0) {
            return 0L;
        } else {
            return super.getItemId(i - 1);
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return VIEW_TYPE_NONE;
        }
        return VIEW_TYPE_REGULAR;
    }

    protected abstract View getNoneView(ViewGroup viewGroup);

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (position == 0) {
            view = getNoneView(viewGroup);
            return view;
        }
        return super.getView(position, view, viewGroup);
    }
}
