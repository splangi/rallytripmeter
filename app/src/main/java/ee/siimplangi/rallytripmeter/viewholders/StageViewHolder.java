package ee.siimplangi.rallytripmeter.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.recyclerview.widget.RecyclerView;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.enums.CRUDAction;
import ee.siimplangi.rallytripmeter.enums.Unit;
import ee.siimplangi.rallytripmeter.helpers.Settings;
import ee.siimplangi.rallytripmeter.helpers.converters.UnitConverter;
import ee.siimplangi.rallytripmeter.listeners.OnStageSelectedListener;
import ee.siimplangi.rallytripmeter.models.StageDef;

/**
 * Created by Siim on 29.08.2017.
 */

public class StageViewHolder extends RecyclerView.ViewHolder {

    private static DecimalFormat df = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US));

    private TextView stageDesc;
    private TextView timestamp;
    private Context context;
    private String ref;
    private StageDef currentStageDef;
    private OnStageSelectedListener listener;


    public StageViewHolder(View itemView, OnStageSelectedListener listener, final CRUDAction defaultAction) {
        super(itemView);
        this.listener = listener;
        itemView.findViewById(R.id.wrapper).setOnClickListener(new OnStageClickListener(defaultAction));
        itemView.findViewById(R.id.edit).setOnClickListener(new OnStageClickListener(CRUDAction.UPDATE));
        itemView.findViewById(R.id.delete).setOnClickListener(new OnStageClickListener(CRUDAction.DELETE));
        context = itemView.getContext();
        stageDesc = itemView.findViewById(R.id.stageDesc);
        timestamp = itemView.findViewById(R.id.timestamp);
    }

    public void bind(StageDef stageDef, String ref) {
        UnitConverter converter = new UnitConverter(Unit.METRIC, Settings.INSTANCE.getUnit());
        //Float length = converter.convert(UnitConverter.METERS_TO_KILOMETERS.convert(stageDef.getStageLengthMeters()));
        String stageLength = "";
        /*if (length != null) {
            stageLength = df.format(length) + settings.getUnit().getDistanceString(context);
        }*/
        this.currentStageDef = stageDef;
        this.ref = ref;
        stageDesc.setText(context.getResources().getString(R.string.stage_desc,
                stageDef.getRallyName(),
                stageDef.getStageName()));
        timestamp.setText(SimpleDateFormat.getDateTimeInstance().format(new Date(stageDef.getFirebaseTimeStamp().getTimeStampLong())));
    }

    private class OnStageClickListener implements View.OnClickListener {

        private CRUDAction crudAction;

        public OnStageClickListener(CRUDAction crudAction) {
            this.crudAction = crudAction;
        }

        @Override
        public void onClick(View view) {
            if (listener != null) {
                listener.onSelected(currentStageDef, ref, crudAction);
            }
        }
    }
}
