package ee.siimplangi.rallytripmeter.viewholders

import android.view.View
import android.widget.EditText
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.enums.Unit
import ee.siimplangi.rallytripmeter.extensions.*
import ee.siimplangi.rallytripmeter.helpers.Settings
import ee.siimplangi.rallytripmeter.helpers.converters.UnitConverter
import ee.siimplangi.rallytripmeter.models.Section
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

/**
 * Created by Siim on 02.09.2017.
 */

class SectionViewHolder(itemView: View, val removeAction: (Section) -> kotlin.Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView), View.OnClickListener {

    private val toDistance: EditText
    //private EditText fromDistance;
    private val limit: EditText
    private val deleteButton: View

    private lateinit var current: Section

    init {
        //fromDistance = itemView.findViewById(R.id.fromDistance);
        toDistance = itemView.findViewById(R.id.toDistance)
        limit = itemView.findViewById(R.id.speedLimit)
        deleteButton = itemView.findViewById(R.id.deleteButton)
        deleteButton.setOnClickListener(this)
    }

    fun bind(section: Section) {
        val unit = Settings.unit
        val distanceFormat = DecimalFormat("0.00", DecimalFormatSymbols(Locale.US))
        val speedFormat = DecimalFormat("0")
        current = section
        //fromDistance.setText(Tools.formatFloat(section.getFrom(), distanceFormat, UnitConverter.METERS_TO_KILOMETERS, unitConverter));
        toDistance.setText(section.to?.convertMetersTo(unit)?.let { distanceFormat.format(it) }
                ?: "")
        limit.setText(section.speed?.convertMPSTo(unit)?.let { speedFormat.format(it) } ?: "")
    }


    fun saveData() {
        val unit = Settings.unit
        //current.setFrom(Tools.extractData(fromDistance, unitConverter, UnitConverter.KILOMETERS_TO_METERS));
        current.to = toDistance.extractNumber()?.toMetersFrom(unit)
        current.speed = limit.extractNumber()?.toMPSfrom(unit)
    }


    fun validateAndSetError(previousToDistance: Double): Boolean {
        var valid = toDistance.validate({
            UnitConverter(Settings.unit, Unit.METRIC)
            it.toDoubleOrNull() != null && (it.toDouble().toMetersFrom(Settings.unit) > previousToDistance)
        })
        valid = valid && limit.validate({
            it.isBlank() || (it.toDoubleOrNull() != null && it.toDouble() > 0.0)
        })
        return valid
    }

    override fun onClick(view: View) {
        removeAction.invoke(current)
    }
}
