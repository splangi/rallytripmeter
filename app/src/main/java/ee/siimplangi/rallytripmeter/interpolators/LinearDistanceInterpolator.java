package ee.siimplangi.rallytripmeter.interpolators;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import ee.siimplangi.rallytripmeter.helpers.TimeHelper;

/**
 * Created by Siim on 05.08.2017.
 */

public class LinearDistanceInterpolator implements DistanceInterpolator {

    public static final Parcelable.Creator<LinearDistanceInterpolator> CREATOR = new Parcelable.Creator<LinearDistanceInterpolator>() {
        @Override
        public LinearDistanceInterpolator createFromParcel(Parcel source) {
            return new LinearDistanceInterpolator(source);
        }

        @Override
        public LinearDistanceInterpolator[] newArray(int size) {
            return new LinearDistanceInterpolator[size];
        }
    };
    private static final float MIN_ACCURACCY = 50;
    private static final float ACCURACCY_MOVEMENT_RATIO = 3;
    private static final float MIN_SPEED_MS = 1f;
    private static final long LOCATION_INVALIDATION = 10000;
    private float realDistance;
    private Location lastLocation;

    public LinearDistanceInterpolator() {
        realDistance = 0F;
        lastLocation = null;
    }

    protected LinearDistanceInterpolator(Parcel in) {
        this.realDistance = in.readFloat();
        this.lastLocation = in.readParcelable(Location.class.getClassLoader());
    }

    @Override
    public void onNewRealLocation(Location location) {
        //Log.d(Location.class.getSimpleName(), "LocationEvent. Accuraccy: " + location.getAccuracy() + " Speed: " + location.getSpeed());
        float distanceTo = lastLocation != null ? lastLocation.distanceTo(location) : 0;
        if (lastLocation != null && location.getSpeed() > MIN_SPEED_MS && location.getAccuracy() / ACCURACCY_MOVEMENT_RATIO < distanceTo && location.getAccuracy() < MIN_ACCURACCY) {
            //Log.d(Location.class.getSimpleName(), "Increasing!. Accuraccy: " + location.getAccuracy() + " Speed: " + location.getSpeed() + " distanceTo: " + distanceTo + "realDistance " + realDistance);
            realDistance = realDistance + distanceTo;
            lastLocation = location;
        } else if (lastLocation == null) {
            lastLocation = location;
        } else {
            lastLocation.setSpeed(location.getSpeed());
        }
    }

    @Override
    public float predict() {
        if (lastLocation == null || TimeHelper.getElapsedTime() - TimeHelper.getLocationElapsedTime(lastLocation) > LOCATION_INVALIDATION)
            return realDistance;
        float deltaTimeSeconds = (TimeHelper.getElapsedTime() - TimeHelper.getLocationElapsedTime(lastLocation)) / 1000F;
        float speedMS = lastLocation.getSpeed();
        return realDistance + (speedMS * deltaTimeSeconds);
    }

    @Override
    public long getTimeToNextDistance(float distance) {
        float distanceTo = distance - predict();
        if (distanceTo < 0) return 0;
        if (lastLocation == null) return Long.MAX_VALUE;
        float speedMS = lastLocation.getSpeed();
        if (speedMS <= 0L) return Long.MAX_VALUE;
        return (long) ((distanceTo / speedMS) * 1000L);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.realDistance);
        dest.writeParcelable(this.lastLocation, flags);
    }
}
