package ee.siimplangi.rallytripmeter.interpolators;

import android.location.Location;
import android.os.Parcelable;

/**
 * Created by Siim on 01.08.2017.
 */

public interface DistanceInterpolator extends Parcelable {

    void onNewRealLocation(Location location);

    float predict();

    long getTimeToNextDistance(float distance);


}
