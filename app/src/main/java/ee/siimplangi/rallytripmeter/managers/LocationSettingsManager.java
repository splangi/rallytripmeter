package ee.siimplangi.rallytripmeter.managers;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.provider.Settings;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.tasks.OnFailureListener;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import ee.siimplangi.rallytripmeter.AppLog;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.services.LocationService;

/**
 * Created by Siim on 24.07.2017.
 */

public class LocationSettingsManager {

    private static final LocationSettingsRequest locationSettingsRequest = new LocationSettingsRequest.Builder().addLocationRequest(LocationService.LOCATION_REQUEST).setAlwaysShow(true).build();
    private final static int ENABLE_LOCATION_REQUEST_CODE = 5;
    private final GoogleApiManager googleApiManager;
    private final Activity activity;

    public LocationSettingsManager(Activity activity) {
        this.activity = activity;
        googleApiManager = new GoogleApiManager(activity, LocationServices.API);
    }

    public void onStart() {
        googleApiManager.onStart();
    }

    public void onStop() {
        googleApiManager.onStop();
    }

    public void requestEnableLocation() {
        googleApiManager.googleApiClientRequest(new GoogleApiManager.GoogleApiClientRequest() {
            @Override
            public void onGoogleApiReady(GoogleApiClient googleApiClient) {

                LocationServices.getSettingsClient(activity).checkLocationSettings(locationSettingsRequest).addOnFailureListener(activity, new OnFailureListener() {

                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ResolvableApiException) {
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                ResolvableApiException resolvable = (ResolvableApiException) e;
                                resolvable.startResolutionForResult(activity,
                                        ENABLE_LOCATION_REQUEST_CODE);
                            } catch (IntentSender.SendIntentException sendEx) {
                                AppLog.e(LocationSettingsManager.class.getSimpleName(), "Cannot start resolution for result", e);

                            }
                        }
                    }
                });
            }

            @Override
            public void onGoogleApiNotAvailable() {
                new AlertDialog.Builder(activity).setTitle(R.string.action_required).setMessage(R.string.dialog_enable_location_services_msg).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        activity.startActivity(intent);
                    }
                }).setNegativeButton(R.string.dismiss, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();

            }
        });
    }


}
