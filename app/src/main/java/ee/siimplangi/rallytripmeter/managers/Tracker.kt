package ee.siimplangi.rallytripmeter.managers

import android.location.Location
import android.util.Log
import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants
import ee.siimplangi.rallytripmeter.models.TrackingData
import ee.siimplangi.rallytripmeter.storage.Storage
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class Tracker {

    private val db = FirebaseDatabase.getInstance().getReference(FirebaseConstants.TELEMETRY_NODE).child(Storage.installationID)
    private val geoDb = FirebaseDatabase.getInstance().getReference(FirebaseConstants.RACER_LOCATIONS).child(Storage.installationID)
    private val connectedRef = FirebaseDatabase.getInstance().getReference(FirebaseConstants.CONNECTED_NODE)
    private val geoFire = GeoFire(FirebaseDatabase.getInstance().getReference(FirebaseConstants.RACER_LOCATIONS))
    private val auth = FirebaseAuth.getInstance()

    private var lastTrackingData: TrackingData? = null
    private var lastGeoLocation: GeoLocation? = null

    private var lastTrackingUploadTask: Task<Void>? = null
    private var lastGeoUploadTask: Task<Void>? = null

    lateinit var eventListener: ValueEventListener

    init {

    }

    @Subscribe
    fun onLocation(location: Location) {
        val newUserLocation = TrackingData(auth.currentUser?.uid, location)
        if (lastTrackingData?.hasMajorChanged(newUserLocation) != false) {
            if (lastTrackingUploadTask?.isComplete == true) {
                lastTrackingUploadTask = db.setValue(newUserLocation)
                lastTrackingData = newUserLocation
            }
            if (lastGeoUploadTask?.isComplete == true) {
                val newGeoLocation = GeoLocation(location.latitude, location.longitude)
                geoFire.setLocation(Storage.installationID, newGeoLocation, { key, error ->

                })
                lastGeoLocation = newGeoLocation
            }

        }

    }


    fun onStart() {
        EventBus.getDefault().register(this)
        eventListener = connectedRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                Log.i(Tracker::class.java.simpleName, "onConnectedChange: " + p0.getValue(Boolean::class.java))
                if (p0.getValue(Boolean::class.java) == true) {
                    lastTrackingUploadTask = db.onDisconnect().updateChildren(HashMap<String, Any>().also {
                        it["disconnected"] = true
                        //it["disconnected_timestamp"] = FirebaseTimeStamp()
                    })
                    lastGeoUploadTask = geoDb.onDisconnect().removeValue()
                } else {
                    lastTrackingUploadTask = null
                    lastGeoUploadTask = null
                }
            }

        })
    }

    fun onStop() {
        EventBus.getDefault().unregister(this)
        connectedRef.removeEventListener(eventListener)
        geoFire.removeLocation(Storage.installationID, { key, error ->

        })
        db.removeValue()
    }

}