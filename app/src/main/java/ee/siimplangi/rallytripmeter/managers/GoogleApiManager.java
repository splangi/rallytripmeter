package ee.siimplangi.rallytripmeter.managers;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayDeque;
import java.util.Queue;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import ee.siimplangi.rallytripmeter.AppLog;
import ee.siimplangi.rallytripmeter.R;

/**
 * Created by Siim on 24.07.2017.
 */

public class GoogleApiManager implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, PendingIntent.OnFinished {

    private final static int REQUEST_CODE_RESOLUTION = 6;
    private final GoogleApiClient googleApiClient;
    private Context context;
    private Queue<GoogleApiClientRequest> requests = new ArrayDeque<>();

    public GoogleApiManager(Context context, Api<Api.ApiOptions.NoOptions> api) {
        this.context = context;
        this.googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(api)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public void onStart() {
        googleApiClient.connect();
    }

    public void onStop() {
        googleApiClient.disconnect();
    }

    @Override
    public void onConnectionFailed(@NonNull final ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            new AlertDialog.Builder(context)
                    .setTitle(R.string.action_required)
                    .setMessage(R.string.google_play_services_resolution_dialog_text)
                    .setPositiveButton(R.string.fix, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                connectionResult.getResolution().send(REQUEST_CODE_RESOLUTION, GoogleApiManager.this, null);
                            } catch (PendingIntent.CanceledException e) {
                                AppLog.e(GoogleApiManager.class.getSimpleName(), "Failed to fix the problem", e);
                            }
                        }
                    })
                    .setNegativeButton(R.string.later, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            declineRequests();
                        }
                    });
        } else {
            AppLog.e(GoogleApiManager.class.getSimpleName(), "Cannot connect to Google Play Services. ErrorMessage:" + connectionResult.getErrorMessage());
            declineRequests();
        }
    }

    public void googleApiClientRequest(GoogleApiClientRequest request) {
        if (googleApiClient.isConnected()) {
            request.onGoogleApiReady(googleApiClient);
        } else {
            requests.add(request);
        }
    }

    private void acceptRequests() {
        while (!requests.isEmpty()) {
            requests.remove().onGoogleApiReady(googleApiClient);
        }
    }

    private void declineRequests() {
        while (!requests.isEmpty()) {
            requests.remove().onGoogleApiNotAvailable();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        AppLog.i(GoogleApiManager.class.getSimpleName(), "GoogleApiClient connected");
        acceptRequests();
    }

    @Override
    public void onConnectionSuspended(int i) {
        AppLog.i(GoogleApiManager.class.getSimpleName(), "GoogleApiClient suspended");
    }

    @Override
    public void onSendFinished(PendingIntent pendingIntent, Intent intent, int resultCode, String resultData, Bundle resultExtras) {
        if (resultCode == Activity.RESULT_OK) {
            acceptRequests();
        } else if (resultCode == Activity.RESULT_CANCELED) {
            declineRequests();
        }
    }

    public interface GoogleApiClientRequest {

        void onGoogleApiReady(GoogleApiClient googleApiClient);

        void onGoogleApiNotAvailable();

    }

    public class GoogleApiFailureException extends Exception {


        public GoogleApiFailureException() {
        }

        public GoogleApiFailureException(String message) {
            super(message);
        }

        public GoogleApiFailureException(String message, Throwable cause) {
            super(message, cause);
        }

        public GoogleApiFailureException(Throwable cause) {
            super(cause);
        }
    }
}
