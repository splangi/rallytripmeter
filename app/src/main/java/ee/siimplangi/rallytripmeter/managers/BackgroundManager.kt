package ee.siimplangi.rallytripmeter.managers

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import ee.siimplangi.rallytripmeter.controllers.ActiveStageController
import ee.siimplangi.rallytripmeter.controllers.SpecialStageController
import ee.siimplangi.rallytripmeter.controllers.TCController
import ee.siimplangi.rallytripmeter.controllers.TripController
import ee.siimplangi.rallytripmeter.events.TrackingStateEvent
import ee.siimplangi.rallytripmeter.helpers.Settings
import ee.siimplangi.rallytripmeter.interpolators.LinearDistanceInterpolator
import ee.siimplangi.rallytripmeter.models.TripModel
import ee.siimplangi.rallytripmeter.services.LocationService
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Created by Siim on 04.08.2017.
 */

class BackgroundManager(private val context: Context, instanceState: Bundle? = null) : ServiceConnection {

    private var trip1Controller: TripController<TripModel.Trip1>
    private var trip2Controller: TripController<TripModel.Trip2>
    private var stageController: SpecialStageController
    private var tcController: TCController
    private var activeStageController: ActiveStageController
    private val recorder: Tracker

    var isTracking: Boolean = false;
    var isStarted: Boolean = false
        private set


    init {
        if (instanceState == null) {
            trip1Controller = TripController(TripModel.Trip1(), LinearDistanceInterpolator(), TripController.UpdateSchedulerType.SPEED)
            trip2Controller = TripController(TripModel.Trip2(), LinearDistanceInterpolator(), TripController.UpdateSchedulerType.SPEED)
            //avgSpeedController = new TripController<>(new TripModel.Average(), new LinearDistanceInterpolator(), TripController.UpdateSchedulerType.SIMPLE);
            //stageTripController = StageTripController(StageTrip(), LinearDistanceInterpolator(), TripController.UpdateSchedulerType.SPEED)
            tcController = TCController()

        } else {
            trip1Controller = TripController(instanceState.getBundle(KEY_TRIP1))
            trip2Controller = TripController(instanceState.getBundle(KEY_TRIP2))
            //avgSpeedController = new TripController(instanceState.getBundle(KEY_AVG));
            //stageTripController = SpecialStageController(instanceState.getBundle(KEY_STAGE)!!)
            tcController = TCController(instanceState.getBundle(KEY_TC))
        }
        stageController = SpecialStageController(instanceState?.getBundle(KEY_STAGE))
        activeStageController = ActiveStageController(instanceState?.getBundle(ACTIVE_STAGE))
        recorder = Tracker()
    }

    fun onStart() {

        if (!isStarted) {
            EventBus.getDefault().register(this)
            bindLocationService()
            trip1Controller.onStart()
            trip2Controller.onStart()
            //stageTripController!!.onStart()
            //avgSpeedController.onStart();
            stageController.onStart()
            tcController.onStart()
            activeStageController.onStart()
            isStarted = true
            startTracking()
        }
    }

    fun onStop() {
        if (isStarted) {
            EventBus.getDefault().unregister(this)
            isStarted = false
            unBindLocationService()
            trip1Controller.onStop()
            trip2Controller.onStop()
            stageController.onStop()
            //stageTripController!!.onStop()
            activeStageController.onStop()
            //avgSpeedController.onStop();
            tcController.onStop()
        }
        stopTracking()
    }

    @Subscribe
    fun on(trackingState: TrackingStateEvent) {
        if (trackingState.on) {
            startTracking()
        } else {
            stopTracking()
        }
    }

    fun startTracking() {
        if (isStarted && !isTracking && Settings.isTrackingOn) {
            recorder.onStart()
            isTracking = true
        }
    }

    fun stopTracking() {
        if (isTracking) {
            recorder.onStop()
            isTracking = false
        }
    }

    private fun bindLocationService() {
        try {
            context.bindService(LocationService.getIntent(context), this, Context.BIND_AUTO_CREATE or Context.BIND_IMPORTANT)
        } catch (e: SecurityException) {
            throw IllegalStateException("Cannot start service without Location permission")
        }

    }

    private fun unBindLocationService() {
        context.unbindService(this)
    }

    fun onSaveInstanceState(instanceState: Bundle) {
        instanceState.putBundle(KEY_TRIP1, trip1Controller.onSaveInstanceState())
        instanceState.putBundle(KEY_TRIP2, trip2Controller.onSaveInstanceState())
        instanceState.putBundle(KEY_TC, tcController.onSaveInstanceState())
        instanceState.putBundle(KEY_STAGE, stageController.onSaveInstanceState())
        instanceState.putBundle(ACTIVE_STAGE, activeStageController.onSaveInstanceState())
    }


    override fun onServiceConnected(name: ComponentName, service: IBinder) {

    }

    override fun onServiceDisconnected(name: ComponentName) {

    }

    companion object {

        private val KEY_TRIP1 = "trip1"
        private val KEY_TRIP2 = "trip2"
        private val KEY_AVG = "avg"
        private val KEY_STAGE = "stageDef"
        private val KEY_TC = "tc"
        private val ACTIVE_STAGE = "active"
    }

}
