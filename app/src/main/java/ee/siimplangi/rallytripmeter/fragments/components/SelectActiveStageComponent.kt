package ee.siimplangi.rallytripmeter.fragments.components

import android.os.Bundle
import android.view.View
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.enums.ComponentType
import ee.siimplangi.rallytripmeter.events.ActiveStage
import org.greenrobot.eventbus.Subscribe

class SelectActiveStageComponent : TextViewComponentFragment() {


    override val componentType: ComponentType
        get() = ComponentType.SELECT_STAGE


    override fun getMeasurableText(): CharSequence {
        return getString(R.string.set)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.setOnClickListener {
            showSelectStageDialog(mainTextView)
        }
    }

    @Subscribe(sticky = true)
    fun onNewActiveStage(onNewActiveStageDef: ActiveStage) {
        mainTextView.text = onNewActiveStageDef.stageDef?.let {
            getString(R.string.stage_desc, it.rallyName, it.stageName)
        }
                ?: getString(R.string.NotAvailable)
    }

}