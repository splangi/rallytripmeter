package ee.siimplangi.rallytripmeter.fragments.components

import ee.siimplangi.rallytripmeter.enums.ComponentType
import ee.siimplangi.rallytripmeter.enums.Unit
import ee.siimplangi.rallytripmeter.extensions.safe
import ee.siimplangi.rallytripmeter.helpers.ComponentSettings
import ee.siimplangi.rallytripmeter.helpers.converters.UnitConverter
import ee.siimplangi.rallytripmeter.models.*
import org.greenrobot.eventbus.Subscribe
import java.text.DecimalFormat

/**
 * Created by Siim on 12.09.2017.
 */

abstract class DistanceComponentFragment : UnitComponentFragment() {

    override fun formatValue(value: Float, unit: Unit): String {
        var value = value
        val unitConverter = UnitConverter(Unit.METRIC, unit)
        value = unitConverter.convert(value)
        value = UnitConverter.METERS_TO_KILOMETERS.convert(value)
        return decimalFormat.format(value)
    }

    override fun getUnitText(unit: Unit): String {
        return unit.getDistanceString(activity!!)
    }

    override fun getMeasurableTextString(): CharSequence {
        return "0.00"
    }

    companion object {

        private val decimalFormat = DecimalFormat("0.00")
    }
}

class Trip1 : DistanceComponentFragment() {


    override val componentType: ComponentType = ComponentType.TRIP1

    @Subscribe(sticky = true)
    fun onEvent(trip: TripModel.Trip1) {
        setValue(trip.trip)
    }
}

class Trip2 : DistanceComponentFragment() {

    override val componentType: ComponentType = ComponentType.TRIP2

    @Subscribe(sticky = true)
    fun onEvent(trip: TripModel.Trip2) {
        setValue(trip.trip)
    }
}

class StageTripComponent : DistanceComponentFragment() {

    var currentTrip: StageTrip? = null
    var currentActiveStage: SpecialStage? = null
    var isEnabled: Boolean = false
    var bgColorNormal: Int = 0
    var textColorNormal: Int = 0
    var bgColorOver: Int = 0
    var textColorOver: Int = 0

    override val componentType: ComponentType = ComponentType.STAGE_TRIP

    override fun onApplyCustomization(settings: ComponentSettings) {
        super.onApplyCustomization(settings)
        isEnabled = settings.enableThreshold
        bgColorNormal = settings.componentBG
        bgColorOver = settings.bgColorOver
        textColorOver = settings.txtColorOver
        textColorNormal = settings.componentTextColor
    }

    @Subscribe(sticky = true)
    fun onEvent(trip: SpecialStageTrip) {
        setValue(trip.trip)
        currentTrip = trip
        applyCustomization(currentTrip, currentActiveStage)
    }


    @Subscribe(sticky = true)
    fun onEvent(activeStage: SpecialStage) {
        currentActiveStage = activeStage
        applyCustomization(currentTrip, currentActiveStage)
    }

    private fun setNormal() {
        setTextColor(textColorNormal)
        setBGColor(bgColorNormal)
    }

    private fun setOver() {
        setTextColor(textColorOver)
        setBGColor(bgColorOver)
    }

    private fun applyCustomization(trip: TripModel?, activeStage: SpecialStage?) {
        safe(trip, activeStage?.stageDef, activeStage?.currentSection) { trip, stagedef, choice ->
            val section = stagedef.getCurrentSectionByChoice(choice)
            if (isEnabled == true && section != null) {
                if (section.to!! < trip.trip) {
                    setOver()
                } else {
                    setNormal()
                }
            } else {
                setNormal()
            }
        } ?: setNormal()

    }
}

class SectionTripComponent : DistanceComponentFragment() {


    override val componentType: ComponentType = ComponentType.SECTION_TRIP


    @Subscribe(sticky = true)
    fun onEvent(trip: SectionTrip) {
        setValue(trip.trip)

    }


}

