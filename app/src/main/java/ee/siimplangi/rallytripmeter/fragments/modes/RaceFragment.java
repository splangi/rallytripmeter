package ee.siimplangi.rallytripmeter.fragments.modes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ee.siimplangi.rallytripmeter.R;

public class RaceFragment extends ModeFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_mode_race, container, false);
		view.setKeepScreenOn(true);
		return view;
	}

	@Override
	protected String getTitle() {
		return getString(R.string.racemode);
	}
}