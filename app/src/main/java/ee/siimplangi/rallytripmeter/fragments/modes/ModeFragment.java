package ee.siimplangi.rallytripmeter.fragments.modes;

import android.os.Bundle;
import android.view.View;

import org.greenrobot.eventbus.EventBus;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.enums.TripType;
import ee.siimplangi.rallytripmeter.events.ResetEvent;

/**
 * Created by Siim on 13.09.2017.
 */

public abstract class ModeFragment extends Fragment {

    @Nullable
    private View resetView;

    @Override
    public void onStart() {
        super.onStart();
        //getActivity().setTitle(getTitle());
    }

    protected abstract String getTitle();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        resetView = view.findViewById(R.id.resetView);
    }

    @Nullable
    public View getResetView() {
        return resetView;
    }

    public void initDefaultResetViewBehaviour() {
        if (resetView != null) {
            resetView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new ResetEvent(TripType.TRIP1));
                }
            });
            resetView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    EventBus.getDefault().post(new ResetEvent(TripType.TRIP1));
                    EventBus.getDefault().post(new ResetEvent(TripType.TRIP2));
                    return true;
                }
            });
        }
    }
}
