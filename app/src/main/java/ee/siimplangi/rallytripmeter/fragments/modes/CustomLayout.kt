package ee.siimplangi.rallytripmeter.fragments.modes

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import androidx.annotation.IdRes
import ee.siimplangi.rallytripmeter.AppLog
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.activities.settings.CustomLayoutSettingsActivity
import ee.siimplangi.rallytripmeter.enums.CustomLayoutMode
import ee.siimplangi.rallytripmeter.fragments.components.BaseComponentFragment
import java.util.*

/**
 * Created by Siim on 17.09.2017.
 */

class CustomLayout : ModeFragment(), SharedPreferences.OnSharedPreferenceChangeListener {
    private lateinit var customLayoutMode: CustomLayoutMode
    private lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        customLayoutMode = CustomLayoutMode.valueOf(arguments!!.getString(KEY_CUSTOM_LAYOUT_ENUM))
        prefs = activity!!.getSharedPreferences(getString(customLayoutMode!!.prefsFile), Context.MODE_PRIVATE)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater!!.inflate(R.menu.custom_layout_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {
            R.id.layoutSettings -> {
                startActivity(CustomLayoutSettingsActivity.getIntent(context!!, customLayoutMode!!.prefsFile))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initDefaultResetViewBehaviour()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mode_custom_layout, container, false)
    }

    override fun onStart() {
        super.onStart()
        initializeRows()
        prefs.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onStop() {
        prefs.unregisterOnSharedPreferenceChangeListener(this)
        super.onStop()
    }


    private fun removeFrags() {
        val fragments = childFragmentManager.fragments
        for (fragment in fragments) {
            val ft = childFragmentManager.beginTransaction()
            ft.remove(fragment)
            ft.commit()
        }

    }

    private fun initializeRows() {
        removeFrags()
        initializeRow(R.id.row1, 1)
        initializeRow(R.id.row2, 2)
        initializeRow(R.id.row3, 3)
        initializeRow(R.id.row4, 4)
        initializeRow(R.id.row5, 5)
    }

    private fun initializeRow(@IdRes res: Int, rowId: Int) {
        val layoutParams = view!!.findViewById<View>(res).layoutParams as LinearLayout.LayoutParams
        val weightKey = "weight_$rowId"
        val rowKey = "row_$rowId"
        var columnKey: String
        var key: String
        val items = ArrayList<String>()
        for (i in 1..4) {
            columnKey = "column_$i"
            key = rowKey + "_" + columnKey
            val item = prefs!!.getString(key, null)
            if (item != null && "null" != item) {
                items.add(item)
            }
        }
        if (items.size > 0) {
            layoutParams.weight = Integer.parseInt(prefs.getString(weightKey, "1")!!).toFloat()
            val bundle = Bundle()
            bundle.putBoolean(BaseComponentFragment.ARG_IS_CUSTOM_LAYOUT, true)
            val ft = childFragmentManager.beginTransaction()
            for (className in items) {
                try {
                    val fragment = androidx.fragment.app.Fragment.instantiate(context, className, bundle)
                    ft.add(res, fragment)
                } catch (e: androidx.fragment.app.Fragment.InstantiationException) {
                    AppLog.e(CustomLayout::class.java.simpleName, "Failed to initialize fragment: $className");
                }

            }
            ft.commit()
        } else {
            layoutParams.weight = 0f
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        initializeRows()
    }

    override fun getTitle(): String {
        return getString(customLayoutMode!!.title)
    }

    companion object {

        //private static final Sku KEY_PREFS_FILE = "prefsFile";
        private val KEY_CUSTOM_LAYOUT_ENUM = "layout_enum"

        fun newInstance(mode: CustomLayoutMode): androidx.fragment.app.Fragment {
            val fragment = CustomLayout()
            val args = Bundle()
            args.putString(KEY_CUSTOM_LAYOUT_ENUM, mode.name)
            fragment.arguments = args
            return fragment
        }
    }


}
