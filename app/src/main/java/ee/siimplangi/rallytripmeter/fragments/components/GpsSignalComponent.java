package ee.siimplangi.rallytripmeter.fragments.components;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import androidx.annotation.Nullable;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.enums.ComponentType;
import ee.siimplangi.rallytripmeter.models.GpsAccuracy;

/**
 * Created by Siim on 17.09.2017.
 */

public class GpsSignalComponent extends EventBusComponentFragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gps_connection, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onGpsAccuraccy(EventBus.getDefault().getStickyEvent(GpsAccuracy.class));
    }

    @Subscribe(sticky = true)
    public void onGpsAccuraccy(GpsAccuracy gpsAccuracy) {
        if (gpsAccuracy != null && GpsAccuracy.Accuracy.SEARCHING.equals(gpsAccuracy.ACCURACY) && isHidden()) {
            show();
        } else if (!isHidden()) {
            hide();
        }
    }

    @NotNull
    @Override
    protected ComponentType getComponentType() {
        return ComponentType.GPS_SIGNAL;
    }
}
