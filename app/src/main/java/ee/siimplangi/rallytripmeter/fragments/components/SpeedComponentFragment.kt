package ee.siimplangi.rallytripmeter.fragments.components

import android.location.Location
import ee.siimplangi.rallytripmeter.enums.ComponentType
import ee.siimplangi.rallytripmeter.enums.Unit
import ee.siimplangi.rallytripmeter.helpers.converters.UnitConverter
import ee.siimplangi.rallytripmeter.models.GpsAccuracy
import ee.siimplangi.rallytripmeter.models.SectionTrip
import ee.siimplangi.rallytripmeter.models.SpecialStageTrip
import ee.siimplangi.rallytripmeter.models.TripModel
import org.greenrobot.eventbus.Subscribe
import java.text.DecimalFormat

/**
 * Created by Siim on 12.09.2017.
 */

abstract class SpeedComponentFragment : UnitComponentFragment() {

    override fun formatValue(value: Float, unit: Unit): String {
        var value = value
        val unitConverter = UnitConverter(Unit.METRIC, unit)
        value = unitConverter.convert(value)
        value = UnitConverter.MPS_TO_KPH.convert(value)
        return decimalFormat.format(value)
    }

    override fun getMeasurableTextString(): CharSequence {
        return "00.0"
    }

    override fun getUnitText(unit: Unit): String {
        return unit.getSpeedString(activity!!)
    }


    companion object {

        private val decimalFormat = DecimalFormat("0.0")
    }


}

class Average2 : SpeedComponentFragment() {
    override val componentType: ComponentType = ComponentType.TRIP2_AVG


    @Subscribe(sticky = true)
    fun onAvgTrip(average: TripModel.Trip2) {
        setValue(average.averageSpeed)
    }
}

class Average1 : SpeedComponentFragment() {

    override val componentType: ComponentType = ComponentType.TRIP1_AVG

    @Subscribe(sticky = true)
    fun onAvgTrip(average: TripModel.Trip1) {
        setValue(average.averageSpeed)
    }
}

class CurrentSpeed : SpeedComponentFragment() {

    override val componentType: ComponentType = ComponentType.CURRENT_SPEED

    @Subscribe(sticky = true)
    fun onLocation(location: Location) {
        if (location.hasSpeed()) {
            setValue(location.speed)
        } else {
            setValue(null)
        }

    }

    @Subscribe(sticky = true)
    fun onGpsAccuraccy(gpsAccuracy: GpsAccuracy?) {
        if (gpsAccuracy != null && GpsAccuracy.Accuracy.SEARCHING == gpsAccuracy.ACCURACY) {
            setValue(null)
        }
    }

}

class StageMaxSpeedComponent : SpeedComponentFragment() {

    override val componentType: ComponentType = ComponentType.STAGE_MAX

    @Subscribe(sticky = true)
    fun onStageUpdate(stageTrip: SpecialStageTrip) {
        setValue(stageTrip.maxSpeedMS)
    }

}

class StageAverageSpeedComponent : SpeedComponentFragment() {

    override val componentType: ComponentType = ComponentType.STAGE_AVG

    @Subscribe(sticky = true)
    fun onStageUpdate(stageTrip: SpecialStageTrip) {
        setValue(stageTrip.averageSpeed)
    }

}

class SectionAverageSpeedComponent : SpeedComponentFragment() {

    override val componentType: ComponentType = ComponentType.SECTION_AVG

    @Subscribe(sticky = true)
    fun onStageUpdate(sectionTrip: SectionTrip) {
        setValue(sectionTrip.averageSpeed)
    }

}
