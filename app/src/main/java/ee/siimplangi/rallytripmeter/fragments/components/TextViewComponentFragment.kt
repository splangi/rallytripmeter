package ee.siimplangi.rallytripmeter.fragments.components

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.StringRes
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.helpers.ComponentSettings
import ee.siimplangi.rallytripmeter.helpers.Settings
import ee.siimplangi.rallytripmeter.views.AutoResizeTextView

/**
 * Created by Siim on 12.09.2017.
 */

abstract class TextViewComponentFragment : EventBusComponentFragment() {

    lateinit var headingTextView: TextView
        private set
    lateinit var mainTextView: TextView
        private set
    lateinit var wrapper: View
        private set
    private var headerText: CharSequence? = null


    protected abstract fun getMeasurableText(): CharSequence

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private val headingStringRes: Int
        @StringRes
        get() = componentType.heading


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_component_layout, container, false)
    }


    override fun onInflate(context: Context, attrs: AttributeSet, savedInstanceState: Bundle?) {
        super.onInflate(context, attrs, savedInstanceState)
        val ta = context.obtainStyledAttributes(attrs, R.styleable.TextViewComponentFragment)
        headerText = ta.getText(R.styleable.TextViewComponentFragment_headerText)
        ta.recycle()
    }

    override fun onApplyCustomization(settings: ComponentSettings) {
        super.onApplyCustomization(settings)
        val color = settings.componentTextColor
        mainTextView.typeface = Settings.textFont.typeFace ?: mainTextView.typeface
        headingTextView.typeface = Settings.headingFont.typeFace ?: headingTextView.typeface
        mainTextView.setTextColor(color)
        val colorHeading = settings.componentHeadingColor
        headingTextView.setTextColor(colorHeading)
    }

    protected fun setTextColor(@ColorInt color: Int) {
        setTextColor(color, mainTextView)
    }

    protected fun setHeadingTextColor(@ColorInt color: Int) {
        setTextColor(color, headingTextView)
    }

    private fun setTextColor(@ColorInt color: Int, textView: TextView) {
        if (textView.currentTextColor != color) {
            textView.setTextColor(color)
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainTextView = view.findViewById(R.id.mainTextView)
        wrapper = view.findViewById(R.id.wrapper)
        if (mainTextView is AutoResizeTextView) {
            (mainTextView as AutoResizeTextView).setMeasurableText(getMeasurableText())
        }
        mainTextView.setSingleLine()
        headingTextView = view.findViewById(R.id.headingTextView)
        headingTextView.text = if (headerText != null) headerText else getString(headingStringRes)
    }
}
