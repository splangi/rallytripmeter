package ee.siimplangi.rallytripmeter.fragments.components

import android.view.View
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.enums.ComponentType
import ee.siimplangi.rallytripmeter.events.SectionAdvancer
import ee.siimplangi.rallytripmeter.extensions.safe
import ee.siimplangi.rallytripmeter.helpers.ComponentSettings
import ee.siimplangi.rallytripmeter.models.SpecialStage
import ee.siimplangi.rallytripmeter.models.SpecialStageTrip
import ee.siimplangi.rallytripmeter.models.StageTrip
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class NextSectionButton : ButtonComponentFragment() {


    var currentTrip: StageTrip? = null
    var currentActiveStage: SpecialStage? = null
    var isEnabled: Boolean = false
    var bgColorNormal: Int = 0
    var textColorNormal: Int = 0
    var bgColorOver: Int = 0
    var textColorOver: Int = 0

    override val componentType: ComponentType = ComponentType.NEXT_SECTION


    override val maxLines = 2

    override val buttonTextRes: Int
        get() = R.string.next_section

    override fun onClick(v: View?) {
        EventBus.getDefault().post(SectionAdvancer.NEXT)
    }

    override fun onApplyCustomization(settings: ComponentSettings) {
        super.onApplyCustomization(settings)
        isEnabled = settings.enableThreshold
        bgColorNormal = settings.buttonBG
        bgColorOver = settings.bgColorOver
        textColorOver = settings.txtColorOver
        textColorNormal = settings.componentTextColor
        applyCustomization(currentTrip, currentActiveStage)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()

    }

    @Subscribe(sticky = true)
    fun onEvent(trip: SpecialStageTrip) {
        currentTrip = trip
        applyCustomization(currentTrip, currentActiveStage)
    }

    @Subscribe(sticky = true)
    fun onEvent(activeStage: SpecialStage) {
        currentActiveStage = activeStage
        applyCustomization(currentTrip, currentActiveStage)
    }

    private fun setNormal() {
        setTextColor(textColorNormal)
        setButtonBG(bgColorNormal)
    }

    private fun setOver() {
        setTextColor(textColorOver)
        setButtonBG(bgColorOver)
    }

    private fun applyCustomization(trip: StageTrip?, activeStage: SpecialStage?) {
        safe(trip, activeStage?.stageDef, activeStage?.currentSection) { trip, stagedef, choice ->
            val section = stagedef.getCurrentSectionByChoice(choice)
            if (isEnabled == true && section != null) {
                if (section.to!! < trip.trip) {
                    setOver()
                } else {
                    setNormal()
                }
            } else {
                setNormal()
            }
        } ?: setNormal()

    }

}

class PrevSectionButton : ButtonComponentFragment() {
    override val componentType: ComponentType = ComponentType.PREV_SECTION


    override val maxLines = 2

    override val buttonTextRes: Int
        get() = R.string.prev_section

    override fun onClick(v: View?) {
        EventBus.getDefault().post(SectionAdvancer.PREV)
    }

}