package ee.siimplangi.rallytripmeter.fragments.settings;


import android.os.Bundle;

import androidx.preference.PreferenceFragment;


/**
 * Created by Siim on 17.09.2017.
 */

public class ComponentSettingsFragment extends PreferenceFragment {

    private static final String PREFERENCE_FILE_KEY = "prefsFile";
    private static final String PREFERENCE_XML_KEY = "prefs";

    public static ComponentSettingsFragment newInstance(String preferenceFile, int[] xmlValues) {
        Bundle args = new Bundle();
        args.putString(PREFERENCE_FILE_KEY, preferenceFile);
        args.putIntArray(PREFERENCE_XML_KEY, xmlValues);
        ComponentSettingsFragment fragment = new ComponentSettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        getPreferenceManager().setSharedPreferencesName(getArguments().getString(PREFERENCE_FILE_KEY));
        for (Integer xmlRes : getArguments().getIntArray(PREFERENCE_XML_KEY)) {
            addPreferencesFromResource(xmlRes);
        }
    }


}
