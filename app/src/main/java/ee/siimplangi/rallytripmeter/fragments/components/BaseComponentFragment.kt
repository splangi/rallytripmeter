package ee.siimplangi.rallytripmeter.fragments.components

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.CallSuper
import androidx.annotation.ColorInt
import androidx.appcompat.widget.PopupMenu
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.activities.StagesActivity
import ee.siimplangi.rallytripmeter.enums.CRUDAction
import ee.siimplangi.rallytripmeter.enums.ComponentType
import ee.siimplangi.rallytripmeter.helpers.ComponentSettings
import ee.siimplangi.rallytripmeter.storage.Storage
import ee.siimplangi.rallytripmeter.views.ColorGradientDrawable

/**
 * Created by Siim on 11.09.2017.
 */

abstract class BaseComponentFragment : androidx.fragment.app.Fragment() {

    protected abstract val componentType: ComponentType
    private var settings: ComponentSettings? = null
    var isCustomLayout: Boolean? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCustomLayout = arguments?.getBoolean(ARG_IS_CUSTOM_LAYOUT, false)
        settings = componentType.prefsFile?.let {
            ComponentSettings(activity!!.getSharedPreferences(getString(it), Context.MODE_PRIVATE))
        }
    }


    @CallSuper
    open fun onApplyCustomization(settings: ComponentSettings) {
        view?.background = createBGFromSettings(settings)
    }

    private fun initLayoutParams() {
        if (arguments != null && isCustomLayout == true) {
            val parent = view?.parent as LinearLayout
            val layoutParams = view?.layoutParams as LinearLayout.LayoutParams
            var height = LinearLayout.LayoutParams.MATCH_PARENT
            var width = LinearLayout.LayoutParams.MATCH_PARENT
            layoutParams.weight = arguments!!.getFloat(ARG_CUSTOM_LAYOUT_WEIGHT, 1f)
            /*val margin = dpToPx(arguments!!.getInt(ARG_CUSTOM_MARGIN, 4))
            layoutParams.bottomMargin = margin
            layoutParams.topMargin = margin
            layoutParams.rightMargin = margin
            layoutParams.leftMargin = margin*/
            if (parent.orientation == LinearLayout.HORIZONTAL) {
                width = 0
            } else {
                height = 0
            }
            layoutParams.height = height
            layoutParams.width = width
        }
    }

    override fun onStart() {
        initLayoutParams()
        val settings = this.settings
        if (settings != null) {
            onApplyCustomization(settings)
        }
        super.onStart()
    }

    fun setBGColor(@ColorInt color: Int) {
        val currentBG = view?.background
        if (currentBG == null || (currentBG is ColorDrawable && currentBG.color != color)) {
            view?.setBackgroundColor(color)
        } else if (currentBG is ColorGradientDrawable && currentBG.currentColor != color) {
            currentBG.setColor(color)
        } else if (currentBG !is ColorDrawable && currentBG !is ColorGradientDrawable) {
            view?.setBackgroundColor(color)
        }
    }

    private fun createBGFromSettings(settings: ComponentSettings): Drawable? {
        val drawable = ColorGradientDrawable()
        val bgColor = settings.componentBG
        val strokeWidth = if (settings.drawStroke) {
            dpToPx(settings.strokeWidth)
        } else {
            0
        }
        val strokeColor = settings.strokeColor
        if (bgColor == Color.TRANSPARENT && strokeWidth == 0) return null
        drawable.mutate()
        drawable.shape = GradientDrawable.RECTANGLE
        drawable.setColor(bgColor)
        drawable.setStroke(strokeWidth, strokeColor)
        return drawable

    }

    fun showSelectStageDialog(v: View) {
        val popupMenu = PopupMenu(context!!, v)
        popupMenu.inflate(R.menu.speed_limit_menu)
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.choose -> startSelectStageActivity()
                R.id.cancel -> popupMenu.dismiss()
                R.id.clear -> Storage.activeStage = null
            }
            true
        }
        popupMenu.show()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHOOSE_STAGE && resultCode == Activity.RESULT_OK) {
            Storage.activeStage = data?.getStringExtra(StagesActivity.SPECIAL_STAGE_REF_KEY)
        }
    }

    private fun startSelectStageActivity() {
        startActivityForResult(StagesActivity.getIntent(context!!, CRUDAction.READ), REQUEST_CHOOSE_STAGE)
    }

    protected fun hide() {
        fragmentManager!!.beginTransaction().hide(this).setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit()
    }

    protected fun show() {
        fragmentManager!!.beginTransaction().show(this).setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commit()
    }

    companion object {

        val ARG_IS_CUSTOM_LAYOUT = "customLayout"
        val ARG_CUSTOM_LAYOUT_WEIGHT = "layoutWeight"
        val ARG_CUSTOM_MARGIN = "customMargin"
        private const val REQUEST_CHOOSE_STAGE = 70

        fun dpToPx(dp: Int): Int {
            return (dp * Resources.getSystem().displayMetrics.density).toInt()
        }
    }

}
