package ee.siimplangi.rallytripmeter.fragments.components

import android.os.Bundle
import android.os.Handler
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.dialogs.TimePickerDialog
import ee.siimplangi.rallytripmeter.enums.ComponentType
import ee.siimplangi.rallytripmeter.events.NewStage
import ee.siimplangi.rallytripmeter.events.StopStage
import ee.siimplangi.rallytripmeter.helpers.Settings
import ee.siimplangi.rallytripmeter.models.RallyTime
import ee.siimplangi.rallytripmeter.models.SpecialStage
import ee.siimplangi.rallytripmeter.models.TcModel
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Siim on 13.09.2017.
 */

abstract class RallyChronometerComponent : TextViewComponentFragment() {

    private val mHandler = Handler()
    private val tickFrequency = tickFrequencyMillis()
    private var detached = true
    private var mTicker: Runnable = object : Runnable {
        override fun run() {
            if (detached || !isTicking)
                return
            val chronometer = mainTextView
            chronometer.text = formattedTimeText
            chronometer.invalidate()
            val now = Settings.rallyTime.currentRallyTimeMillis()
            val next = tickFrequency - now % tickFrequency
            mHandler.postDelayed(this, next)
        }
    }

    protected abstract val formattedTimeText: String

    protected abstract val isTicking: Boolean

    protected fun normalizeSeconds(value: Float, late: Boolean): Int {
        //Log.d(RallyChronometerComponent.class.getSimpleName(), "value" + value + " negative: " + negative);
        return if (!late) {
            Math.ceil(value.toDouble()).toInt()
        } else {
            Math.floor(value.toDouble()).toInt()
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        reStartTicker()
    }

    protected abstract fun tickFrequencyMillis(): Int

    protected fun reStartTicker() {
        mHandler.removeCallbacks(mTicker)
        mHandler.post(mTicker)
    }

    override fun onStart() {
        super.onStart()
        detached = false
        mHandler.post(mTicker)
    }

    override fun onStop() {
        detached = true
        mHandler.removeCallbacksAndMessages(null)
        super.onStop()
    }

    @Subscribe
    fun onNewAppTime(onAppTimeSet: RallyTime) {
        reStartTicker()
    }


}

class RallyTimeComponent : RallyChronometerComponent() {

    private val mFormat = "kk:mm:ss"

    override val formattedTimeText: String
        get() = DateFormat.format(mFormat, Settings.rallyTime.rallyTimeCalendar).toString()

    override val isTicking: Boolean
        get() = true

    override val componentType: ComponentType
        get() = ComponentType.RALLY_TIME

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.sublayout_rally_clock, container, false)
    }

    override fun getMeasurableText(): String {
        return "00:00:00"
    }

    override fun tickFrequencyMillis(): Int {
        return 1000
    }
}

class NextTcInTimerComponent : RallyChronometerComponent() {

    override val componentType: ComponentType = ComponentType.TIME_REMAINING

    private var tcModel: TcModel? = null

    override val formattedTimeText: String
        get() {
            if (tcModel == null || tcModel!!.nextTcInRallyCalendar == null) {
                return getString(R.string.NotAvailable)
            } else {
                var late = false
                val string: String
                val context = context
                var time = tcModel!!.nextTcInRallyCalendar!!.timeInMillis - System.currentTimeMillis()
                if (time < 0) {
                    late = true
                    time = Math.abs(time)
                }
                val seconds = normalizeSeconds(time / 1000f % 60f, late)
                val minutes = (time / (1000 * 60) % 60).toInt()
                val hours = (time / (1000 * 60 * 60) % 24).toInt()
                if (hours > 0) {
                    string = context!!.getString(R.string.timerLong, hours, minutes, seconds)
                } else {
                    string = context!!.getString(R.string.timerShort, minutes, seconds)
                }

                return if (late) "-$string" else string
            }

        }

    override val isTicking: Boolean
        get() = tcModel!!.isComplete

    protected val headingStringRes: Int
        get() = R.string.time_remaining


    override fun tickFrequencyMillis(): Int {
        return 1000
    }

    @Subscribe(sticky = true)
    fun onNewTcModel(tcModel: TcModel) {
        this.tcModel = tcModel
        if (tcModel.isComplete) {
            reStartTicker()
        }
    }

    override fun getMeasurableText(): String {
        return "00:00"
    }
}

class StageStopperComponent : RallyChronometerComponent() {
    private val handler = Handler()
    private var specialStage: SpecialStage? = null

    override val componentType: ComponentType = ComponentType.STAGE_TIMER

    override val formattedTimeText: String
        get() {
            if (specialStage == null) return ""
            var elapsedTimeInMillis: Long = specialStage!!.getElapsedTime() ?: return ""
            val negative = elapsedTimeInMillis < 0
            if (negative) {
                elapsedTimeInMillis = (-elapsedTimeInMillis)
            }
            val millis = (elapsedTimeInMillis % 1000).toInt() / 100
            val seconds = normalizeSeconds(elapsedTimeInMillis / 1000f % 60f, !negative)
            val minutes = (elapsedTimeInMillis / (1000 * 60) % 60).toInt()
            val hours = (elapsedTimeInMillis / (1000 * 60 * 60) % 24).toInt()
            val context = context
            val timeText: String
            timeText = when {
                negative -> decimalFormat.format(seconds.toLong())
                hours > 0 -> context!!.getString(R.string.stopWatchTextLong, hours, minutes, seconds, millis)
                else -> context!!.getString(R.string.stopWatchTextShort, minutes, seconds, millis)
            }
            return timeText
        }

    override val isTicking: Boolean
        get() = specialStage != null && specialStage!!.isLessThanTimeToStart(PRETICKER_DURATION) && !specialStage!!.hasEnded()

    override fun getMeasurableText(): String {
        return "00:00:0"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.sublayout_stop_watch, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        wrapper.setOnClickListener {
            if (specialStage != null && specialStage!!.isLessThanTimeToStart(PRETICKER_DURATION) && !specialStage!!.hasEnded()) {
                if (!Settings.endStageOnLongClick) {
                    EventBus.getDefault().post(StopStage())
                }
            } else {
                TimePickerDialog(context!!, R.string.setStartTime, TimePickerDialog.OnTimeSetListener { hourOfDay, minute, seconds -> EventBus.getDefault().post(NewStage(hourOfDay, minute, seconds)) }).show()
            }
        }
        wrapper.setOnLongClickListener(View.OnLongClickListener {
            if (specialStage != null && specialStage!!.isLessThanTimeToStart(PRETICKER_DURATION) && !specialStage!!.hasEnded()) {
                if (Settings.endStageOnLongClick) {
                    EventBus.getDefault().post(StopStage())
                    return@OnLongClickListener true
                }
            }
            false
        })
    }

    private fun setHintText(stage: SpecialStage) {
        val hintText = headingTextView
        if (stage.isLessThanTimeToStart(PRETICKER_DURATION) && !stage.hasEnded()) {
            hintText.setText(R.string.hintTextStopStopper)
        } else {
            hintText.setText(R.string.hintTextSetNewStartTime)
        }
    }

    private fun setText(stage: SpecialStage) {
        val textView = mainTextView
        if (stage.hasEnded()) {
            textView.text = formattedTimeText
        } else if (stage.isSet()) {
            sdf.timeZone = Settings.rallyTime.timezone
            textView.text = sdf.format(Date(stage.startTime!!))
        } else {
            textView.setText(R.string.set)
        }
    }

    private fun scheduleTicker(stage: SpecialStage) {
        val startTime = stage.startTime
        if (!stage.isRunning() && !stage.hasEnded() && startTime != null) {
            val delay = startTime - System.currentTimeMillis() - PRETICKER_DURATION
            handler.postDelayed({ onStageModelUpdate(stage) }, Math.max(0, delay))
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        handler.removeCallbacksAndMessages(null)
        super.onStop()
    }

    @Subscribe(sticky = true)
    fun onStageModelUpdate(stage: SpecialStage) {
        handler.removeCallbacksAndMessages(null)
        this.specialStage = stage
        if (isTicking) {
            reStartTicker()
        } else {
            scheduleTicker(stage)
            setText(stage)
        }
        setHintText(stage)
    }


    override fun tickFrequencyMillis(): Int {
        return 100
    }

    companion object {

        private val PRETICKER_DURATION = (30 * 1000).toLong()
        private val decimalFormat = DecimalFormat("0")
        private val sdf = SimpleDateFormat("HH:mm:ss")
    }
}
