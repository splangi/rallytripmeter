package ee.siimplangi.rallytripmeter.fragments.components

import android.os.Bundle
import android.view.View
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.dialogs.TimePickerDialog
import ee.siimplangi.rallytripmeter.enums.ComponentType
import ee.siimplangi.rallytripmeter.events.AllowedTime
import ee.siimplangi.rallytripmeter.events.TCOutTime
import ee.siimplangi.rallytripmeter.models.TcModel
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Siim on 14.09.2017.
 */

abstract class TimeComponentFragment : TextViewComponentFragment() {

    private val SDF_W_SECONDS = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
    private val SDF_WO_SECONDS = SimpleDateFormat("HH:mm", Locale.getDefault())


    override fun getMeasurableText(): String {
        return "00:00"
    }

    fun setText(calendar: Calendar?) {
        if (calendar == null) {
            mainTextView.setText(R.string.NotAvailable)
        } else {
            val sdf: SimpleDateFormat = if (calendar.get(Calendar.SECOND) == 0) {
                SDF_WO_SECONDS
            } else {
                SDF_W_SECONDS
            }

            sdf.timeZone = calendar.timeZone
            mainTextView.text = sdf.format(calendar.time)
        }

    }


}

class NextTcInComponent : TimeComponentFragment() {
    override val componentType: ComponentType = ComponentType.NEXT_TC

    @Subscribe(sticky = true)
    fun onNewTcModel(tcModel: TcModel) {
        setText(tcModel.nextTcInRallyCalendar)
    }

}

class LastTcOutComponent : TimeComponentFragment() {


    override val componentType: ComponentType = ComponentType.LAST_TC

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        wrapper.setOnClickListener { TimePickerDialog(context!!, R.string.lastTC, TimePickerDialog.OnTimeSetListener { hourOfDay, minute, seconds -> EventBus.getDefault().post(TCOutTime(hourOfDay, minute, seconds)) }).show() }
    }

    @Subscribe(sticky = true)
    fun onNewTcModel(tcModel: TcModel) {
        val tcOutTime = tcModel.tcOutTime
        if (tcOutTime == null) {
            setText(null)
        } else {
            setText(tcOutTime.calendar)
        }
    }
}

class AllowedTimeComponent : TimeComponentFragment() {

    override val componentType: ComponentType = ComponentType.ALLOWED_TIME

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        wrapper.setOnClickListener {
            val alertDialog = TimePickerDialog(context!!, R.string.allowedTime, TimePickerDialog.OnTimeSetListener { hourOfDay, minute, seconds -> EventBus.getDefault().post(AllowedTime(hourOfDay, minute, seconds)) }, true, 1, false)
            alertDialog.setTitle(R.string.allowedTime)
            alertDialog.show()
        }
    }


    @Subscribe(sticky = true)
    fun onNewTcModel(tcModel: TcModel) {
        if (tcModel.allowedTime != null) {
            setText(tcModel.allowedTime!!.asCalendar())
        } else {
            setText(null)
        }

    }

    companion object {

        private val decimalFormat = DecimalFormat("0")
    }
}
