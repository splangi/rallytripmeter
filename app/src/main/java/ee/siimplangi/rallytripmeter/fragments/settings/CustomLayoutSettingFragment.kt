package ee.siimplangi.rallytripmeter.fragments.settings


import android.os.Bundle
import androidx.preference.*
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.enums.ComponentType
import java.util.*

/**
 * Created by Siim on 17.09.2017.
 */

class CustomLayoutSettingFragment : PreferenceFragment() {

    companion object {

        private const val PREFERENCE_FILE_KEY = "prefsFile"

        fun newInstance(preferenceFile: String): CustomLayoutSettingFragment {
            val args = Bundle()
            args.putString(PREFERENCE_FILE_KEY, preferenceFile)
            val fragment = CustomLayoutSettingFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        preferenceManager.sharedPreferencesName = arguments.getString(PREFERENCE_FILE_KEY)
        //setPreferencesFromResource(R.xml.prefs_custom_layout, rootKey)
        val screen = preferenceManager.createPreferenceScreen(activity)
        preferenceScreen = screen
        screen.addPreference(createEnableLayoutPreference())
        for (row in 1..5) {
            val category = createRow(row, screen)
            for (column in 1..4) {
                createComponentPreference(row, column, category)
            }
            createRowWeight(row, category)
        }

    }


    private fun createEnableLayoutPreference(): SwitchPreference {
        return SwitchPreference(activity).also {
            it.key = getString(R.string.prefs_key_enable_layout)
            it.title = getString(R.string.enable_layout)
            it.setDefaultValue(false)
        }
    }

    private fun createComponentPreference(row: Int, column: Int, category: PreferenceCategory) {
        val pref = ListPreference(activity)
        pref.also {
            it.key = "row_${row}_column_$column"
            it.entries = Collections.singletonList(getString(R.string.none)).plus(ComponentType.values().filter { type -> type.isCustom }.map { type -> getString(type.heading) }).toTypedArray()
            it.entryValues = Collections.singletonList<String>("null").plus(ComponentType.values().filter { type -> type.isCustom }.map { type -> type.clazz.java.canonicalName }).toTypedArray()
            it.title = getString(when (column) {
                1 -> R.string.column_1
                2 -> R.string.column_2
                3 -> R.string.column_3
                else -> R.string.column_4

            })
            it.summary = "%s"
        }
        category.addPreference(pref)
    }

    private fun createRow(rowNo: Int, preferenceScreen: PreferenceScreen): PreferenceCategory {
        val row = PreferenceCategory(activity)
        preferenceScreen.addPreference(row)
        row.also {
            it.title = getString(when (rowNo) {
                1 -> R.string.row_1
                2 -> R.string.row_2
                3 -> R.string.row_3
                4 -> R.string.row_4
                else -> R.string.row_5
            })
            it.dependency = getString(R.string.prefs_key_enable_layout)
        }
        return row;
    }

    private fun createRowWeight(row: Int, preferenceCategory: PreferenceCategory) {
        val weight = ListPreference(activity)

        weight.also {
            it.key = "weight_$row"
            it.title = getString(R.string.row_weight)
            it.summary = "%s"
            it.entries = resources.getStringArray(R.array.prefs_value_weights)
            it.entryValues = resources.getStringArray(R.array.prefs_value_weights)
        }
        preferenceCategory.addPreference(weight)
    }


}
