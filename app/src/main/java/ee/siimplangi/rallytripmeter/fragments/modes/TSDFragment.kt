package ee.siimplangi.rallytripmeter.fragments.modes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.enums.TripType
import ee.siimplangi.rallytripmeter.events.ResetEvent
import org.greenrobot.eventbus.EventBus

class TSDFragment : ModeFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_mode_tsd, container, false)
        view.keepScreenOn = true
        return view
    }

    override fun getTitle(): String {
        return getString(R.string.tsd)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val resetView = this.resetView
        if (resetView != null) {
            resetView.setOnLongClickListener {
                EventBus.getDefault().post(ResetEvent(TripType.SECTION))
                true
            }
        }
    }
}