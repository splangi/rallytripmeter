package ee.siimplangi.rallytripmeter.fragments.components

import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.enums.ComponentType
import ee.siimplangi.rallytripmeter.helpers.ComponentSettings
import ee.siimplangi.rallytripmeter.models.SpecialStage
import ee.siimplangi.rallytripmeter.models.SpecialStageTrip
import ee.siimplangi.rallytripmeter.models.StageDef
import org.greenrobot.eventbus.Subscribe

class SectionFromComponent : DistanceComponentFragment() {
    override val componentType: ComponentType = ComponentType.SECTION_FROM

    @Subscribe(sticky = true)
    fun onActiveStageChange(currentSpecialStage: SpecialStage) {
        setValue(currentSpecialStage.stageDef?.getFromValueBySection(currentSpecialStage.currentSection)?.toFloat())
    }


}

class SectionToComponent : DistanceComponentFragment() {

    override val componentType: ComponentType = ComponentType.SECTION_TO

    @Subscribe(sticky = true)
    fun onActiveStageChange(currentSpecialStage: SpecialStage) {
        setValue(currentSpecialStage.stageDef?.getToValueBySection(currentSpecialStage.currentSection)?.toFloat())
    }


}

class SectionSpeedComponent : SpeedComponentFragment() {

    override val componentType: ComponentType = ComponentType.TARGET_SPEED


    @Subscribe(sticky = true)
    fun onActiveStageChange(currentSpecialStage: SpecialStage) {
        setValue(currentSpecialStage.stageDef?.getSpeedConstraints(currentSpecialStage.currentSection)?.toFloat())
    }


}


class TimeToIdeal : TextViewComponentFragment() {

    private var bgColorNormal: Int = 0
    private var textColorNormal: Int = 0
    private var thresholdEnabled: Boolean = false
    private var threshold: Int = 0
    private var bgColorOver: Int = 0
    private var bgColorUnder: Int = 0
    private var textColorOver: Int = 0
    private var textColorUnder: Int = 0
    private var secondShort: String = "s"

    private var currentState: Int = 0

    override val componentType: ComponentType = ComponentType.IDEAL_TIME


    private var currentStageDef: StageDef? = null
    private var stageTrip: SpecialStageTrip? = null

    override fun getMeasurableText(): CharSequence {
        return "-30.0s"
    }


    @Subscribe(sticky = true)
    fun onActiveStageChange(currentSpecialStage: SpecialStage) {
        this.currentStageDef = currentSpecialStage.stageDef
        updateView()
    }

    @Subscribe(sticky = true)
    fun onStageTrip(stageTrip: SpecialStageTrip) {
        this.stageTrip = stageTrip
        updateView()
    }

    override fun onApplyCustomization(settings: ComponentSettings) {
        super.onApplyCustomization(settings)
        secondShort = getString(R.string.seconds_short)
        bgColorNormal = settings.componentBG
        textColorNormal = settings.componentTextColor
        thresholdEnabled = settings.enableThreshold
        if (thresholdEnabled) {
            bgColorOver = settings.bgColorOver
            bgColorUnder = settings.bgColorUnder
            textColorOver = settings.txtColorOver
            textColorUnder = settings.txtColorUnder
            threshold = settings.threshold
        }

    }

    private fun convertMillisToText(fromIdealTime: Long): String {
        var fromIdealTimeVar = fromIdealTime
        val negative = fromIdealTimeVar < 0
        var append: String = ""
        if (negative) {
            fromIdealTimeVar = (-fromIdealTimeVar)!!
            append = "-"
        }
        val millis = (fromIdealTimeVar % 1000).toInt() / 100
        val seconds = (fromIdealTimeVar / 1000f % 60f).toInt()
        val minutes = (fromIdealTimeVar / (1000 * 60)).toInt()
        return append + if (minutes > 0) {
            context?.getString(R.string.ideal_time_text_long, minutes, seconds) ?: ""
        } else {
            context?.getString(R.string.ideal_time_text_short, seconds, millis, secondShort) ?: ""
        }
    }

    private fun setNormal() {
        setTextColor(textColorNormal)
        setBGColor(bgColorNormal)

    }

    private fun setOver() {
        setTextColor(textColorOver)
        setBGColor(bgColorOver)

    }

    private fun setUnder() {
        setTextColor(textColorUnder)
        setBGColor(bgColorUnder)
    }

    private fun setColors(fromIdealTime: Long) {
        val fromIdealTimeSecs = fromIdealTime.toDouble() / 1000.0
        if (!thresholdEnabled) {
            setNormal()
        } else if (-threshold < fromIdealTimeSecs && fromIdealTimeSecs < threshold) {
            setNormal()
        } else if (-threshold > fromIdealTimeSecs) {
            setOver()
        } else if (threshold < fromIdealTimeSecs) {
            setUnder()
        }
    }


    private fun updateView() {
        val trip = stageTrip
        val def = currentStageDef
        if (trip != null && def != null) {
            val idealTime = def.getIdealTime(trip.trip.toDouble())
            if ((trip.isRunning || trip.hasEnded()) && idealTime != null) {
                mainTextView.text = convertMillisToText(trip.elapsedTime - idealTime)
                setColors(trip.elapsedTime - idealTime)
            } else if (idealTime != null) {
                mainTextView.text = getString(R.string.zero_seconds)
                setColors(0)
            } else {
                mainTextView.text = getString(R.string.NotAvailable)
                setColors(0)
            }
        } else {
            mainTextView.setText(R.string.NotAvailable)
            setColors(0)
        }

    }


}