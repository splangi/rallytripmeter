package ee.siimplangi.rallytripmeter.fragments.components

import android.location.Location
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.enums.ComponentType
import ee.siimplangi.rallytripmeter.events.ActiveStage
import ee.siimplangi.rallytripmeter.extensions.convertMPSTo
import ee.siimplangi.rallytripmeter.helpers.AuthHelper
import ee.siimplangi.rallytripmeter.helpers.Settings
import ee.siimplangi.rallytripmeter.models.StageDef
import ee.siimplangi.rallytripmeter.models.TripModel
import ee.siimplangi.rallytripmeter.views.SpeedLimitView
import org.greenrobot.eventbus.Subscribe

/**
 * Created by Siim on 11.09.2017.
 */

class SpeedLimitFragment : EventBusComponentFragment() {


    private var limit: StageDef? = null
    private var speedMS: Float = 0F
    private var totalDistance: Float = 0F

    private lateinit var speedLimitView: SpeedLimitView
    private lateinit var authHelper: AuthHelper

    private lateinit var toneGenerator: ToneGenerator

    private var playingTone = false

    private val onWrapperClicked = View.OnClickListener {
        showSelectStageDialog(speedLimitView)
    }

    override val componentType: ComponentType
        get() = ComponentType.SPEED_LIMIT

    @Subscribe(sticky = true)
    fun onSpeedLimitUpdateEvent(updateEvent: ActiveStage) {
        updateLimits(updateEvent.stageDef)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toneGenerator = ToneGenerator(AudioManager.STREAM_ALARM, 100)
        authHelper = AuthHelper()
    }

    override fun onDestroy() {
        toneGenerator.release()
        super.onDestroy()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_speed_limit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.setOnClickListener(onWrapperClicked)
        speedLimitView = view.findViewById(R.id.speedLimitView)
    }

    @Subscribe(sticky = true)
    fun onLocation(location: Location) {
        this.speedMS = location.speed
        updateLimits(limit)
    }

    @Subscribe(sticky = true)
    fun onTrip2(trip2: TripModel.Trip2) {
        this.totalDistance = trip2.trip
        updateLimits(limit)
    }

    fun updateLimits(limit: StageDef?) {
        this.limit = limit
        if (limit == null) {
            speedLimitView.isActivated = false
            speedLimitView.setLimit(null)
            if (playingTone) {
                playingTone = false
            }

        } else {
            speedLimitView.isActivated = limit.areLimitsViolated(speedMS.toDouble(), totalDistance.toDouble())
            speedLimitView.setLimit(limit.getCurrentSectionByDistance(totalDistance.toDouble())?.speed?.convertMPSTo(Settings.unit)?.toFloat())
            if (Settings.showSpeedLimit && Settings.beepOn() && limit.areLimitsViolated(speedMS.toDouble(), totalDistance.toDouble()) && !playingTone) {
                playingTone = true
                toneGenerator.startTone(ToneGenerator.TONE_CDMA_PIP)
            } else if (!limit.areLimitsViolated(speedMS.toDouble(), totalDistance.toDouble())) {
                playingTone = false
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (Settings.showSpeedLimit || isCustomLayout == true) {
            show()
        } else {
            hide()
        }
    }


}
