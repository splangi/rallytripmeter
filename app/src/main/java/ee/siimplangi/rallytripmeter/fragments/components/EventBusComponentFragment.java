package ee.siimplangi.rallytripmeter.fragments.components;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Siim on 13.09.2017.
 */

public abstract class EventBusComponentFragment extends BaseComponentFragment {

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

    }

}
