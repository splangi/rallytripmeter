package ee.siimplangi.rallytripmeter.fragments.components

import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.appcompat.widget.AppCompatButton
import androidx.core.view.ViewCompat
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.enums.ComponentType
import ee.siimplangi.rallytripmeter.enums.TripType
import ee.siimplangi.rallytripmeter.events.ResetEvent
import ee.siimplangi.rallytripmeter.events.TripPlus
import ee.siimplangi.rallytripmeter.helpers.ComponentSettings
import ee.siimplangi.rallytripmeter.helpers.Settings
import org.greenrobot.eventbus.EventBus

abstract class ButtonComponentFragment : BaseComponentFragment(), View.OnClickListener, View.OnLongClickListener {

    lateinit var btn: AppCompatButton
    abstract val buttonTextRes: Int
    open val maxLines = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.putInt(BaseComponentFragment.ARG_CUSTOM_MARGIN, 0)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_component_button, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn = view.findViewById(R.id.button)
        btn.setOnClickListener(this)
        btn.setText(buttonTextRes)
        btn.maxLines = maxLines
    }

    override fun onApplyCustomization(settings: ComponentSettings) {
        super.onApplyCustomization(settings)
        setButtonBG(settings.buttonBG)
        setTextColor(settings.componentTextColor)
        Settings.textFont.typeFace?.let {
            btn.typeface = it
        }

    }

    override fun onLongClick(v: View?): Boolean {
        Log.d(ButtonComponentFragment::class.java.simpleName, "onLongClick!")
        return false
    }

    protected fun setButtonBG(@ColorInt color: Int) {
        ViewCompat.setBackgroundTintList(btn, ColorStateList.valueOf(color))
    }

    protected fun setTextColor(@ColorInt color: Int) {
        btn.setTextColor(color)
    }


}

abstract class TripAdd : ButtonComponentFragment() {

    abstract val plusAmount: Int

    override fun onClick(v: View?) {
        EventBus.getDefault().post(TripPlus(plusAmount))
    }

}

class ResetTrip1 : ButtonComponentFragment() {

    override val componentType: ComponentType
        get() = ComponentType.RESET_TRIP1

    override val buttonTextRes: Int
        get() = componentType.heading

    override val maxLines: Int
        get() = 2


    override fun onClick(v: View?) {
        EventBus.getDefault().post(ResetEvent(TripType.TRIP1))
    }

}

class ResetSectionTrip : ButtonComponentFragment() {
    override val componentType: ComponentType
        get() = ComponentType.RESET_TRIP1

    override val buttonTextRes: Int
        get() = componentType.heading

    override val maxLines: Int
        get() = 2


    override fun onClick(v: View?) {
        EventBus.getDefault().post(ResetEvent(TripType.SECTION))
    }

}

class ResetTrip2 : ButtonComponentFragment() {

    override val maxLines: Int
        get() = 2

    override val componentType: ComponentType get() = ComponentType.RESET_TRIP2


    override val buttonTextRes: Int
        get() = componentType.heading

    override fun onClick(v: View?) {
        EventBus.getDefault().post(ResetEvent(TripType.TRIP2))
    }

}


class Add10 : TripAdd() {
    override val componentType: ComponentType get() = ComponentType.PLUS10


    override val buttonTextRes: Int = R.string.plus10
    override val plusAmount: Int = 10
}

class Add100 : TripAdd() {
    override val componentType: ComponentType = ComponentType.PLUS100

    override val buttonTextRes: Int = R.string.plus100
    override val plusAmount: Int = 100
}

class Remove10 : TripAdd() {
    override val componentType: ComponentType = ComponentType.MINUS10

    override val buttonTextRes: Int = R.string.minus10
    override val plusAmount: Int = -10
}

class Remove100 : TripAdd() {
    override val componentType: ComponentType = ComponentType.MINUS100

    override val buttonTextRes: Int = R.string.minus100
    override val plusAmount: Int = -100
}