package ee.siimplangi.rallytripmeter.fragments.settings

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceFragment
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.activities.settings.ComponentSettingsActivity
import ee.siimplangi.rallytripmeter.activities.settings.CustomLayoutSettingsActivity
import ee.siimplangi.rallytripmeter.enums.ComponentType
import ee.siimplangi.rallytripmeter.enums.CustomLayoutMode
import ee.siimplangi.rallytripmeter.enums.Font

class UICustomizationFragment : PreferenceFragment() {

    companion object {

        fun newInstance(): UICustomizationFragment {
            return UICustomizationFragment()
        }

    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        val layoutCategory = PreferenceCategory(activity)
        preferenceScreen = preferenceManager.createPreferenceScreen(activity)
        addPreferencesFromResource(R.xml.prefs_global_customization)
        val globalCategory: PreferenceCategory = findPreference(getString(R.string.global_category_key)) as PreferenceCategory
        globalCategory.addPreference(createGlobalFontPreference(R.string.prefs_key_global_main_text_font, R.string.prefs_text_font))
        globalCategory.addPreference(createGlobalFontPreference(R.string.prefs_key_global_heading_text_font, R.string.prefs_heading_font))
        preferenceScreen.addPreference(layoutCategory)
        layoutCategory.title = getString(R.string.customize_layouts)
        for (mode: CustomLayoutMode in CustomLayoutMode.values()) {
            layoutCategory.addPreference(createPreference(mode))
        }
        val typeCategory = PreferenceCategory(activity)
        preferenceScreen.addPreference(typeCategory)
        typeCategory.title = getString(R.string.customize_components)
        for (componentType: ComponentType in ComponentType.values().filter { type -> type.isCustom }.sortedBy { getString(it.heading) }) {
            typeCategory.addPreference(createPreference(componentType))
        }
    }

    private fun createGlobalFontPreference(@StringRes key: Int, @StringRes titleRes: Int): Preference {
        return ListPreference(activity!!).also {
            it.entryValues = Font.values().map { font -> font.name }.toTypedArray()
            it.entries = Font.values().map { font -> getString(font.heading) }.toTypedArray()
            it.key = getString(key)
            it.summary = "%s"
            it.title = getString(titleRes)
            it.setDefaultValue(Font.SYSTEM_NORMAL.name)
        }

    }

    private fun createPreference(mode: CustomLayoutMode): Preference {
        return Preference(activity).also {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                it.setViewId(View.generateViewId())
            }
            it.intent = CustomLayoutSettingsActivity.getIntent(activity, mode.prefsFile)
            it.title = getString(mode.title)
        }
    }

    private fun createPreference(componentType: ComponentType): Preference {
        return Preference(activity).also {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                it.setViewId(View.generateViewId())
            }
            it.intent = ComponentSettingsActivity.getIntent(activity, componentType.heading, componentType.prefsFile!!, *componentType.prefsXMLs)
            it.title = getString(componentType.heading)
        }
    }

}