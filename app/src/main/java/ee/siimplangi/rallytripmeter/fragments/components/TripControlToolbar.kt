package ee.siimplangi.rallytripmeter.fragments.components

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.dialogs.DialogFactory
import ee.siimplangi.rallytripmeter.enums.ComponentType
import ee.siimplangi.rallytripmeter.enums.TripType
import ee.siimplangi.rallytripmeter.enums.Unit
import ee.siimplangi.rallytripmeter.events.TripControlEvent
import ee.siimplangi.rallytripmeter.events.TripEditEvent
import ee.siimplangi.rallytripmeter.helpers.Settings
import ee.siimplangi.rallytripmeter.helpers.converters.UnitConverter
import ee.siimplangi.rallytripmeter.models.TripControl
import ee.siimplangi.rallytripmeter.models.TripControl.READ_DOWN
import ee.siimplangi.rallytripmeter.models.TripModel
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Created by Siim on 13.09.2017.
 */

class TripControlToolbar : EventBusComponentFragment() {

    private lateinit var wrapper: View
    private lateinit var spinner: Spinner
    private lateinit var readUpButton: View
    private lateinit var readDownButton: View
    private lateinit var pauseButton: View
    private lateinit var editButton: View

    private val currentlySelectedTripType: TripType
        get() = TripType.values().getOrElse(spinner.selectedItemPosition) { _ -> TripType.TRIP1 }

    override val componentType: ComponentType
        get() = ComponentType.TRIP_TOOLBAR


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_trip_edit_toolbar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        spinner = view.findViewById(R.id.spinner)
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                syncControlButtons()
            }

        }
        spinner.adapter = ArrayAdapter<String>(context!!, R.layout.view_spinner_textitem, R.id.textView, TripType.values().map { it.name })
        readUpButton = view.findViewById(R.id.readUpButton)
        pauseButton = view.findViewById(R.id.pauseButton)
        editButton = view.findViewById(R.id.editButton)
        readDownButton = view.findViewById(R.id.readDownButton)
        readUpButton!!.setOnClickListener(TripControlButtonClickListener(TripControl.READ_UP))
        pauseButton!!.setOnClickListener(TripControlButtonClickListener(TripControl.PAUSE))
        readDownButton!!.setOnClickListener(TripControlButtonClickListener(READ_DOWN))
        editButton!!.setOnClickListener { startEditTripDialog() }
        wrapper = view.findViewById(R.id.wrapper)
        unselectControlButtons()
    }

    private fun startEditTripDialog() {
        val tripType = currentlySelectedTripType
        val alertDialog = DialogFactory.getEditTextDialog(context, R.layout.dialog_distance_edittext) { dialogInterface, data ->
            try {
                var f = java.lang.Float.parseFloat(data)
                val unitConverter = UnitConverter(Settings.unit, Unit.METRIC)
                f = unitConverter.convert(f)!!
                f = UnitConverter.KILOMETERS_TO_METERS.convert(f)!!
                EventBus.getDefault().post(TripEditEvent(f, tripType))
                dialogInterface.dismiss()
            } catch (e: NumberFormatException) {
                Toast.makeText(context, R.string.invalid_input, Toast.LENGTH_SHORT).show()
            }
        }
        alertDialog.setTitle(R.string.set_new_value)
        alertDialog.show()
    }

    private fun syncControlButtons() {
        unselectControlButtons()
        val currentTrip = currentlySelectedTripType.modelClass
        val tripModel = EventBus.getDefault().getStickyEvent(currentTrip)
        if (tripModel != null) {
            when (tripModel.tripControl) {
                TripControl.READ_UP -> readUpButton.isSelected = true
                TripControl.PAUSE -> pauseButton.isSelected = true
                READ_DOWN -> readDownButton.isSelected = true
            }
        }
    }

    private fun isOutOfSync(control: TripControl): Boolean {
        return when (control) {
            TripControl.READ_UP -> !readUpButton.isSelected
            TripControl.PAUSE -> !pauseButton.isSelected
            READ_DOWN -> !readDownButton.isSelected
        }
    }


    @Subscribe
    fun onTripUpdate(tripModel: TripModel) {
        if (currentlySelectedTripType.modelClass == tripModel.javaClass && isOutOfSync(tripModel.tripControl)) {
            syncControlButtons()
        }
    }


    override fun onStart() {
        super.onStart()
        syncControlButtons()
        if (!Settings.showToolbar) {
            hide()
        } else {
            show()
            wrapper.setOnClickListener(null)
            setOtherViewsClickable(true)
        }
    }

    private fun setOtherViewsClickable(clickable: Boolean) {
        readUpButton.isClickable = clickable
        readDownButton.isClickable = clickable
        pauseButton.isClickable = clickable
        editButton.isClickable = clickable
    }

    private fun unselectControlButtons() {
        readUpButton.isSelected = false
        pauseButton.isSelected = false
        readDownButton.isSelected = false
    }

    private inner class TripControlButtonClickListener(private val tripControl: TripControl) : View.OnClickListener {

        override fun onClick(view: View) {
            EventBus.getDefault().post(TripControlEvent(currentlySelectedTripType, tripControl))
        }
    }
}
