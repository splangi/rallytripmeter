package ee.siimplangi.rallytripmeter.fragments.settings;


import android.os.Bundle;

import androidx.preference.Preference;
import androidx.preference.PreferenceDialogFragment;
import androidx.preference.PreferenceFragment;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.dialogs.TimePickerPreferenceDialog;
import ee.siimplangi.rallytripmeter.preferences.TimePickerDialogPreference;

/**
 * Created by Siim on 10.09.2017.
 */

public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.prefs, rootKey);
    }

    @Override
    public void onDisplayPreferenceDialog(Preference preference) {
        if (preference instanceof TimePickerDialogPreference) {
            PreferenceDialogFragment dialogFragment;
            if (preference.getKey().equals(getString(R.string.prefs_key_rallytime))){
                dialogFragment = TimePickerPreferenceDialog.RallyTimePreferenceDialog.newInstance(preference.getKey());
                dialogFragment.setTargetFragment(this, 0);
                dialogFragment.show(getFragmentManager(), "RallyTimeDialog");
            }
        } else{
            super.onDisplayPreferenceDialog(preference);
        }

    }


}
