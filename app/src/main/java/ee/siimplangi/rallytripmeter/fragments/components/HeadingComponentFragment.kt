package ee.siimplangi.rallytripmeter.fragments.components

import android.location.Location
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.enums.ComponentType
import org.greenrobot.eventbus.Subscribe
import java.text.DecimalFormat

class HeadingComponentFragment : TextViewComponentFragment() {
    override val componentType: ComponentType = ComponentType.HEADING


    val df = DecimalFormat("#")

    var hasHeading = false

    override fun getMeasurableText(): CharSequence {
        return "259°"
    }

    @Subscribe(sticky = true)
    fun onLocation(location: Location) {
        if (location.hasBearing()) {
            mainTextView.text = df.format(location.bearing) + "°"
            hasHeading = true
        } else if (!hasHeading) {
            mainTextView.text = getString(R.string.NotAvailable)
        }

    }


}