package ee.siimplangi.rallytripmeter.fragments.modes

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import ee.siimplangi.rallytripmeter.R
import ee.siimplangi.rallytripmeter.activities.StagesActivity
import ee.siimplangi.rallytripmeter.enums.CRUDAction
import ee.siimplangi.rallytripmeter.storage.Storage

class TSDAdvancedFragment : ModeFragment() {

    companion object {
        private const val KEY_CHOOSE_STAGE = 23
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater!!.inflate(R.menu.tsd_advanced_menu, menu)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_mode_tsd_advanced, container, false)
        view.keepScreenOn = true
        return view
    }

    override fun getTitle(): String {
        return getString(R.string.tsd_advanced)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {
            R.id.stages -> {
                startActivityForResult(StagesActivity.getIntent(context!!, CRUDAction.READ), KEY_CHOOSE_STAGE)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == KEY_CHOOSE_STAGE && resultCode == Activity.RESULT_OK) {
            Storage.activeStage = data?.getStringExtra(StagesActivity.SPECIAL_STAGE_REF_KEY)
        }
    }


}