package ee.siimplangi.rallytripmeter.fragments.modes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import ee.siimplangi.rallytripmeter.R;

public class LiaisonFragment extends ModeFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_mode_liaison, container, false);
		view.setKeepScreenOn(true);
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		initDefaultResetViewBehaviour();
	}

	@Override
	protected String getTitle() {
		return getString(R.string.liaisonmode);
	}
}
