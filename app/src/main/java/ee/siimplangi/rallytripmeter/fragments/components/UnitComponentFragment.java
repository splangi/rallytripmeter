package ee.siimplangi.rallytripmeter.fragments.components;

import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.text.style.SubscriptSpan;

import org.greenrobot.eventbus.Subscribe;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.enums.Unit;
import ee.siimplangi.rallytripmeter.helpers.Settings;

/**
 * Created by Siim on 12.09.2017.
 */

public abstract class UnitComponentFragment extends TextViewComponentFragment {

    private Float value;

    protected abstract String formatValue(@NonNull Float value, @NonNull Unit unit);

    protected abstract String getUnitText(Unit unit);

    protected abstract CharSequence getMeasurableTextString();

    @Override
    protected Spannable getMeasurableText() {
        return getTextWithUnit(getMeasurableTextString());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Subscribe
    public void onUnitsChanged(Unit unit) {
        setValue(value);
    }

    public void setValue(@Nullable Float value) {
        this.value = value;
        if (value == null) {
            getMainTextView().setText(R.string.NotAvailable);
        } else {
            getMainTextView().setText(getTextWithUnit(formatValue(value, Settings.INSTANCE.getUnit())));
        }
    }

    private Spannable getTextWithUnit(CharSequence s) {
        String unitText = getUnitText(Settings.INSTANCE.getUnit());
        SpannableStringBuilder ssb = new SpannableStringBuilder(s);
        ssb.append(unitText);
        int len = ssb.length();
        //ssb.setSpan(new AbsoluteSizeSpan(12, true), len-unitText.length(), len, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        ssb.setSpan(new RelativeSizeSpan(0.33f), len - unitText.length(), len, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        ssb.setSpan(new SubscriptSpan(), len - unitText.length(), len, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        return ssb;
    }

}
