package ee.siimplangi.rallytripmeter.preferences

import androidx.preference.PreferenceGroup


interface PreferenceBuilder {

    fun add(preferenceScreen: PreferenceGroup)

}