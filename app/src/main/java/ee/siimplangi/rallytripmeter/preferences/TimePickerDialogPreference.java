package ee.siimplangi.rallytripmeter.preferences;

import android.content.Context;
import android.util.AttributeSet;

import androidx.preference.DialogPreference;

/**
 * Created by Siim on 10.09.2017.
 */

public class TimePickerDialogPreference extends DialogPreference {

    public TimePickerDialogPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public TimePickerDialogPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TimePickerDialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TimePickerDialogPreference(Context context) {
        super(context);
    }




}
