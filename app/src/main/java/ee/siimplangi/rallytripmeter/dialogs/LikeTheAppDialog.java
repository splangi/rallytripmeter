package ee.siimplangi.rallytripmeter.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.fragment.app.FragmentManager;
import ee.siimplangi.rallytripmeter.AppLog;
import ee.siimplangi.rallytripmeter.BuildConfig;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.constants.FirebaseConstants;
import ee.siimplangi.rallytripmeter.models.Feedback;

/**
 * Created by Siim on 21.09.2017.
 */

public class LikeTheAppDialog extends AppCompatDialogFragment {

    private static final String KEY_HAS_DONE_POSITIVE_ACTION = "key_has_done_positive_action";
    private static final String KEY_COUNTER = "counter";

    private static final String FRAGMENT_TAG = "liketheappdialog";

    private static final int divider = 2;

    public static void show(Context context, FragmentManager manager) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean hasDonePositiveActionAlready = preferences.getBoolean(KEY_HAS_DONE_POSITIVE_ACTION, false);
        if (!hasDonePositiveActionAlready) {
            int counter = PreferenceManager.getDefaultSharedPreferences(context).getInt(KEY_COUNTER, 5);
            for (float counterCopy = (float) counter / divider; counterCopy >= 1; counterCopy = counterCopy / divider) {
                if (counterCopy == 1f) {
                    new LikeTheAppDialog().show(manager, FRAGMENT_TAG);
                }
            }
            PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(KEY_COUNTER, counter + 1).apply();
        }

    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new LikeTheAppAlertDialog(getContext());
    }

    private static class LikeTheAppAlertDialog extends AlertDialog {


        protected LikeTheAppAlertDialog(@NonNull final Context context) {
            super(context);
            View v = LayoutInflater.from(context).inflate(R.layout.dialog_like_the_app, (ViewGroup) getWindow().getDecorView(), false);
            setView(v);
            final AppCompatRatingBar ratingBar = v.findViewById(R.id.ratingBar);
            final TextView rateViewText = v.findViewById(R.id.rateAppText);
            final EditText review = v.findViewById(R.id.review);
            final View leaveReviewLayout = v.findViewById(R.id.leaveReview);
            setButton(BUTTON_NEGATIVE, context.getString(R.string.later), new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            setButton(BUTTON_POSITIVE, "", new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (ratingBar.getRating() > 4) {
                        openGooglePlayLink(context);
                    } else {
                        sendToFirebase(ratingBar.getRating(), review.getText());
                    }
                    PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(KEY_HAS_DONE_POSITIVE_ACTION, true).apply();
                }
            });


            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    if (rating > 4 || rating == 0) {
                        rateViewText.setVisibility(View.VISIBLE);
                        leaveReviewLayout.setVisibility(View.GONE);
                        if (rating > 4) {
                            getButton(BUTTON_POSITIVE).setText(R.string.leave_review);
                            getButton(BUTTON_POSITIVE).setVisibility(View.VISIBLE);
                        } else {
                            getButton(BUTTON_POSITIVE).setVisibility(View.GONE);
                            getButton(BUTTON_POSITIVE).setText("");

                        }
                    } else {
                        rateViewText.setVisibility(View.GONE);
                        leaveReviewLayout.setVisibility(View.VISIBLE);
                        getButton(BUTTON_POSITIVE).setText(R.string.submit);
                        getButton(BUTTON_POSITIVE).setVisibility(View.VISIBLE);
                    }
                }
            });

        }


        private void openGooglePlayLink(Context context) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID));
            context.startActivity(intent);
        }

        private void sendToFirebase(float rating, CharSequence sequence) {
            Feedback feedback = new Feedback(rating, sequence.toString(), FirebaseAuth.getInstance().getCurrentUser());
            FirebaseDatabase.getInstance().getReference(FirebaseConstants.FEEDBACK_NODE).push().setValue(feedback).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    AppLog.e(LikeTheAppDialog.class, "Failed to send feedback to firebase!", e);
                }
            });
        }
    }
}
