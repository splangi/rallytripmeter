package ee.siimplangi.rallytripmeter.dialogs;

import android.content.Context;
import android.content.DialogInterface;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.views.TimePicker;

/**
 * Created by Siim on 09.08.2017.
 */

public class TimePickerDialog extends AlertDialog {

    private TimePicker timePicker;

    public TimePickerDialog(@NonNull final Context context, @StringRes int title, final OnTimeSetListener onTimeSetListener, boolean showSeconds, int secondSecquence, boolean initializaCurrentTime) {
        super(context);
        timePicker = new TimePicker(context, showSeconds, secondSecquence, initializaCurrentTime);
        setView(timePicker);
        setButton(BUTTON_POSITIVE, context.getString(R.string.ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onTimeSetListener.onTimeSet(timePicker.getHours(), timePicker.getMinutes(), timePicker.getSeconds());
                dialog.dismiss();
            }
        });
        setButton(BUTTON_NEGATIVE, context.getString(R.string.cancel), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        setTitle(title);

    }

    public TimePickerDialog(@NonNull final Context context, @StringRes int title, final OnTimeSetListener onTimeSetListener) {
        this(context, title, onTimeSetListener, true, 30, true);
    }

    public interface OnTimeSetListener {

        void onTimeSet(int hourOfDay, int minute, int seconds);
    }

}
