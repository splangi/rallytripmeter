package ee.siimplangi.rallytripmeter.dialogs;

/**
 * Created by Siim on 06.09.2017.
 */

/*

public class ProVersionDialog extends AlertDialog {

    private ProVersionDialog(@NonNull final BillingActivity activity, final Sku sku, int layoutId, CharSequence price) {
        super(activity, R.style.AppTheme_Light_Dialog);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(activity).inflate(layoutId, null, false);
        setView(v);
        TextView priceView = v.findViewById(R.id.price);
        if (price == null || "".equals(price.toString())) {
            priceView.setVisibility(View.GONE);
        } else {
            priceView.setText(price, TextView.BufferType.SPANNABLE);
        }
        setButton(BUTTON_POSITIVE, activity.getString(R.string.subscribe), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                activity.launchBillingFlow(sku);
                dismiss();
            }
        });
        setButton(BUTTON_NEGATIVE, activity.getString(R.string.later), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });


    }

    public static void show(BillingActivity activity) {
        show(activity, R.layout.dialog_pro_version2, Sku.PREMIUM_YEARLY_TRIAL);
    }

    public static void show(BillingActivity activity, int layoutId, Sku sku, CharSequence price) {
        try {
            new ProVersionDialog(activity, sku, layoutId, price).show();
            if (RallyTripmeter.crashLyticsEnabled) {
                try {
                    FirebaseAnalytics.getInstance(activity).logEvent(FirebaseConstants.EVENT_OPEN_PRO_DIALOG, null);
                    PremiumFeatures premiumFeatures = Settings.getPremiumFeatures();
                    Answers.getInstance().logAddToCart(new AddToCartEvent()
                            .putCurrency(Currency.getInstance(Locale.getDefault()))
                            .putItemId(sku.getId())
                            .putItemPrice(new BigDecimal(premiumFeatures.getSkuDetailsById(sku.getId()).getPriceAmountMicros() / 1000000f)));
                } catch (Exception e) {
                    AppLog.e(ProVersionDialog.class, "Failed to log event", e);
                }
            }

        } catch (Exception e) {
            AppLog.e(ProVersionDialog.class, "Failed to open ProVersionDialog", e);
        }
    }

    public static void show(BillingActivity activity, int layoutId, Sku sku) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        try {

            Collection<SkuDetails> details = Settings.getPremiumFeatures().getSkuDetailsList();
            if (details != null) {
                String price = "";
                String ending = "/" + activity.getString(R.string.year_short);
                String then = activity.getString(R.string.then);
                for (SkuDetails detail : details) {
                    if (detail.getSku().equals(sku.getId())) {
                        price = detail.getPrice().replace(" ", "");
                        break;
                    }
                }
                price = then + " " + price;
                builder.append(price);
                builder.append(ending);
                builder.setSpan(new RelativeSizeSpan(0.75f), price.length(), builder.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        } catch (Exception e) {
            AppLog.e(ProVersionDialog.class.getSimpleName(), "Failed to get price", e);
        }
        show(activity, layoutId, sku, builder);

    }


}
*/