package ee.siimplangi.rallytripmeter.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceDialogFragment;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.helpers.Settings;
import ee.siimplangi.rallytripmeter.helpers.TimeHelper;
import ee.siimplangi.rallytripmeter.models.RallyTime;
import ee.siimplangi.rallytripmeter.views.TimePicker;

/**
 * Created by Siim on 07.08.2017.
 */

public abstract class TimePickerPreferenceDialog extends PreferenceDialogFragment {

    private static final String KEY_HOURS = "hours";
    private static final String KEY_MINUTES = "minutes";
    private static final String KEY_SECONDS = "seconds";
    private TimePicker timePicker;

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_HOURS, timePicker.getHours());
        outState.putInt(KEY_MINUTES, timePicker.getMinutes());
        outState.putInt(KEY_SECONDS, timePicker.getSeconds());
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        if (savedInstanceState != null){
            timePicker.setHour(savedInstanceState.getInt(KEY_HOURS));
            timePicker.setMinutes(savedInstanceState.getInt(KEY_MINUTES));
            timePicker.setSeconds(savedInstanceState.getInt(KEY_SECONDS));
        }

        return dialog;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        timePicker = view.findViewById(R.id.timepicker);
    }

    protected TimePicker getTimePicker() {
        return timePicker;
    }

    public static class RallyTimePreferenceDialog extends TimePickerPreferenceDialog {

        public static PreferenceDialogFragment newInstance(String key) {
            PreferenceDialogFragment fragment = new RallyTimePreferenceDialog();
            Bundle bundle = new Bundle();
            bundle.putString(ARG_KEY, key);
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public void onDialogClosed(boolean positiveResult) {
            if (positiveResult) {
                TimePicker timePicker = getTimePicker();
                Settings.INSTANCE.setRallyTime(new RallyTime(TimeHelper.getOffsetWithSystemTime(timePicker.getHours(), timePicker.getMinutes(), timePicker.getSeconds())));
            }
        }
    }


}
