package ee.siimplangi.rallytripmeter.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.appcompat.app.AlertDialog;
import ee.siimplangi.rallytripmeter.R;
import ee.siimplangi.rallytripmeter.listeners.OnDialogAcceptedListener;

/**
 * Created by Siim on 07.09.2017.
 */

public class DialogFactory {

    private DialogFactory() {

    }


    public static AlertDialog getEditTextDialog(final Context context, @LayoutRes int editTextLayoutRes, final OnDialogAcceptedListener<String> dialogAcceptedListener) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).setView(editTextLayoutRes).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AlertDialog alertDialog = (AlertDialog) dialog;
                EditText editText = alertDialog.findViewById(R.id.editText);
                dialogAcceptedListener.onClick(dialog, editText.getText().toString());
            }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).create();
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        EditText editText = alertDialog.findViewById(R.id.editText);
        if (editText != null) {
            editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
                        return true;
                    }
                    return false;
                }
            });
        }
        return alertDialog;
    }

}
