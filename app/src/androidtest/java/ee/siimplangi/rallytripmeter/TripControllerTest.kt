package ee.siimplangi.rallytripmeter

import android.support.test.filters.SdkSuppress
import android.support.test.filters.SmallTest
import android.support.test.runner.AndroidJUnit4
import ee.siimplangi.rallytripmeter.controllers.TripController
import ee.siimplangi.rallytripmeter.models.TripModel
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
@SdkSuppress(minSdkVersion = 18)
class TripControllerTest {

    lateinit var tripController: TripController<TripModel.Trip1>


    @Before
    fun initController() {
        tripController = TripController(TripModel.Trip1(), )
    }

    @Test
    fun testSaveInstance() {

    }

    fun testOnLocation() {

    }

    fun testOnPlus() {

    }

    fun test


}