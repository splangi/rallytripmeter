package ee.siimplangi.rallytripmeter;

import android.content.pm.ActivityInfo;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SdkSuppress;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ee.siimplangi.rallytripmeter.activities.MainActivity;

import static ee.siimplangi.rallytripmeter.TestHelper.openDrawerWithMenuId;

/**
 * Created by Siim on 20.09.2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
@SdkSuppress(minSdkVersion = 18)
public class RallyTripmeterTests {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule(MainActivity.class);

    @Test
    public void openRecceModeAndRotate() {
        openDrawerWithMenuId(R.id.recce);
        rotateDevice();
    }

    @Test
    public void openLiaisonModeAndRotate() {
        openDrawerWithMenuId(R.id.liaison);
        rotateDevice();

    }

    @Test
    public void openRaceModeAndRotate() {
        openDrawerWithMenuId(R.id.racing);
        rotateDevice();

    }

    @Test
    public void openAllThreeModes() {
        openDrawerWithMenuId(R.id.recce);
        SystemClock.sleep(500);
        openDrawerWithMenuId(R.id.liaison);
        SystemClock.sleep(500);
        openDrawerWithMenuId(R.id.racing);
    }


    public void rotateDevice() {
        mActivityRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        InstrumentationRegistry.getInstrumentation().waitForIdleSync();
        mActivityRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        InstrumentationRegistry.getInstrumentation().waitForIdleSync();
    }


}