package ee.siimplangi.rallytripmeter;

import android.os.SystemClock;
import android.support.test.espresso.action.ViewActions;
import android.support.test.filters.SdkSuppress;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Random;

import ee.siimplangi.rallytripmeter.activities.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItem;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static ee.siimplangi.rallytripmeter.TestHelper.openDrawerWithMenuId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;

/**
 * Created by Siim on 20.09.2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
@SdkSuppress(minSdkVersion = 18)
public class SpecialStagesTests {

    public final String RALLY_NAME = "Rally Name";
    public final String STAGE_NAME = "StageDef name";
    public final String LENGTH = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US)).format(new Random().nextFloat() * 10);

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule(MainActivity.class);

    @Before
    public void addStageTest() {
        openDrawerWithMenuId(R.id.stages);
        onView(withId(R.id.fab)).perform(click());
        onView(withId(R.id.rallyName)).perform(typeText(RALLY_NAME));
        onView(withId(R.id.stageName)).perform(typeText(STAGE_NAME));
        onView(withId(R.id.fab)).perform(click());
    }

    @Test
    public void removeStageTest() {
        SystemClock.sleep(1000);
        onView(withId(R.id.recyclerView))
                .perform(
                        actionOnItem(
                                hasDescendant(allOf(withText(containsString(RALLY_NAME)), withText(containsString(STAGE_NAME)))),
                                ViewActions.swipeLeft()
                        )).perform(actionOnItem(
                hasDescendant(allOf(withText(containsString(RALLY_NAME)), withText(containsString(STAGE_NAME)), withText(containsString(LENGTH)))),
                ViewActions.swipeLeft()
        ));
        SystemClock.sleep(1000);
        onView(withId(R.id.recyclerView))
                .perform(
                        actionOnItem(
                                hasDescendant(withId(R.id.delete)),
                                ViewActions.click()));
    }

}
