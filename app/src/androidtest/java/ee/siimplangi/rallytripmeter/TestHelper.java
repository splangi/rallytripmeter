package ee.siimplangi.rallytripmeter;

import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.espresso.contrib.NavigationViewActions;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by Siim on 20.09.2017.
 */

public class TestHelper {


    public static void openDrawerWithMenuId(int menuId) {
        onView(withId(R.id.drawer)).perform(DrawerActions.open());
        onView(withId(R.id.navigation)).perform(NavigationViewActions.navigateTo(menuId));
    }

}
